import { RouteLocationNormalized, RouteRecordNormalized } from 'vue-router';
import {
  resolvePageTitleFromBreadcrumbs,
  Breadcrumb,
} from '../../../src/utils/routes'; // Replace with your actual module path

describe('Page Title Resolver', () => {
  test('returns default title when breadcrumbs are empty', () => {
    const defaultTitle = 'Default Title';
    const routeName = 'Home';
    const route = { name: routeName };

    const result = resolvePageTitleFromBreadcrumbs(
      [],
      route as RouteLocationNormalized,
      defaultTitle
    );
    expect(result).toBe(routeName);
  });

  test('returns hyphen-separated, reversed breadcrumb titles', () => {
    const breadcrumbs: Breadcrumb[] = [
      { title: 'First' },
      { title: 'Second' },
      { title: 'Third' },
    ];
    const route = { name: 'irrelevant' };
    const defaultTitle = 'irrelevant';

    const result = resolvePageTitleFromBreadcrumbs(
      breadcrumbs,
      route as RouteLocationNormalized,
      defaultTitle
    );
    expect(result).toBe('Third - Second - First');
  });

  test('returns single breadcrumb without hyphenation', () => {
    const breadcrumbs: Breadcrumb[] = [{ title: 'First' }];
    const route = { name: 'Home' };
    const defaultTitle = 'Default Title';

    const result = resolvePageTitleFromBreadcrumbs(
      breadcrumbs,
      route as RouteLocationNormalized,
      defaultTitle
    );
    expect(result).toBe('First');
  });
});
