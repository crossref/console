import { describe, it, expect } from 'vitest';
import { trim, TrimOption } from '@/forms/utils/format';
import { ref } from 'vue';

describe('trim function', () => {
  it('should return the input as is if it is not a string', () => {
    expect(trim(123)).toBe(123);
    expect(trim({})).toEqual({});
    expect(trim([])).toEqual([]);
    expect(trim(null)).toBeNull();
    expect(trim(undefined)).toBeUndefined();
  });

  it('should trim leading spaces', () => {
    expect(trim('   leading spaces', { trim: TrimOption.Start })).toBe('leading spaces');
  });

  it('should trim trailing spaces', () => {
    expect(trim('trailing spaces   ', { trim: TrimOption.End })).toBe('trailing spaces');
  });

  it('should trim both leading and trailing spaces', () => {
    expect(trim('   both spaces   ', { trim: TrimOption.Both })).toBe('both spaces');
  });

  it('should trim all spaces', () => {
    expect(trim('   all   spaces   ', { trim: TrimOption.All })).toBe('allspaces');
  });

  it('should return the input as is if no trim option is provided', () => {
    expect(trim('   no trim option   ')).toBe('   no trim option   ');
  });

  it('should handle reactive options', () => {
    const options = ref({ trim: TrimOption.All });
    expect(trim('   reactive   options   ', options)).toBe('reactiveoptions');
  });
});
