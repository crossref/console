import {
  JsonFormsRendererRegistryEntry,
  JsonSchema,
  UISchemaElement,
} from '@jsonforms/core';
import { mount } from '@vue/test-utils';
import TestComponent from './TestComponent.vue';
import vuetifyInstance from '@/plugins/vuetify';

// Mount the Json Schema backed form test harness
export const mountJsonForms = (
  data: any,
  schema: JsonSchema,
  renderers: JsonFormsRendererRegistryEntry[],
  uischema?: UISchemaElement,
  config?: any
) => {
  return mount(TestComponent, {
    props: {
      initialData: data,
      schema,
      uischema,
      config,
      initialRenderers: renderers,
    },
    global: {
      plugins: [vuetifyInstance],
    },
  });
};
