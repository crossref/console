import { describe, it, expect, vi } from 'vitest';
import journalArticleSchema from '@/forms/schemas/json/journalArticleForm/journalArticleForm.schema.json';
import {getLanguageCodes} from "@/forms/utils/i18nUtil";

const jsonSchemaLangCodes = journalArticleSchema.definitions['iso-639-1_language_codes'].enum

test('Language Chooser ISO 639-1 code list matches Journal Article form JSON Schema', () => {
  expect(getLanguageCodes()).toEqual(jsonSchemaLangCodes)
})

