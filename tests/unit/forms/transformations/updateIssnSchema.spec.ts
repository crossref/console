import { describe, it, expect } from 'vitest';
import {
  hasIssnChanged,
  transformIssnFields,
} from '../../../../src/forms/transformations/updateIssnSchema'; // Update the path as necessary
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';

describe('ISSN transformation tester', () => {
  it('should return true when ISSNs have changed', () => {
    const prevData = {
      journal: { issn: { printIssn: '1234-5678' } },
    };
    const newData = {
      journal: { issn: { printIssn: '8765-4321' } },
    };
    const schema = {};
    const context = {};

    const result = hasIssnChanged(schema, newData, prevData, context);
    expect(result).toBe(true);
  });

  it('should return false when ISSNs have not changed', () => {
    const commonData = {
      journal: { issn: { printIssn: '1234-5678', onlineIssn: '0000-1111' } },
    };
    const schema = {};
    const context = {};

    const result = hasIssnChanged(schema, commonData, commonData, context);
    expect(result).toBe(false);
  });
});

describe('ISSN schema transformer', () => {
  it('updates the schema to require ISSNs when neither is present', () => {
    const data = {
      journal: { issn: {} }, // No ISSNs present
    };
    const schema = {
      properties: {
        journal: {
          properties: {
            issn: {
              required: [],
            },
          },
        },
      },
    };

    const updatedSchema = transformIssnFields(cloneDeep(schema), data);
    expect(
      get(updatedSchema, 'properties.journal.properties.issn.required')
    ).toEqual(['printIssn', 'onlineIssn']);
  });

  it('does not modify the schema if at least one ISSN is present', () => {
    const data = {
      journal: { issn: { printIssn: '1234-5678' } },
    };
    const expectedSchema = {
      properties: {
        journal: {
          properties: {
            issn: {
              required: [],
            },
          },
        },
      },
    };

    const updatedSchema = transformIssnFields(cloneDeep(expectedSchema), data);
    expect(updatedSchema).toEqual(expectedSchema);
  });
});
