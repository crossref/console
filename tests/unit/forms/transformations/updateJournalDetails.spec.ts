import { describe, it, expect } from 'vitest';
import { updateJournalDetails } from '@/forms/transformations/updateJournalDetails';
import cloneDeep from 'lodash/cloneDeep';
import {Journal} from "@pvale/crossref-client";

// Mock context and data
const initialData = {
  journal: {
    issn: {
      printIssn: 'previous-print',
      onlineIssn: 'previous-online',
    },
    titles: {
      fullTitle: 'Initial Title',
    },
  },
};

const contextWithoutIssn: Partial<Journal> = {
  title: 'New Journal Title',
  issnType: [],
};

const contextWithPrintIssn = {
  title: 'New Journal Title',
  issnType: [{ type: 'print', value: '1111-1111', issnType: () => true }],
};

const contextWithOnlineIssn = {
  title: 'New Journal Title',
  issnType: [{ type: 'electronic', value: '2222-2222', issnType: () => true }],
};

const contextWithBothIssn = {
  title: 'New Journal Title',
  issnType: [
    { type: 'print', value: '1111-1111', issnType: () => true },
    { type: 'electronic', value: '2222-2222', issnType: () => true },
  ],
};

const contextNoTitleWithIssn = {
  issnType: [
    { type: 'print', value: '3333-3333', issnType: () => true },
    { type: 'electronic', value: '4444-4444', issnType: () => true },
  ],
};

const contextNoTitleNoIssn = {
  issnType: [],
};

describe('updateJournalDetails', () => {
  it('removes ISSNs if not provided in context', () => {
    const result = updateJournalDetails(
      cloneDeep(initialData),
      {},
      contextWithoutIssn
    );
    expect(result.journal.issn.printIssn).toBeUndefined();
    expect(result.journal.issn.onlineIssn).toBeUndefined();
    expect(result.journal.titles.fullTitle).toBe(contextWithoutIssn.title);
  });

  it('updates only print ISSN when provided', () => {
    const result = updateJournalDetails(
      cloneDeep(initialData),
      {},
      contextWithPrintIssn
    );
    expect(result.journal.issn.printIssn).toBe('1111-1111');
    expect(result.journal.issn.onlineIssn).toBeUndefined();
    expect(result.journal.titles.fullTitle).toBe(contextWithPrintIssn.title);
  });

  it('updates only online ISSN when provided', () => {
    const result = updateJournalDetails(
      cloneDeep(initialData),
      {},
      contextWithOnlineIssn
    );
    expect(result.journal.issn.printIssn).toBeUndefined();
    expect(result.journal.issn.onlineIssn).toBe('2222-2222');
    expect(result.journal.titles.fullTitle).toBe(contextWithOnlineIssn.title);
  });

  it('updates both print and online ISSNs when provided', () => {
    const result = updateJournalDetails(
      cloneDeep(initialData),
      {},
      contextWithBothIssn
    );
    expect(result.journal.issn.printIssn).toBe('1111-1111');
    expect(result.journal.issn.onlineIssn).toBe('2222-2222');
    expect(result.journal.titles.fullTitle).toBe(contextWithBothIssn.title);
  });
  it('updates ISSNs correctly without title', () => {
    const resultWithIssn = updateJournalDetails(
      cloneDeep(initialData),
      {},
      contextNoTitleWithIssn
    );
    expect(resultWithIssn.journal.issn.printIssn).toBe('3333-3333');
    expect(resultWithIssn.journal.issn.onlineIssn).toBe('4444-4444');
    expect(resultWithIssn.journal.titles.fullTitle).toBe(
      initialData.journal.titles.fullTitle
    ); // Should be unchanged

    const resultNoIssn = updateJournalDetails(
      cloneDeep(initialData),
      {},
      contextNoTitleNoIssn
    );
    expect(resultNoIssn.journal.issn.printIssn).toBeUndefined();
    expect(resultNoIssn.journal.issn.onlineIssn).toBeUndefined();
    expect(resultNoIssn.journal.titles.fullTitle).toBe(
      initialData.journal.titles.fullTitle
    ); // Should be unchanged
  });
});
