import { describe, it, expect } from 'vitest';
import Ajv, { SchemaObjCxt } from 'ajv';
import getDef from '../../../../src/forms/validators/AjvKeywordUniqueValue';

// Create an AJV instance and add the custom keyword
const ajv = new Ajv();
ajv.addKeyword(getDef());

describe('uniqueValue keyword', () => {
  it('should validate unique field values in an array', () => {
    // Define a schema with the uniqueValue keyword
    const schema = {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: { type: 'string', uniqueValue: true },
        },
        required: ['id'],
      },
    };

    // Compile the schema
    const validate = ajv.compile(schema);

    // Test data with unique values
    const validData = [
      { id: 'a' },
      { id: 'b' },
      { id: 'c' },
    ];

    // Test data with duplicate values
    const invalidData = [
      { id: 'a' },
      { id: 'b' },
      { id: 'a' },
    ];

    // Validate the data
    expect(validate(validData)).toBe(true); // Should be valid
    expect(validate(invalidData)).toBe(false); // Should be invalid
    expect(validate.errors).toBeTruthy(); // Errors should be present for invalid data
    if (validate.errors) {
      expect(validate.errors[0].message).toBe('must pass "uniqueValue" keyword validation');
    }
  });

  it('should validate as true if context is not provided', () => {
    // Define parent schema
    const parentSchema = {};
    // Use type assertion to create a minimal mock object for SchemaObjCxt
    const mockSchemaObjCxt = {} as unknown as SchemaObjCxt;

    // Get the keyword definition and compile method
    const keywordDef = getDef();
    if (keywordDef && keywordDef.compile) {
      // Compile the validation function with minimal context
      const validate = keywordDef.compile('id', parentSchema, mockSchemaObjCxt);
      // Validate an empty array to check the default behavior
      expect(validate([])).toBe(true); // Should be valid without context
    } else {
      throw new Error('Keyword definition or compile method is undefined');
    }
  });

  it('should handle empty arrays correctly', () => {
    // Define a schema with the uniqueValue keyword
    const schema = {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: { type: 'string', uniqueValue: true },
        },
      },
    };

    // Compile the schema
    const validate = ajv.compile(schema);
    const emptyData = [];

    // Validate the empty array
    expect(validate(emptyData)).toBe(true); // Should be valid
  });

  it('should return true for arrays with a single item', () => {
    // Define a schema with the uniqueValue keyword
    const schema = {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: { type: 'string', uniqueValue: true },
        },
      },
    };

    // Compile the schema
    const validate = ajv.compile(schema);
    const singleItemData = [{ id: 'a' }];

    // Validate the array with a single item
    expect(validate(singleItemData)).toBe(true); // Should be valid
  });

  it('should ignore empty strings when checking for uniqueness', () => {
    // Define a schema with the uniqueValue keyword
    const schema = {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: { type: 'string', uniqueValue: true },
        },
      },
    };

    // Compile the schema
    const validate = ajv.compile(schema);

    // Test data with empty string values along with valid strings
    const complexData = [
      { id: 'a' },
      { id: '' }, // Should be ignored
      { id: '' }, // Should be ignored
      { id: 'b' },
      { id: 'c' },
      { id: 'a' }, // Should trigger uniqueness violation
    ];

    // Validate the data
    expect(validate(complexData)).toBe(false); // Should be invalid due to duplicate 'a'
    expect(validate.errors).toBeTruthy(); // Errors should be present for invalid data

    // Ensure the error is specifically about uniqueness
    if (validate.errors) {
      expect(validate.errors[0].keyword).toBe('uniqueValue');
      expect(validate.errors[0].message).toBe(
        'must pass "uniqueValue" keyword validation'
      );
    }

    // Test data where non-string values do not cause false positives
    const validDataIgnoringNones = [
      { id: 'a' },
      { id: 'b' },
      { id: 'c' },
      { id: '' }, // Should be ignored
      { id: '' }, // Should be ignored
    ];

    // Validate the adjusted data
    expect(validate(validDataIgnoringNones)).toBe(true); // Should be valid as only 'a', 'b', 'c' are considered
  });
});
