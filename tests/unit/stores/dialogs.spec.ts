import { describe, it, expect, beforeEach } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';
import { useDialogStore } from '@/stores/dialogs';

describe('Dialog Store', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  it('should start with no dialog shown', () => {
    const dialogStore = useDialogStore();
    expect(dialogStore.show).toBe(false);
    expect(dialogStore.dialog).toBeNull();
  });

  it('should show and populate dialog on showDialog', () => {
    const dialogStore = useDialogStore();
    const testError = new Error('Test error');
    dialogStore.showDialog(testError, null, 'Test', true);
    expect(dialogStore.show).toBe(true);
    expect(dialogStore.dialog).toEqual({
      error: testError,
      callback: null,
      secondaryActionText: 'Test',
      closeOnCallback: true,
    });
  });

  it('should dismiss dialog', () => {
    const dialogStore = useDialogStore();
    dialogStore.showDialog(new Error('Test error'));
    dialogStore.dismissDialog();
    expect(dialogStore.show).toBe(false);
    expect(dialogStore.dialog).toBeNull();
  });

  it('should execute callback and close dialog if closeOnCallback is true', () => {
    const dialogStore = useDialogStore();
    const callback = vi.fn();
    dialogStore.showDialog(new Error('Test error'), callback, 'Test', true);
    dialogStore.executeCallback();
    expect(callback).toHaveBeenCalledTimes(1);
    expect(dialogStore.show).toBe(false);
    expect(dialogStore.dialog).toBeNull();
  });
});
