import {
  deposit,
  DepositRequestBody,
  DepositRequestHeaders,
} from '@/utils/deposit';
import {
  depositResponse200Processed,
  depositResponse403Unauthorised,
  depositResponse403UnauthorisedMessage,
  grantidFullExampleInvalidXML,
} from './exampleXMLs';
import { generateXMLString } from '@/services/grantsXMLGenerator';
import { grantFormData as grantFormData1 } from '../XmlGeneration/example-data/grantForm1';
import grantFormData2 from '../XmlGeneration/example-data/grantForm2.json';
import { describe, expect, it } from 'vitest';
import axios from 'axios';
import {
  deposit401UnauthorisedResponse,
  deposit500ServerErrorResponse,
} from '@/mocks/handlers';

const generatedXML = [
  ['Generated XML example 1', generateXMLString(grantFormData1, true)],
  ['Generated XML example 1', generateXMLString(grantFormData2, true)],
];
const handMadeXML = [
  ['Hand-made XML example 1', generateXMLString(grantFormData1, true)],
];
const xml = [...generatedXML, ...handMadeXML];

const mockSuccessResponse = {
  data: depositResponse200Processed,
  status: 200,
};

const mockForbiddenResponse = {
  data: depositResponse403Unauthorised,
  status: 403,
};

const mockUnauthorisedResponse = {
  data: deposit401UnauthorisedResponse,
  status: 401,
};

const mockServerErrorResponse = {
  data: deposit500ServerErrorResponse,
  status: 500,
};

describe('XML Deposit', () => {
  const TOKEN_VALID = 'VALID_TOKEN';
  const TOKEN_INVALID = 'INVALID_TOKEN';
  const REFRESH_TOKEN_VALID = 'VALID_REFRESH_TOKEN';
  const REFRESH_TOKEN_INVALID = 'INVALID_REFRESH_TOKEN';
  it('Should succeed with a success message on receipt of a 200 response with no processing failures', async () => {
    // Mock the specific Axios call
    vi.spyOn(axios, 'post').mockResolvedValue(mockSuccessResponse);
    const depositBody: DepositRequestBody = {
      usr: '200ProcessedOK',
      pwd: 'testpass',
      operation: 'doMDUpload',
      mdFile: new Blob([''], { type: 'text/xml' }),
    };
    const headers: DepositRequestHeaders = {
      token: TOKEN_VALID,
      refresh_token: REFRESH_TOKEN_VALID,
    };
    // mockedAxios.post.(mockedResponse)
    const result = await deposit(depositBody, headers);
    expect(result.body).toEqual(depositResponse200Processed);
    expect(result.statusCode).toEqual(200);
    expect(result.status).toEqual('success');
  });
  it('Should fail with error message on receipt of 401 response', async () => {
    vi.spyOn(axios, 'post').mockResolvedValue(mockUnauthorisedResponse);

    const headers: DepositRequestHeaders = {
      token: TOKEN_INVALID,
      refresh_token: REFRESH_TOKEN_INVALID,
    };
    const depositBody: DepositRequestBody = {
      usr: '401Unauthorized',
      operation: 'doMDUpload',
      mdFile: new Blob([''], { type: 'text/xml' }),
    };
    const result = await deposit(depositBody, headers);
    expect(result.statusCode).toEqual(401);
    expect(result.status).toEqual('failure');
    expect(result.message).toEqual('Unauthorized');
  });
  it('Should fail with error message on receipt of 403 response', async () => {
    vi.spyOn(axios, 'post').mockResolvedValue(mockForbiddenResponse);

    const headers: DepositRequestHeaders = {
      token: TOKEN_INVALID,
      refresh_token: REFRESH_TOKEN_INVALID,
    };

    const depositBody: DepositRequestBody = {
      usr: '403Forbidden',
      operation: 'doMDUpload',
      mdFile: new Blob([''], { type: 'text/xml' }),
    };
    const result = await deposit(depositBody, headers);
    expect(result.body).toEqual(depositResponse403Unauthorised);
    expect(result.statusCode).toEqual(403);
    expect(result.status).toEqual('failure');
    expect(result.message).toEqual(depositResponse403UnauthorisedMessage);
  });
  it('Should fail with error message on receipt of 500 response', async () => {
    const depositBody: DepositRequestBody = {
      usr: '500InternalServerError',
      operation: 'doMDUpload',
      mdFile: new Blob([grantidFullExampleInvalidXML], { type: 'text/xml' }),
    };
    const headers: DepositRequestHeaders = {
      token: TOKEN_VALID,
      refresh_token: REFRESH_TOKEN_VALID,
    };
    vi.spyOn(axios, 'post').mockResolvedValue(mockServerErrorResponse);
    const result = await deposit(depositBody, headers);
    expect(result.statusCode).toEqual(500);
    expect(result.body).toEqual(deposit500ServerErrorResponse);
    expect(result.status).toEqual('failure');
    expect(result.message).toEqual('Internal server error');
  });
  it('Should emit an error when a network error occurs', async () => {
    // Mock a network failure error
    const networkFailureError = new Error('Network Error');

    // Nb we're not adding a 'response' property to simulate a total failure
    vi.spyOn(axios, 'post').mockRejectedValue(networkFailureError);

    const headers: DepositRequestHeaders = {
      token: TOKEN_VALID,
      refresh_token: REFRESH_TOKEN_VALID,
    };

    const depositBody: DepositRequestBody = {
      usr: 'NetworkError',
      operation: 'doMDUpload',
      mdFile: new Blob([grantidFullExampleInvalidXML], { type: 'text/xml' }),
    };
    const result = await deposit(depositBody, headers);
    expect(result.status).toEqual('failure');
    expect(result.message).toEqual('Network Error');
  });
});
