export const grantFormData = {
  depositor: {
    depositor_name: 'Crossref',
    email_address: 'support@crossref.org',
  },
  grants: [
    {
      'award-number': 'CR-001x',
      'award-start-date': '2018-01-01',
      doi_data: {
        doi: '10.32013/NI84HFx',
        resource:
          'https://www.crossref.org/blog/wellcome-explains-the-benefits-of-developing-an-open-and-global-grant-identifier/',
      },
      project: [
        {
          'project-title': [
            {
              'title-text': 'Improving Metadata using cats as motivators',
              lang: 'en',
            },
            {
              'title-text':
                'Melhorando Metadados usando gatos como motivadores',
              lang: 'pt',
            },
          ],
          award_amount: {
            amount: '24.50',
            currency: 'USD',
          },
          description: [
            {
              'description-text':
                'This is an example of a project description or abstract. Multiple descriptions may be supplied to accommodate different description or abstract types, or to provide descriptions in multiple languages.',
              lang: 'en',
            },
            {
              'description-text':
                'Este é um exemplo de uma descrição ou resumo do projeto. Várias descrições podem ser fornecidas para acomodar diferentes descrições ou tipos abstratos, ou para fornecer descrições em vários idiomas.',
              lang: 'pt',
            },
          ],
          funding: {
            'funding-amount': '24.50',
            'funder-name': 'Wellcome Trust',
            'funder-id': 'https://doi.org/10.13039/100004440',
            'funding-type': 'equipment',
            'funding-scheme': 'Sofa Lending Programme',
          },
          awardDates: {
            'award-start-date': '2018-01-05',
            'award-end-date': '2023-02-01',
          },
          investigators: [
            {
              givenName: 'Minerva',
              familyName: 'Reuland',
              alternateName: 'Minnie Cat',
              ORCID: 'https://orcid.org/0000-0002-4011-3590',
              affiliation: {
                institution: 'University of Crossref',
              },
              role: 'lead_investigator',
            },
          ],
        },
      ],
    },
    {
      'award-number': 'CR-567',
      'award-start-date': '2018-01-01',
      doi_data: {
        doi: '10.32013/NI84HFx',
        resource:
          'https://www.crossref.org/blog/wellcome-explains-the-benefits-of-developing-an-open-and-global-grant-identifier/',
      },
      project: [
        {
          'project-title': [
            {
              'title-text': 'Registering identifiers for grants',
              lang: 'en',
            },
            {
              'title-text': '補助金の識別子を登録する',
              lang: 'ja',
            },
            {
              'title-text':
                "Enregistrement d'identifiants pour des subventions",
              lang: 'fr',
            },
          ],
          award_amount: {
            amount: '1000000',
            currency: 'USD',
          },
          description: [
            {
              'description-text':
                'This is an example of a project description or abstract. Multiple descriptions may be supplied to accommodate different description or abstract types, or to provide descriptions in multiple languages.',
              lang: 'en',
            },
            {
              'description-text':
                'Ceci est un exemple de description de projet ou de résumé. Plusieurs descriptions peuvent être fournies pour prendre en charge différents types de description ou d’abrégés, ou pour fournir des descriptions dans plusieurs langues.',
              lang: 'fr',
            },
          ],
          funding: [
            {
              'funding-amount': '500000',
              'funding-currency': 'USD',
              'funding-percentage': '50',
              'funder-name': 'Wellcome Trust',
              'funder-id': 'https://doi.org/10.13039/100004440',
              'funding-type': 'grant',
              'funding-scheme': 'Metadata quality initiative',
            },
            {
              'funding-amount': '500000',
              'funding-currency': 'USD',
              'funding-percentage': '50',
              'funder-name': 'Wellcome Trust',
              'funder-id': 'https://doi.org/10.13039/100004440',
              'funding-type': 'grant',
              'funding-scheme': 'Metadata is awesome prize',
            },
          ],
          awardDates: {
            'award-start-date': '2018-01-05',
            'award-end-date': '2023-02-01',
          },
          investigators: [
            {
              givenName: 'Elvis',
              familyName: 'Yausudder',
              alternateName: 'Elvis Slozzuffer',
              ORCID: 'https://orcid.org/0000-0002-4011-3590',
              affiliation: [
                {
                  institution: 'University of Metadata',
                },
                {
                  institution: 'Crossref',
                  country: 'US',
                  ror: 'https://ror.org/02twcfp32',
                },
              ],
              startDate: '2018-01-01',
              role: 'lead_investigator',
            },
            {
              givenName: 'Valerie',
              familyName: 'Torf',
              ORCID: 'https://orcid.org/0000-0002-4011-3590',
              affiliation: [
                {
                  institution: 'Crossref',
                  country: 'US',
                  ror: 'https://ror.org/02twcfp32',
                },
              ],
              startDate: '2018-01-01',
              role: 'lead_investigator',
            },
          ],
        },
        {
          'project-title': [
            {
              'title-text': 'Metadata outputs for grant identifiers',
            },
          ],
          award_amount: {
            amount: '1000',
            currency: 'USD',
          },
          description: [
            {
              'description-text':
                'This is an example of an abstract or project description. The length is unlimited.',
            },
          ],
          funding: [
            {
              'funder-name': 'Wellcome Trust',
              'funder-id': 'https://doi.org/10.13039/100004440',
              'funding-type': 'contract',
              'funding-scheme': 'Metadata quality initiative',
            },
          ],
          investigators: [
            {
              givenName: 'Edana',
              familyName: 'Trolb',
              alternateName: 'Scarlet Zomb',
              affiliation: [
                {
                  institution: 'Crossref',
                  country: 'US',
                  ror: 'https://ror.org/02twcfp32',
                },
              ],
              startDate: '2018-01-01',
              role: 'lead_investigator',
            },
            {
              givenName: 'Valerie',
              familyName: 'Torf',
              alternateName: 'Elvis Slozzuffer',
              ORCID: 'https://orcid.org/0000-0002-4011-3590',
              affiliation: [
                {
                  institution: 'Crossref',
                  country: 'US',
                  ror: 'https://ror.org/02twcfp32',
                },
              ],
              startDate: '2018-01-01',
              role: 'lead_investigator',
            },
          ],
        },
      ],
    },
  ],
};
