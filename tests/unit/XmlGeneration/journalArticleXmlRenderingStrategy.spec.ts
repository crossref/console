import {describe, expect, it, vi} from 'vitest';
import {
  JournalArticleXmlRenderingStrategy,
  insertCitationsToXML,
} from '@/services/xmlRendering/JournalArticleXmlRenderingStrategy';
import { XmlConversionBadInputError, XmlConversionError } from '@/errors';
import { ContentRegistrationType } from '@/common/types';

// Mock dependencies
vi.mock('@/itemTree/mapping/JournalArticleMapper', () => ({
  mapJournalArticleToItemTree: vi.fn(),
}));
vi.mock('@/env', () => ({
  env: () => ({
    convertApiEndpoint: 'https://api.example.com/convert',
  }),
}));
vi.mock('@/composable/useI18n', () => ({
  useTranslation: () => {
    return (key: string) => key; // Return the key as the "translation"
  },
}));
const mockFetch = vi.fn();
global.fetch = mockFetch;

describe('JournalArticleXmlRenderingStrategy', () => {
  const strategy = new JournalArticleXmlRenderingStrategy();

  beforeEach(() => {
    mockFetch.mockClear();
  });

  it('should generate XML successfully', async () => {
    mockFetch.mockResolvedValue({
      ok: true,
      text: vi.fn().mockResolvedValue('<xml>Success</xml>'),
    });

    const xml = await strategy.generateXML(
      ContentRegistrationType.JournalArticle,
      {}
    );
    expect(xml).toBe('<xml>Success</xml>');
    expect(mockFetch).toHaveBeenCalledTimes(1);
    const fetchArgs = mockFetch.mock.calls[0];
    const requestBody = JSON.parse(fetchArgs[1].body);
    expect(requestBody.doiBatchHead.depositorName).toBe('Unknown Depositor');
    expect(requestBody.doiBatchHead.depositorEmail).toBe(
      'content-registration-form+unknown-depositor@crossref.org'
    );
  });

  it('should throw XmlConversionBadInputError for status 400', async () => {
    mockFetch.mockResolvedValue({
      ok: false,
      status: 400,
      json: () =>
        Promise.resolve({
          error: 'Bad input parameters',
          details: 'Missing required fields',
        }),
    });

    await expect(
      strategy.generateXML(ContentRegistrationType.JournalArticle, {})
    ).rejects.toThrow(XmlConversionBadInputError);
  });

  it('should throw XmlConversionError for other errors', async () => {
    mockFetch.mockResolvedValue({
      ok: false,
      status: 500,
      json: () => Promise.resolve({}),
    });

    await expect(
      strategy.generateXML(ContentRegistrationType.JournalArticle, {})
    ).rejects.toThrow(XmlConversionError);
  });

  it('should set depositor information correctly', async () => {
    mockFetch.mockResolvedValue({
      ok: true,
      text: vi.fn().mockResolvedValue('<xml>Success</xml>'),
    });

    const depositorInfo = {
      depositorName: 'John Doe',
      depositorEmail: 'johndoe@example.com',
    };

    const xml = await strategy.generateXML(
      ContentRegistrationType.JournalArticle,
      {},
      false,
      depositorInfo
    );
    expect(xml).toBe('<xml>Success</xml>');
    expect(mockFetch).toHaveBeenCalledTimes(1);
    const fetchArgs = mockFetch.mock.calls[0];
    const requestBody = JSON.parse(fetchArgs[1].body);
    expect(requestBody.doiBatchHead.depositorName).toBe('John Doe');
    expect(requestBody.doiBatchHead.depositorEmail).toBe('johndoe@example.com');
  });

  it('should use default depositor information if none is provided', async () => {
    mockFetch.mockResolvedValue({
      ok: true,
      text: vi.fn().mockResolvedValue('<xml>Success</xml>'),
    });

    const xml = await strategy.generateXML(
      ContentRegistrationType.JournalArticle,
      {}
    );
    expect(xml).toBe('<xml>Success</xml>');
    expect(mockFetch).toHaveBeenCalledTimes(1);
    const fetchArgs = mockFetch.mock.calls[0];
    const requestBody = JSON.parse(fetchArgs[1].body);
    expect(requestBody.doiBatchHead.depositorName).toBe('Unknown Depositor');
    expect(requestBody.doiBatchHead.depositorEmail).toBe(
      'content-registration-form+unknown-depositor@crossref.org'
    );
  });
  it('inserts citations into the given XML when provided a valid references array', () => {
    const inputXML = `
      <root xmlns="http://www.crossref.org/schema/5.3.1">
        <journal_article>
          <title>Sample Article</title>
        </journal_article>
      </root>
    `;
    const references = [
      'First reference',
      'Second reference',
      'Third reference',
    ];

    const outputXML = insertCitationsToXML(inputXML, references);

    // Check that <citation_list> was appended
    expect(outputXML).toContain('<citation_list');

    // Check that all expected citations are present with the correct keys and text
    expect(outputXML).toContain('<citation key="ref0">');
    expect(outputXML).toContain(
      '<unstructured_citation>First reference</unstructured_citation>'
    );
    expect(outputXML).toContain('<citation key="ref1">');
    expect(outputXML).toContain(
      '<unstructured_citation>Second reference</unstructured_citation>'
    );
    expect(outputXML).toContain('<citation key="ref2">');
    expect(outputXML).toContain(
      '<unstructured_citation>Third reference</unstructured_citation>'
    );
  });

  it('returns original XML if references is not an array', () => {
    const inputXML = `
      <root xmlns="http://www.crossref.org/schema/5.3.1">
        <journal_article>
          <title>Sample Article</title>
        </journal_article>
      </root>
    `;
    // Passing a string instead of an array
    const references = 'Not an array reference';
    const outputXML = insertCitationsToXML(inputXML, references as any);
    expect(outputXML).toContain('<journal_article>');
    expect(outputXML).not.toContain('<citation_list>');
  });

  it('returns original XML if provided an empty references array', () => {
    const inputXML = `
      <root xmlns="http://www.crossref.org/schema/5.3.1">
        <journal_article>
          <title>Sample Article</title>
        </journal_article>
      </root>
    `;
    const references: string[] = [];
    const outputXML = insertCitationsToXML(inputXML, references);
    expect(outputXML).toContain('<journal_article>');
    expect(outputXML).not.toContain('<citation_list>');
  });

  it('returns original XML if citations already exist in the XML', () => {
    const inputXML = `
      <root xmlns="http://www.crossref.org/schema/5.3.1">
        <journal_article>
          <title>Sample Article</title>
          <citation_list>
            <citation key="existing">
              <unstructured_citation>Existing citation</unstructured_citation>
            </citation>
          </citation_list>
        </journal_article>
      </root>
    `;
    const references = ['New reference'];
    const outputXML = insertCitationsToXML(inputXML, references);
    // Should retain the existing citations without adding new ones
    expect(outputXML).toContain('<citation key="existing">');
    expect(outputXML).not.toContain('New reference');
  });

  it('throws an error if the input XML is invalid', () => {
    const invalidXML = `<root><journal_article></root>`;
    const references = ['Some reference'];
    expect(() => insertCitationsToXML(invalidXML, references)).toThrowError(
      /Invalid XML input/
    );
  });

  it('throws an error if <journal_article> is not found', () => {
    const inputXML = `<root />`;
    const references = ['Some reference'];
    expect(() => insertCitationsToXML(inputXML, references)).toThrowError(
      /<journal_article> tag not found/
    );
  });
});
