#!/bin/sh

find . -type f -path "./tests/unit/XmlGeneration/*" -name "*.xml" -not -name "invalid*" -print0 | xargs -n1 -0 xmllint --schema ./tests/unit/XmlGeneration/schemas/grant_id0.1.1.xsd --noout
