import { mount } from '@vue/test-utils';
import TheHeaderBar from '@/components/TheHeaderBar.vue';
import { authSymbol, getAuthService } from '@/statemachines/auth.machine';
import { AppServiceSymbol, getAppService } from '@/statemachines/app.machine';
import vuetifyInstance from '@/plugins/vuetify';
import { describe, it, expect, beforeEach } from 'vitest';
import { h } from 'vue';
import VuetifyWrapper from '../Support/VuetifyWrapper.vue';
import { store } from '@/store';
import { createTestingPinia } from '@pinia/testing';

let authServiceToProvide: ReturnType<typeof getAuthService>;
let appServiceToProvide: ReturnType<typeof getAppService>;
let provideWithExportedSymbol: Record<string, unknown>;

describe('HeaderBar.vue', () => {
  beforeEach(() => {
    store.appService = getAppService();
    authServiceToProvide = getAuthService();
    appServiceToProvide = getAppService();
    provideWithExportedSymbol = {
      [AppServiceSymbol as symbol]: appServiceToProvide,
      [authSymbol as symbol]: authServiceToProvide,
      loggedInUserEmail: null,
    };
  });

  it('should have a custom title and match snapshot', () => {
    const wrapper = mount(VuetifyWrapper, {
      global: {
        plugins: [vuetifyInstance, createTestingPinia()],
      },
      props: { title: 'Foobar' },
      slots: {
        default: h(TheHeaderBar),
      },
      provide: provideWithExportedSymbol,
    });
    expect(wrapper.html()).toContain('Foobar');
  });
});
