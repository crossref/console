import { mount } from '@vue/test-utils';
import LanguageMenu from '@/components/TheLanguageMenu.vue';
import vuetifyInstance from '@/plugins/vuetify';
import { getAppService, AppServiceSymbol } from '@/statemachines/app.machine';
import { getAuthService, authSymbol } from '@/statemachines/auth.machine';
import { describe, it, expect, beforeEach } from 'vitest';
import VuetifyWrapper from '../Support/VuetifyWrapper.vue';
import { h } from 'vue';

let authServiceToProvide: ReturnType<typeof getAuthService>;
let appServiceToProvide: ReturnType<typeof getAppService>;
let provideWithExportedSymbol: Record<string, unknown>;

describe('LanguageMenu.vue', () => {
  beforeEach(() => {
    authServiceToProvide = getAuthService();
    appServiceToProvide = getAppService();
    provideWithExportedSymbol = {
      [AppServiceSymbol as symbol]: appServiceToProvide,
      [authSymbol as symbol]: authServiceToProvide,
      loggedInUserEmail: null,
    };
  });
  const mockLocales = [
    {
      locale: 'en',
      title: 'English',
    },
    {
      locale: 'fr',
      title: 'Français',
    },
    {
      locale: 'es',
      title: 'Español',
    },
  ];
  function mountFunction(options = {}) {
    options = {
      data: function () {
        return {
          locales: mockLocales,
        };
      },
      ...options,
    };
    const wrapper = mount(VuetifyWrapper, {
      global: {
        plugins: [vuetifyInstance],
      },
      props: { title: 'Foobar' },
      provide: provideWithExportedSymbol,
      slots: {
        default: h(LanguageMenu),
      },
      ...options,
    });
    return wrapper;
  }

  it('should work', () => {
    const wrapper = mountFunction();
    // Fixes '[Vuetify] Unable to locate target [data-app] in "v-menu"'
    expect(wrapper.html()).toMatchSnapshot();
  });
  it('Has the right icon', () => {
    const wrapper = mountFunction();
    expect(wrapper.find('i.v-icon').exists()).toBe(true);
  });
  it('Has the default translation for button text', () => {
    const wrapper = mountFunction();
    const comp = wrapper.find('.v-btn__content');
    expect(comp.text()).toEqual('en');
  });
  it('Has a menu entry for each locale', async () => {
    const wrapper = mountFunction();
    await wrapper.find('button').trigger('click');
    const menuItems = wrapper.findAll('.v-list-item');
    expect(menuItems).toHaveLength(mockLocales.length);
  });
  // Probably a better candidate for an e2e test
  // it('Calls the changeLocale method with the new locale on menu item click', async () => {
  //   const wrapper = mountFunction()
  //   await wrapper.find('button').trigger('click')
  //   const menuItems = wrapper.findAll('.v-list-item')
  //   const newLocateItem = menuItems.at(2)
  //   await newLocateItem.trigger('click')
  //   expect(spy).toBeCalledTimes(1)
  //   expect(menuItems).toHaveLength(3)
  // })
});
