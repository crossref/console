import { mount } from '@vue/test-utils';
import AppLogoWorkmark from '@/components/AppLogoWordmark.vue';
import vuetifyInstance from '@/plugins/vuetify';
import { describe, it, expect } from 'vitest';

describe('AppLogoWordmark.vue', () => {
  it('renders the logo image', async () => {
    const wrapper = mount(AppLogoWorkmark, {
      global: {
        plugins: [vuetifyInstance],
      },
    });
    const img = wrapper.find('.v-img__img');
    expect(img.exists()).toBe(true);
  });
});
