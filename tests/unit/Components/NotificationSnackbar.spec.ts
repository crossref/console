import { mount } from '@vue/test-utils';
import NotificationSnackbar from '@/modules/NotificationsModule/NotificationSnackbar.vue';
import { AlertWithId } from '@/common/types';
import vuetifyInstance from '@/plugins/vuetify';
import { describe, it, expect } from 'vitest';

const SUCCESS_MESSAGE = 'Success message';
const ERROR_MESSAGE = 'Error message';
const successNotification: AlertWithId = {
  id: 1,
  msg: SUCCESS_MESSAGE,
  timeout: -1,
  level: 'success',
};
const errorNotification: AlertWithId = {
  id: 2,
  msg: ERROR_MESSAGE,
  timeout: -1,
  level: 'error',
};

describe('NotificationSnackbar.vue', () => {
  it('Shows a success notification message with the correct classes', async () => {
    const wrapper = mount(NotificationSnackbar, {
      global: {
        plugins: [vuetifyInstance],
      },
    });
    const snackbar = wrapper.find('.v-snack');
    const snackContent = wrapper.find('.v-snack__content');
    expect(snackContent.html()).toContain(SUCCESS_MESSAGE);
    expect(snackContent.classes()).toContain('text-center');
    expect(snackbar.classes()).toEqual(
      expect.arrayContaining(['v-snack--active', 'v-snack--top'])
    );
  });
  it('Shows an error notification message and action button, with the correct classes', async () => {
    const wrapper = mount(NotificationSnackbar, {
      global: {
        plugins: [vuetifyInstance],
      },
      props: {
        active: true,
        message: errorNotification.msg,
        level: errorNotification.level,
      },
    });

    const snackbar = wrapper.find('.v-snack');
    const snackContent = wrapper.find('.v-snack__content');
    const actionButton = wrapper.find('.v-snack__action button.v-snack__btn');
    expect(snackContent.html()).toContain(ERROR_MESSAGE);
    expect(snackContent.classes()).toContain('text-left');
    expect(snackbar.classes()).toEqual(
      expect.arrayContaining(['v-snack--active', 'v-snack--top'])
    );
    expect(actionButton.classes()).toContain('v-btn');
  });
});
