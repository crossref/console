import { mount } from '@vue/test-utils';
import CustomJsonForms from '@/forms/components/common/CustomJsonForms.vue';
import { describe, it, expect, assert } from 'vitest';
import vuetifyInstance from '@/plugins/vuetify';

const mountFn = (props: any = {}) => {
  // The wrapper is the encapsulation of the component under test
  // https://v1.test-utils.vuejs.org/api/wrapper/
  const wrapper = mount(CustomJsonForms, {
    // pass an empty jsonSchema and uiSchema by default
    props: {
      jsonSchema: {},
      uiSchema: {},
      ...props,
    },
    global: {
      plugins: [vuetifyInstance],
    },
  });

  return wrapper;
};
describe('CustomJsonForms.vue', () => {
  it('Calls onChange and emits update:formData and update:errors on JsonForms change event', async () => {
    const wrapper = mountFn();
    const mockData = { mockKey: 'mockValue' };
    const mockErrors = [{ message: 'Error message' }];

    // Find the JsonForms component and trigger the change event
    const jsonForms = wrapper.findComponent({ name: 'JsonForms' }); // Adjust the selector as needed
    await jsonForms.vm.$emit('change', { data: mockData, errors: mockErrors });

    // Assertions
    const updateFormDataEvents = wrapper.emitted('update:formData');
    if (updateFormDataEvents && updateFormDataEvents.length > 1) {
      expect(updateFormDataEvents[1]).toEqual([mockData]);
    } else {
      throw new Error(
        'The "update:formData" event was not emitted once or more as expected.'
      );
    }
    const updateErrorEvents = wrapper.emitted('update:errors');
    if (updateErrorEvents && updateErrorEvents.length > 1) {
      expect(updateErrorEvents[1]).toEqual([mockErrors]);
    } else {
      throw new Error(
        'The "update:errors" event was not emitted once or more as expected.'
      );
    }
    const changeEvents = wrapper.emitted('change');
    if (changeEvents && changeEvents.length > 1) {
      expect(changeEvents[1]).toEqual([{ data: mockData, errors: mockErrors }]);
    } else {
      throw new Error(
        'The "update:errors" event was not emitted once or more as expected.'
      );
    }
  });
  it('renders primative fields', () => {
    const wrapper = mountFn({
      jsonSchema: {
        type: 'object',
        properties: {
          username: {
            type: 'string',
            title: 'Username',
          },
          age: {
            type: 'number',
            title: 'Age',
          },
          isActive: {
            type: 'boolean',
            title: 'Active User',
          },
        },
      },
      uiSchema: {
        type: 'VerticalLayout',
        elements: [
          {
            type: 'Control',
            scope: '#/properties/username',
            label: 'Username',
          },
          {
            type: 'Control',
            scope: '#/properties/age',
            label: 'Age',
          },
          {
            type: 'Control',
            scope: '#/properties/isActive',
            label: 'Active User',
          },
        ],
      },
    });

    expect(wrapper.html()).toContain('id="#/properties/username"');
    expect(wrapper.html()).toContain('id="#/properties/age"');
    expect(wrapper.html()).toContain('id="#/properties/isActive"');
  });
});
