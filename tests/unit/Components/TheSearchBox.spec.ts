import { mount, VueWrapper } from '@vue/test-utils';
import SearchBox from '@/components/SearchBox.vue';
import { getSearchService, searchSymbol } from '@/statemachines/search.machine';
import vuetifyInstance from '@/plugins/vuetify';
import { describe, it, expect, beforeEach } from 'vitest';
import { h } from 'vue';
import VuetifyWrapper from '../Support/VuetifyWrapper.vue';

const toggleSearchBox = async (wrapper: VueWrapper<any>) => {
  const input = wrapper.find('input');
  await input.trigger('focus');
};

const updateSearchText = async (
  wrapper: VueWrapper<any>,
  searchText: string
) => {
  const input = wrapper.find('input');
  await toggleSearchBox(wrapper);
  await input.setValue(searchText);
};

describe('SearchBox.vue', () => {
  let searchServiceToProvide: ReturnType<typeof getSearchService>;
  let provideWithExportedSymbol: Record<string, unknown>;
  searchServiceToProvide = getSearchService();

  function mountFunction(options = {}) {
    const wrapper = mount(VuetifyWrapper, {
      global: {
        plugins: [vuetifyInstance],
      },
      props: { title: 'Foobar' },
      provide: provideWithExportedSymbol,
      slots: {
        default: h(SearchBox),
      },
      ...options,
    });
    return wrapper;
  }

  function mountAttachedToDOM() {
    /*
    When triggering 'focus' with jsdom v16.4.0+, the component must be mounted with the
    attachTo option, see https://vue-test-utils.vuejs.org/api/wrapper/trigger.html
     */
    const div = document.createElement('div');
    div.id = 'root';
    document.body.appendChild(div);
    const wrapper = mountFunction({
      attachTo: '#root',
    });
    return wrapper;
  }

  const getComponentRoot = (wrapper: VueWrapper) => {
    return wrapper.findComponent(SearchBox);
  };

  beforeEach(() => {
    searchServiceToProvide = getSearchService();
    provideWithExportedSymbol = {
      [searchSymbol as symbol]: searchServiceToProvide,
    };
  });
  it('should work', () => {
    const wrapper = mountFunction();
    expect(getComponentRoot(wrapper).html()).toMatchSnapshot();
  });
  it('Has the right icon', () => {
    const wrapper = mountFunction();
    expect(getComponentRoot(wrapper).find('i.v-icon').exists()).toBe(true);
  });
  it('Has the correct initial classnames', () => {
    const wrapper = mountFunction();
    expect(getComponentRoot(wrapper).classes()).toEqual(
      expect.arrayContaining(['expanding-search', 'closed'])
    );
  });
  it('Has the correct initial (blank) search text', () => {
    const wrapper = mountFunction();
    /*
       VTU wrapper is typed as a generic Vue instance and can't infer the types from the component
       when it's an SFC, so we must declare the component - wrapper.vm - as any
       https://github.com/vuejs/vue-test-utils/issues/255
     */
    expect((getComponentRoot(wrapper).vm as any).searchText).toEqual('');
  });
  it('Has the correct classnames on click to open', async () => {
    const wrapper = mountAttachedToDOM();
    const component = getComponentRoot(wrapper);
    await toggleSearchBox(component);
    expect(component.classes()).not.toContain('closed');
    /*
    https://vue-test-utils.vuejs.org/api/options.html#attachto
    When attaching to the DOM, you should call wrapper.destroy() at the end of your test to remove
    the rendered elements from the document and destroy the component instance.
     */
    wrapper.unmount();
  });
  it.only('Applies the closed class on blur, when search text is empty', async () => {
    const wrapper = mountAttachedToDOM();
    const component = getComponentRoot(wrapper);
    await toggleSearchBox(wrapper);
    const input = component.find('input');
    await input.trigger('blur');
    expect(component.classes()).toContain('closed');
  });
  it('Does not apply the closed class on blur, when the search text is not empty', async () => {
    const wrapper = mountAttachedToDOM();
    const newSearchText =
      'Some other new search text in Korean 한국어로 된 다른 새로운 검색 텍스트';
    await updateSearchText(wrapper, newSearchText);
    const component = getComponentRoot(wrapper);
    expect((component.vm as any).searchText).toEqual(newSearchText);
    const input = wrapper.find('input');
    input.trigger('blur');
    expect(component.classes()).not.toContain('closed');
  });
});
