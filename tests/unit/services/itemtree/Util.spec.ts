import { describe, expect, it, test } from 'vitest';
import journal_article_form_data_1 from '../../../resources/json.formdata/journal_article_1.json';
import * as Util from '@/itemTree/mapping/Util';
import { Abstract } from '@/models/Abstract';
import {
  getPublicationDates,
  transformIdentifiers,
} from '@/itemTree/mapping/Util';
import contributor_relationships_1 from '../../../resources/json.itemtree/components/contributor_relationships_1.json';

export const compareObjects = (result, expected) => {
  expect(JSON.stringify(result, null, 4)).toEqual(
    JSON.stringify(expected, null, 4)
  );
};

describe('ItemTree Object Mapper Utility Functions', () => {
  it('converts journal article DOI to an Identifiers array', () => {
    const formData = { journal: { doi: '10.1000/testdoi' } };
    const identifiers = transformIdentifiers(formData);

    expect(identifiers.length).toBe(1);
    expect(identifiers[0].toString()).toEqual(
      'https://doi.org/10.1000/testdoi'
    );
  });

  it('returns empty Identifiers array for missing DOI in journal article form data', () => {
    const formData = { journal: {} };
    const identifiers = transformIdentifiers(formData);

    expect(identifiers).toEqual([]);
  });
  test('maps journal article form data to expected contributor relationships structure', () => {
    const result = Util.getContributorRelationships(
      journal_article_form_data_1
    );
    compareObjects(result, contributor_relationships_1);
  });
  test('extracts DOI from journal article form data correctly', () => {
    const result = Util.getArticleDoi(journal_article_form_data_1);
    const expected = '10.5555/12345678';
    expect(result).toEqual(expected);
  });

  test('accurately transforms publication dates from journal article form data', () => {
    const expectedPublishedOnline = {
      'date-parts': [2008, 8, 13],
      'date-time': '2008-08-13T00:00:00.000Z',
      timestamp: 1218585600000,
      source: 'date-parts',
    };

    const expectedPublishedPrint = {
      'date-parts': [2008, 8, 14],
      'date-time': '2008-08-14T00:00:00.000Z',
      timestamp: 1218672000000,
      source: 'date-parts',
    };

    const result = getPublicationDates(journal_article_form_data_1);

    // Verifying the transformed publication dates
    const publishedOnlineValue = Util.getDatePublishedOnline(
      journal_article_form_data_1
    );
    const publishedPrintValue = Util.getDatePublishedInPrint(
      journal_article_form_data_1
    );
    expect(publishedOnlineValue).toEqual([expectedPublishedOnline]);

    expect(publishedPrintValue).toEqual([expectedPublishedPrint]);
  });

  it('correctly wraps abstracts with JATS tags when necessary', () => {
    const formData = {
      article: {
        abstract: [
          { abstract: 'Sample abstract without tags', language: 'en' },
          {
            abstract: '<jats:p>Already wrapped abstract</jats:p>',
            language: 'en',
          },
        ],
      },
    };

    const expected = [
      new Abstract('<jats:p>Sample abstract without tags</jats:p>', 'en'),
      new Abstract('<jats:p>Already wrapped abstract</jats:p>', 'en'),
    ];

    const result = Util.getAbstract(formData);
    compareObjects(result, expected);
  });

  it('handles undefined and null abstracts gracefully', () => {
    const formData = {
      article: {
        abstract: [
          { abstract: undefined, language: 'en' },
          { abstract: null, language: 'fr' },
        ],
      },
    };

    const expected = [
      Abstract.create(null, 'en'),
      Abstract.create(null, 'fr'),
    ].filter((abstract) => abstract !== null);

    const result = Util.getAbstract(formData);
    compareObjects(result, expected);
  });
});

describe('Abstract Factory Method', () => {
  it('creates an Abstract instance with valid abstract text', () => {
    const value = 'Valid abstract content';
    const language = 'en';
    const abstract = Abstract.create(value, language);
    expect(abstract).toBeInstanceOf(Abstract);
    expect(abstract?.value).toBe('<jats:p>Valid abstract content</jats:p>');
    expect(abstract?.language).toBe('en');
  });

  it('returns null when abstract text is null', () => {
    const value = null;
    const language = 'en';
    const abstract = Abstract.create(value, language);
    expect(abstract).toBeNull();
  });

  it('returns null when abstract text is an empty string', () => {
    const value = '';
    const language = 'en';
    const abstract = Abstract.create(value, language);
    expect(abstract).toBeNull();
  });

  it('returns null when abstract text is only whitespace', () => {
    const value = '   ';
    const language = 'en';
    const abstract = Abstract.create(value, language);
    expect(abstract).toBeNull();
  });

  // Test to ensure the getAbstract function properly filters out invalid abstracts
  it('filters out invalid abstract instances', () => {
    const formData = {
      article: {
        abstract: [
          { abstract: 'Valid abstract content', language: 'en' },
          { abstract: '', language: 'en' },
          { abstract: '   ', language: 'en' },
          { abstract: null, language: 'en' },
        ],
      },
    };

    const expected = [
      new Abstract('<jats:p>Valid abstract content</jats:p>', 'en'),
    ];

    const result = Util.getAbstract(formData);
    compareObjects(result, expected);
  });
});

describe('Date Processing in ItemTree Object Mapper Utility', () => {
  test.each([
    // Standard date
    [
      '2008-08-14',
      {
        'date-parts': [2008, 8, 14],
        'date-time': '2008-08-14T00:00:00.000Z',
        timestamp: new Date('2008-08-14T00:00:00.000Z').getTime(),
        source: 'date-parts',
      },
    ],
    // Leap year
    [
      '2012-02-29',
      {
        'date-parts': [2012, 2, 29],
        'date-time': '2012-02-29T00:00:00.000Z',
        timestamp: new Date('2012-02-29T00:00:00.000Z').getTime(),
        source: 'date-parts',
      },
    ],
    // Date before a known leap second
    [
      '2016-12-30',
      {
        'date-parts': [2016, 12, 30],
        'date-time': '2016-12-30T00:00:00.000Z',
        timestamp: new Date('2016-12-30T00:00:00.000Z').getTime(),
        source: 'date-parts',
      },
    ],
    // Date of a known leap second
    [
      '2016-12-31',
      {
        'date-parts': [2016, 12, 31],
        'date-time': '2016-12-31T00:00:00.000Z',
        timestamp: new Date('2016-12-31T00:00:00.000Z').getTime(),
        source: 'date-parts',
      },
    ],
    // Immediately after a leap second
    [
      '2017-01-01',
      {
        'date-parts': [2017, 1, 1],
        'date-time': '2017-01-01T00:00:00.000Z',
        timestamp: new Date('2017-01-01T00:00:00.000Z').getTime(),
        source: 'date-parts',
      },
    ],
  ])(
    'derives correct date parts, date-time, and timestamp from "%s"',
    (inputDate, expected) => {
      const result = Util.getDateParts(inputDate);

      // Now includes direct comparison of timestamps along with other components
      expect(result).toEqual(expected);
    }
  );
});
