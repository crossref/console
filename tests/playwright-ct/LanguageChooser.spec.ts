import { test, expect } from '@playwright/experimental-ct-vue';
import JsonFormsTestComponent from './JsonFormsTestComponent.vue';

// JSON Schema for the form data structure
const schema = {
  type: 'object',
  properties: {
    article: {
      type: 'object',
      properties: {
        language: {
          type: 'string',
        },
      },
    },
  },
};

// ui schema for the form rendering structure
const uischema = {
  type: 'VerticalLayout',
  elements: [
    {
      type: 'Control',
      scope: '#/properties/language',
      i18n: 'article_abstract_language',
      options: {
        autocomplete: true,
        autoSelectFirst: true,
        'x-rendering-hint': 'languageChooser',
      },
    },
  ],
};
const data = {};

test.use({});

test('Language list is shown on click', async ({ mount }) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
  });
  const page = component.page();
  await component.locator('.v-field__input').click();
  await expect(page.getByText('avesta', { exact: true })).toBeVisible();
  await expect(page.getByText('Abkhaz (ab)')).toBeVisible();
});

test('Language is selected on click', async ({ mount }) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
  });
  const page = component.page();
  const input = component.locator('.v-field__input');
  await input.click();
  await page.getByText('avesta', { exact: true }).click();
  await expect(
    component.getByLabel('Language of abstract (optional)', { exact: true })
  ).toHaveValue('Avestan (ae)');
});

test('Language is selected on enter', async ({ mount }) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
  });
  const page = component.page();
  const input = component.locator('.v-field__input');
  await input.click();
  await page.keyboard.press('ArrowDown');
  await page.keyboard.press('ArrowDown');
  await page.keyboard.press('Enter');

  await expect(
    component.getByLabel('Language of abstract (optional)', { exact: true })
  ).toHaveValue('Abkhaz (ab)');
});

test('Language is cleared on click of clear icon', async ({ mount }) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
  });
  const page = component.page();
  const input = component.locator('.v-field__input');
  await input.click();
  await page.getByText('avesta', { exact: true }).click();
  await input.hover();
  await component
    .getByLabel('Clear Language of abstract (')
    .waitFor({ state: 'visible' });
  await component.getByLabel('Clear Language of abstract (').click();
  await expect(
    component.getByLabel('Language of abstract (optional)', { exact: true })
  ).toHaveValue('');
});
