import { test, expect } from '@playwright/experimental-ct-vue';
import JsonFormsTestComponent from './JsonFormsTestComponent.vue';

// JSON Schema for the form data structure
const schema = {
  type: 'object',
  properties: {
    name: {
      type: 'string',
    },
    art: {
      type: 'object',
      properties: {
        contributors: {
          type: 'array',
          items: {
            title: 'Person',
            type: 'object',
            properties: {
              givenName: {
                i18n: 'person.givenName',
                type: 'string',
              },
              familyName: {
                i18n: 'person.familyName',
                type: 'string',
              },
              suffix: {
                i18n: 'person.suffix',
                type: 'string',
              },
              affiliation: {
                title: 'Affiliation',
                type: 'object',
                properties: {
                  institution: {
                    i18n: 'person.affiliation.institution',
                    type: 'string',
                  },
                  ror: {
                    i18n: 'person.affiliation.ror',
                    type: 'string',
                  },
                },
              },
              orcid: {
                i18n: 'person.ORCID',
                type: 'string',
                pattern:
                  '^[Hh][Tt][Tt][Pp][Ss]://orcid.org/\\d{4}-\\d{4}-\\d{4}-\\d{3}[0-9xX]',
              },
              role: {
                i18n: 'person.role',
                type: 'string',
                enum: [
                  'first-author',
                  'additional-author',
                  'editor',
                  'chair',
                  'reviewer',
                  'review-assistant',
                  'stats-reviewer',
                  'reviewer-external',
                  'reader',
                  'translator',
                ],
              },
              asequence: {
                type: 'string',
                i18n: 'contentRegistrationForm_article_contributor_sequence',
                enum: ['first', 'additional'],
              },
              sequence: {
                type: 'string',
              },
            },
            required: ['role', 'familyName', 'sequence'],
          },
        },
      },
    },
  },
};

// ui schema for the form rendering structure
const uischema = {
  type: 'VerticalLayout',
  elements: [
    {
      type: 'Control',
      scope: '#/properties/art/properties/contributors',
    },
  ],
};
const data = {
  art: {
    contributors: [
      {
        givenName: 'bob',
      },
    ],
  },
};

test.use({});

test('Sequence is changed when selecting a new first author', async ({
  mount,
}) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
  });
  const page = component.page();

  await expect(component).toContainText('Role');
  await component
    .locator('header')
    .filter({ hasText: 'Contributors' })
    .getByLabel('Add new')
    .click();
  await expect(
    component.locator('[id="\\#\\/properties\\/sequence2-input"]')
  ).toHaveValue('additional');
  await component.locator('[id="\\#\\/properties\\/role2"]').click();
  await page.getByText('first-author').click();
  await page.getByRole('button', { name: 'OK' }).click();
  await expect(component.getByLabel('Suffix')).toBeEmpty();
  await expect(
    component.locator('[id="\\#\\/properties\\/role2-input"]')
  ).toHaveValue('first-author');
  await expect(
    component.locator('[id="\\#\\/properties\\/sequence2-input"]')
  ).toHaveValue('first');
});

test('Neither Sequence nor Role is changed when rejecting a change of first author', async ({
  mount,
}) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
  });
  const page = component.page();

  await expect(component).toContainText('Role');
  await component
    .locator('header')
    .filter({ hasText: 'Contributors' })
    .getByLabel('Add new')
    .click();
  await expect(
    component.locator('[id="\\#\\/properties\\/sequence2-input"]')
  ).toHaveValue('additional');
  await component.locator('[id="\\#\\/properties\\/role2"]').click();
  await page.getByText('first-author').click();
  await page.getByRole('button', { name: 'CANCEL' }).click();
  await expect(component.getByLabel('Suffix')).toBeEmpty();
  await expect(
    component.locator('[id="\\#\\/properties\\/role2-input"]')
  ).toHaveValue('additional-author');
  await expect(
    component.locator('[id="\\#\\/properties\\/sequence2-input"]')
  ).toHaveValue('additional');
});
