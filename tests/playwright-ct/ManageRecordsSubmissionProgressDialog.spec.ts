import { test, expect } from '@playwright/experimental-ct-vue';
import ManageRecordsSubmissionProgressDialog from '@/components/ManageRecordsSubmissionProgressDialog.vue';

test.use({});

test('Progress dialog is shown during XML generation', async ({ mount }) => {
  const component = await mount(ManageRecordsSubmissionProgressDialog, {
    props: {
      submissionState: 'generateXML',
    },
  });
  const page = component.page();
  await expect(page.getByText('Generating XML...')).toBeVisible();
});

test('Progress dialog is shown during record deposit', async ({ mount }) => {
  const component = await mount(ManageRecordsSubmissionProgressDialog, {
    props: {
      submissionState: 'depositRecord',
    },
  });
  const page = component.page();
  await expect(page.getByText('Depositing metadata...')).toBeVisible();
});

test('Progress dialog is not shown in other states', async ({ mount }) => {
  const component = await mount(ManageRecordsSubmissionProgressDialog, {
    props: {
      submissionState: 'checkCredentials',
    },
  });
  const page = component.page();
  await expect(page.getByRole('listbox')).not.toBeVisible();
});
