import { test, expect } from '@playwright/experimental-ct-vue';
import GetHelpButton from "@/components/GetHelpButton.vue";

test.use({});

const targetUrl = 'not-a-real-url';

test('Get help button works', async ({ mount }) => {
  const component = await mount(GetHelpButton, {
    props: {
      url: targetUrl,
    },
  });
  const page = component.page();
  const newTabPromise = page.waitForEvent('popup');
  await page.getByRole('link', { name: 'Get help (Opens in new tab)' }).click();
  const newTab = await newTabPromise;
  await newTab.waitForLoadState();

  await expect(newTab).toHaveURL(targetUrl);
});
