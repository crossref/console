import { test, expect } from '@playwright/experimental-ct-vue';
import DepositSuccessMessageJournalArticle from '@/components/EditRecordForm/DepositSuccessMessageJournalArticle.vue';
import { URL_SIMPLE_TEXT_QUERY, URL_SIMPLE_TEXT_QUERY_FAQ } from "@/constants/urls";
import { DepositedMetadataRecord } from '@/common/types';

const TEST_RECORD: DepositedMetadataRecord = {
  recordId: 'ca069826-e09f-4faf-bc1e-fba7041283b0',
  recordType: 'journal-article',
  recordExtension: 'json',
  schemaName: 'journal-schema',
  schemaVersion: '1.0.0',
  formData: {
    journal: {
      titles: {
        fullTitle: 'Test Journal Title',
      },
    },
    issue: {
      issue: '42',
    },
  },
  depositedAt: new Date(),
  depositResponse: {
    statusCode: 200,
    status: 'success',
    submissionId: 1234567890,
    deferredCitationsDiagnosticId: 9876543210,
    doi: '10.5555/1234567890-987654321',
    recordCount: 1,
    warningCount: 0,
    failureCount: 0,
    successCount: 1,
    message: 'Submission successful',
    body: 'Detailed response body here',
  },
};

test.use({});

test('Deposit result is shown', async ({ mount }) => {
  const component = await mount(DepositSuccessMessageJournalArticle, {
    props: {
      record: TEST_RECORD,
    },
  });

  // Check for general success message
  await expect(component).toContainText(
    'Journal Article record successfully submitted.'
  );

  // Check that submission ID is displayed
  await expect(component).toContainText('Submission ID 1234567890');

  // Check that defered citatons submission ID is displayed
  await expect(component).toContainText('Use this link to review the reference matching results: 9876543210');

  // Check that DOI is displayed
  await expect(component).toContainText('DOI 10.5555/1234567890-987654321');

  // Check that the submission link is displayed
  await expect(component).toContainText(
    'Use this link to see more information on your submission in the Crossref Admin tool: 1234567890'
  );

  // Test link attributes and URL for submission ID
  const submissionLink = component.locator('a:text-is("1234567890")');
  await expect(submissionLink).toHaveAttribute('target', '_blank');
  await expect(submissionLink).toHaveAttribute('rel', 'noopener noreferrer');
  await expect(submissionLink).toHaveAttribute(
    'href',
    `https://test.crossref.org/servlet/submissionAdmin?sf=detail&submissionID=${TEST_RECORD.depositResponse.submissionId}`
  );
});
