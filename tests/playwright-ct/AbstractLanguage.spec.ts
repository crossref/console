import { test, expect } from '@playwright/experimental-ct-vue';
import JsonFormsTestComponent from './JsonFormsTestComponent.vue';
import journalArticleSchema from '../../src/forms/schemas/json/journalArticleForm/journalArticleForm.schema.json' assert { type: 'json' };

// JSON Schema for the form data structure
const schema = {
  type: 'object',
  properties: {
    article: {
      type: 'object',
      properties: {
        abstract: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              abstract: {
                type: 'string',
              },
              language: {
                type: 'string',
                default: 'en',
                uniqueValue: true,
              },
            },
          },
          i18n: 'contentRegistrationForm_article_abstracts',
        },
      },
    },
  },
};

// UI schema for the form rendering structure
const uischema = {
  type: 'VerticalLayout',
  elements: [
    {
      type: 'Control',
      scope: '#/properties/article/properties/abstract',
      options: {
        detail: {
          type: 'VerticalLayout',
          elements: [
            {
              type: 'Control',
              scope: '#/properties/abstract',
              options: {
                multi: true,
              },
            },
            {
              type: 'Control',
              scope: '#/properties/language',
              i18n: 'article_abstract_language',
              options: {
                autocomplete: true,
                autoSelectFirst: true,
                'x-rendering-hint': 'languageChooser',
              },
            },
          ],
        },
      },
    },
  ],
};

const data = {
  article: {
    abstract: [
      {
        abstract: '<jats:p>Here is a simple abstract</jats:p>',
        language: 'en',
      },
      {
        abstract: 'Ĉi tiu simpla mesaĝo estas efektive en Esperanto.',
        language: 'en',
      },
    ],
  },
};

test.use({});

// Test to check if error is shown for non-unique abstract languages
test('Displays error for duplicate abstract languages', async ({ mount }) => {
  let errors = [];
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
    on: {
      'update:errors': (e) => {
        errors = e;
      },
    },
  });

  await expect.poll(() => errors).toHaveLength(2);
  await expect(component).toContainText(
    'Please enter a unique language for each abstract.'
  );
});

// Test to check if unique abstract languages pass validation
test('Passes validation with unique abstract languages', async ({ mount }) => {
  let errors = [];
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
    on: {
      'update:errors': (e) => {
        errors = e;
      },
    },
  });
  const page = component.page();

  await expect(component).toContainText(
    'Please enter a unique language for each abstract.'
  );
  await expect.poll(() => errors).toHaveLength(2);
  await component
    .locator('div')
    .filter({ hasText: /^English \(en\)$/ })
    .first()
    .click();
  await page.getByText('Esperanto (eo)').click();
  await expect.poll(() => errors).toHaveLength(0);
});
