import { fetchConfigFromEnv } from '@/utils/fetchConfigFromEnv';
import { ref, readonly, Ref } from 'vue';

type ConfigObject = { [key: string]: any };

/**
 * Fetches environment variables that start with a specified prefix,
 * and returns a readonly ref of a nested configuration object.
 *
 * @param {string} prefix The prefix of the environment variables to include in the configuration object.
 * @returns {Ref<ConfigObject>} The configuration object as a readonly ref.
 */
const useConfig = (prefix: string): Ref<ConfigObject> => {
  const config: ConfigObject = fetchConfigFromEnv(prefix);
  const configRef = ref<ConfigObject>(config);
  return readonly(configRef);
};

export default useConfig;
