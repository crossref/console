// server.js

import { Model, Server, Response } from 'miragejs';
const defaultRoles = [
  'psychoceramics1',
  'psychoceramics2',
  'psychoceramics3',
  'psychoceramics4',
  'psychoceramics5',
];
const defaultBearerToken =
  'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDE0NjE3MDIsInVzZXJfbmFtZSI6InJzYyIsImp0aSI6IjY1NjhkM2JlLWYxMjAtNGQ0OS04NmM3LWQ3OWUzMDJlMTI1YiIsImNsaWVudF9pZCI6ImNzIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl19.7c1qDklSI_-bzxB1i499uOmyen-aopPjEWrddivDcXs';

function parseRequest(request) {
  let attrs, token;
  if (typeof request.requestHeaders.authorization === 'string') {
    token = request.requestHeaders.authorization;
  }
  if (
    request.requestHeaders['Content-Type'] ===
    'application/x-www-form-urlencoded'
  ) {
    const urlEncodedParts = request.requestBody.split('&');

    attrs = urlEncodedParts.reduce((a, urlEncodedPart) => {
      const [key, value] = urlEncodedPart.split('=');
      a[key] = decodeURIComponent(value.replace(/\+/g, ' '));
      return a;
    }, {});
  } else {
    attrs = JSON.parse(request.requestBody);
  }
  const username = attrs.usr;
  const password = attrs.pwd;
  const role = attrs.role;
  return {
    username,
    password,
    role,
    token,
    attrs,
  };
}

function checkCredentials(username, password) {
  if (username && password === 'badpass') {
    return false;
  }
  if (password === 'undefined') {
    return false;
  }
  return true;
}

export function makeServer({
  environment = 'development',
  urlPrefix = '',
} = {}) {
  const server = new Server({
    environment,

    models: {
      todo: Model,
      user: Model,
      role: Model,
    },

    seeds(server) {
      for (let roleId = 0; roleId < 5; roleId++) {
        server.create('role', {
          roleId: roleId,
          roleName: `psychoceramics${roleId}`,
        });
      }
    },

    routes() {
      // this.namespace = 'api'
      this.urlPrefix = urlPrefix;

      this.post(
        '/servlet/login',
        (schema, request) => {
          const parsedRequest = parseRequest(request);
          const { username, password, role } = parsedRequest;
          let roles = defaultRoles;
          if (username.match(/noroles|ams/)) {
            roles = [];
          }
          if (username.match(/onerole/)) {
            roles = ['onerole'];
          }
          if (!checkCredentials(username, password)) {
            return new Response(
              401,
              {},
              {
                success: false,
                errorMessage:
                  'Wrong credentials. Incorrect username or password.',
                authenticated: false,
                authorised: false,
              }
            );
          }
          // User has only one role - automatically login and redirect
          if (username === 'admin' && password === '685277343!') {
            return new Response(
              200,
              {
                authorization: defaultBearerToken,
              },
              {
                success: true,
                message: 'Authorised',
                redirect: null,
                roles: ['psychoceramics1andOnly1'],
                authenticated: true,
                authorised: false,
              }
            );
          }
          // Role Authorisation requested
          if (typeof role === 'string') {
            // if (typeof token !== 'string' || token.length < 1) {
            //   return new Response(
            //     401,
            //     {},
            //     {
            //       success: false,
            //       errorMessage: 'Login error. No token provided.',
            //       authenticated: true,
            //       authorised: false
            //     }
            //   )
            // }
            if (username.match(/badrole/)) {
              return new Response(
                401,
                {
                  authorization: defaultBearerToken,
                },
                {
                  success: false,
                  errorMessage: `User '${username}' cannot assume specified role '${role}'`,
                  redirect: null,
                  authenticated: true,
                  authorised: false,
                }
              );
            }
            if (!checkCredentials(username, password)) {
              return new Response(
                401,
                {},
                {
                  success: false,
                  errorMessage: 'Login error. No token provided.',
                  authenticated: false,
                  authorised: false,
                }
              );
            }
            return new Response(
              200,
              {
                authorization: defaultBearerToken,
              },
              {
                success: true,
                message: 'Authorised',
                redirect: null,
                authenticated: true,
                authorised: true,
              }
            );
          }
          if (roles.length < 1) {
            return new Response(
              200,
              {
                authorization: defaultBearerToken,
              },
              {
                success: false,
                message: 'Authenticated',
                roles: roles,
              }
            );
          }
          return new Response(
            401,
            {
              authorization: defaultBearerToken,
            },
            {
              success: false,
              message: 'Authenticated',
              roles: roles,
              authenticated: true,
              authorised: false,
            }
          );
        },
        { timing: 600 }
      );

      this.post('/authorise', (schema, request) => {
        const parsedRequest = parseRequest(request);
        const { username, password, role, token } = parsedRequest;
        if (!checkCredentials(username, password)) {
          return new Response(
            200,
            {},
            {
              success: false,
              errorMessage: 'Login error. Please enter correct login details',
            }
          );
        }
        if (typeof token !== 'string' || token !== defaultBearerToken) {
          return new Response(
            200,
            {},
            {
              success: false,
              errorMessage: 'Login error. Invalid token.',
            }
          );
        }
        if (typeof role !== 'string' || role.length < 1) {
          return new Response(
            200,
            {},
            {
              success: false,
              errorMessage:
                'Role authorization error - an invalid role name was provided',
            }
          );
        }
        return new Response(
          200,
          {
            authorization: defaultBearerToken,
          },
          { success: true, message: 'Authorised', redirect: null }
        );
      });

      this.passthrough('https://test.crossref.org/*');
    },
  });

  return server;
}
