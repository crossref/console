import axios from 'axios';
import { Ref } from 'vue';

type Token = Ref<string | null>;

// Create a group if it doesn't exist
export const createGroup = async (
  token: Ref<string | null>,
  groupName: string,
  keycloakBaseUrl: string,
  realmName: string
) => {
  const groupsResponse = await axios.get(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups`,
    {
      headers: { Authorization: `Bearer ${token.value}` },
    }
  );
  const existingGroup = groupsResponse.data.find(
    (group: any) => group.name === groupName
  );

  if (existingGroup) {
    return existingGroup;
  } else {
  }

  const requestBody = {
    name: groupName,
  };

  const response = await axios.post(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups`,
    requestBody,
    { headers: { Authorization: `Bearer ${token.value}` } }
  );
  // Fetch the created group details using the group name
  const createdGroupResponse = await axios.get(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups`,
    {
      params: { search: groupName },
      headers: { Authorization: `Bearer ${token.value}` },
    }
  );
  const createdGroup = createdGroupResponse.data.find(
    (group: any) => group.name === groupName
  );

  if (!createdGroup) {
    console.error(`Failed to retrieve created group: ${groupName}`);
    return null;
  }

  return createdGroup;
};

// Create nested groups
export const createNestedGroups = async (
  token: Token,
  groupsData: any[],
  keycloakBaseUrl: string,
  realmName: string
): Promise<any[]> => {
  const createdGroups: any[] = [];

  for (const groupData of groupsData) {
    const { name, children } = groupData;
    // Create parent group
    const parentGroup = await createGroup(
      token,
      name,
      keycloakBaseUrl,
      realmName
    );

    // Create nested child groups
    for (const childData of children) {
      // Associate child group with parent group
      await addChildGroupToParentGroup(
        token,
        childData.name,
        parentGroup.id,
        childData.memberId,
        keycloakBaseUrl,
        realmName
      );
    }
  }

  return createdGroups;
};

// Add a child group to a parent group
const addChildGroupToParentGroup = async (
  token: Token,
  childGroupId: string,
  parentGroupId: string,
  memberId: number,
  keycloakBaseUrl: string,
  realmName: string
) => {
  try {
    const requestBody = {
      name: childGroupId,
      attributes: {
        memberId: [memberId],
      },
    };

    await axios.post(
      `${keycloakBaseUrl}/admin/realms/${realmName}/groups/${parentGroupId}/children`,
      requestBody,
      {
        headers: { Authorization: `Bearer ${token.value}` },
      }
    );
  } catch (error) {
    console.error(
      `Failed to add child group (${childGroupId}) to parent group (${parentGroupId}):`,
      error
    );
  }
};

// Fetch all users
export const fetchAllUsers = async (
  token: Ref<string | null>,
  keycloakBaseUrl: string,
  realmName: string
): Promise<any[]> => {
  try {
    const response = await axios.get(
      `${keycloakBaseUrl}/admin/realms/${realmName}/users`,
      {
        headers: { Authorization: `Bearer ${token.value}` },
      }
    );
    return response.data;
  } catch (error) {
    console.error('Failed to fetch users:', error);
    return [];
  }
};

// Fetch all groups
export const fetchAllGroups = async (
  token: Ref<string | null>,
  keycloakBaseUrl: string,
  realmName: string
): Promise<any[]> => {
  try {
    const response = await axios.get(
      `${keycloakBaseUrl}/admin/realms/${realmName}/groups`,
      {
        headers: { Authorization: `Bearer ${token.value}` },
      }
    );
    return response.data;
  } catch (error) {
    console.error('Failed to fetch groups:', error);
    return [];
  }
};

// Delete all users
export const deleteAllUsers = async (
  token: Ref<string | null>,
  keycloakBaseUrl: string,
  realmName: string
): Promise<void> => {
  const users = await fetchAllUsers(token, keycloakBaseUrl, realmName);

  for (const user of users) {
    if (user.username === 'crossref-sync') {
      continue;
    }
    await axios.delete(
      `${keycloakBaseUrl}/admin/realms/${realmName}/users/${user.id}`,
      {
        headers: { Authorization: `Bearer ${token.value}` },
      }
    );
  }
};

// Get group by name
const getGroupByName = async (
  token: Token,
  keycloakBaseUrl: string,
  realmName: string,
  name: string
) => {
  const { data } = await axios.get(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups`,
    {
      headers: { Authorization: `Bearer ${token.value}` },
      params: { search: name },
    }
  );

  return data.find((group: any) => group.name === name);
};

// Get child groups
const getChildGroups = async (
  token: Token,
  keycloakBaseUrl: string,
  realmName: string,
  groupId: string
) => {
  const { data } = await axios.get(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups/${groupId}/children`,
    {
      headers: { Authorization: `Bearer ${token.value}` },
    }
  );

  return data;
};

// Delete group
const deleteGroup = async (
  token: Token,
  keycloakBaseUrl: string,
  realmName: string,
  groupId: string
) => {
  await axios.delete(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups/${groupId}`,
    {
      headers: { Authorization: `Bearer ${token.value}` },
    }
  );
};

// Get group by id
const getGroupById = async (
  token: Token,
  keycloakBaseUrl: string,
  realmName: string,
  groupId: string
) => {
  const { data } = await axios.get(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups/${groupId}`,
    {
      headers: { Authorization: `Bearer ${token.value}` },
    }
  );

  return data;
};

// Get subgroups by id
const getSubGroupsById = async (
  token: Token,
  keycloakBaseUrl: string,
  realmName: string,
  groupId: string
) => {
  const { data } = await axios.get(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups/${groupId}/children`,
    {
      headers: { Authorization: `Bearer ${token.value}` },
    }
  );

  return data;
};

// Recursive delete
const deleteChildGroupsRecursively = async (
  token: Token,
  keycloakBaseUrl: string,
  realmName: string,
  groupId: string
) => {
  const subGroups = await getSubGroupsById(
    token,
    keycloakBaseUrl,
    realmName,
    groupId
  );

  for (const childGroup of  subGroups) {
    if (childGroup.name !== 'organizations') {
      await deleteChildGroupsRecursively(
        token,
        keycloakBaseUrl,
        realmName,
        childGroup.id
      );
      await deleteGroup(token, keycloakBaseUrl, realmName, childGroup.id);
    } else {
    }
  }
};

export const deleteAllChildOrganizations = async (
  token: Token,
  keycloakBaseUrl: string,
  realmName: string
) => {
  const parentGroup = await getGroupByName(
    token,
    keycloakBaseUrl,
    realmName,
    'organizations'
  );

  if (parentGroup) {
    await deleteChildGroupsRecursively(
      token,
      keycloakBaseUrl,
      realmName,
      parentGroup.id
    );
  } else {
  }
};

// Delete all groups
export const deleteAllGroups = async (
  token: Token,
  keycloakBaseUrl: string,
  realmName: string
) => {
  const groupsResponse = await axios.get(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups`,
    {
      headers: { Authorization: `Bearer ${token.value}` },
    }
  );

  for (const group of groupsResponse.data) {
    if (group.name === 'organizations') {
      continue;
    }
    await axios.delete(
      `${keycloakBaseUrl}/admin/realms/${realmName}/groups/${group.id}`,
      {
        headers: { Authorization: `Bearer ${token.value}` },
      }
    );
  }
};

// Create a user
export const createUser = async (
  token: Ref<string | null>,
  username: string,
  password: string,
  keycloakBaseUrl: string,
  realmName: string
): Promise<void> => {
  await axios.post(
    `${keycloakBaseUrl}/admin/realms/${realmName}/users`,
    {
      username,
      email: username,
      enabled: true,
      emailVerified: true,
      credentials: [
        {
          type: 'password',
          value: password,
          temporary: false,
        },
      ],
    },
    { headers: { Authorization: `Bearer ${token.value}` } }
  );
};

// Add user to group
export const addUserToGroup = async (
  token: Ref<string | null>,
  userId: string,
  groupId: string,
  keycloakBaseUrl: string,
  realmName: string
) => {
  await axios.put(
    `${keycloakBaseUrl}/admin/realms/${realmName}/users/${userId}/groups/${groupId}`,
    {},
    { headers: { Authorization: `Bearer ${token.value}` } }
  );
};

export const addUsersToGroups = async (
  token: Ref<string | null>,
  groupsData: any[],
  keycloakBaseUrl: string,
  realmName: string
): Promise<void> => {
  const allGroupsResponse = await axios.get(
    `${keycloakBaseUrl}/admin/realms/${realmName}/groups`,
    {
      headers: { Authorization: `Bearer ${token.value}` },
    }
  );

  const allGroups = allGroupsResponse.data;

  for (const groupData of groupsData) {
    const groupName = groupData.name;
    const children = groupData.children;

    const group = allGroups.find((group: any) => group.name === groupName);

    if (!group) {
      continue;
    }

    const childGroups = await getSubGroupsById(
      token,
      keycloakBaseUrl,
      realmName,
      group.id
    );

    for (const childData of children) {
      const childGroupName = childData.name;
      const users = childData.users;

      const childGroup = childGroups.find(
        (group: any) => group.name === childGroupName
      );

      if (!childGroup) {
        continue;
      }

      for (const user of users) {
        const userResponse = await axios.get(
          `${keycloakBaseUrl}/admin/realms/${realmName}/users`,
          {
            params: { username: user },
            headers: { Authorization: `Bearer ${token.value}` },
          }
        );

        const foundUser = userResponse.data[0];

        if (!foundUser) {
          continue;
        }

        await addUserToGroup(
          token,
          foundUser.id,
          childGroup.id,
          keycloakBaseUrl,
          realmName
        );
      }
    }
  }
};

/**
 * Assign roles to a user
 * @param token The access token
 * @param userId The ID of the user
 * @param roles An array of role names to assign
 */
export const assignRolesToUser = async (
  token: Token,
  userId: string,
  roles: string[],
  keycloakBaseUrl: string,
  realmName: string
) => {
  try {
    // Fetch all realm roles
    const rolesResponse = await axios.get(
      `${keycloakBaseUrl}/admin/realms/${realmName}/roles`,
      {
        headers: { Authorization: `Bearer ${token.value}` },
      }
    );

    // Filter roles by name
    const filteredRoles = rolesResponse.data.filter((role: any) =>
      roles.includes(role.name)
    );

    if (filteredRoles.length !== roles.length) {
      return;
    }

    // Assign roles to the user
    await axios.post(
      `${keycloakBaseUrl}/admin/realms/${realmName}/users/${userId}/role-mappings/realm`,
      filteredRoles,
      {
        headers: { Authorization: `Bearer ${token.value}` },
      }
    );
  } catch (error) {
    console.error(`Failed to assign roles to user (${userId}):`, error);
  }
};
