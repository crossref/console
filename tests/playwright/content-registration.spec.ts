import { test } from './content-registration-test';
import { ContentRegistrationPage } from './ContentRegistrationPage';
import {
  mockDepositSuccess,
  mockLoginSuccess,
} from './EditRecordForm/my-setup';
import { LoginBoxPage } from './LoginBoxPage';
import { expect } from '@playwright/test';
import * as fs from 'fs';

// test('basic test', async ({ page }) => {
//   await page.goto('https://playwright.dev/');
//   const title = page.locator('.navbar__inner .navbar__title');
//   await expect(title).toHaveText('Playwright');
// });

test.describe.parallel('Grant Registration suite', () => {
  test('Create new Record', async ({ page }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.createNewRecord();
  });

  test('Cancel create new Record select Type', async ({ page }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.cancelCreateNewRecordSelectType();
  });

  test('Cancel create new Record enter Name', async ({ page }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.cancelCreateNewRecordEnterName();
  });

  test('Shows no error when depositor name is valid', async ({ page }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.createNewRecord();
    await contentRegistrationPage.fillValidDepositorName();
  });

  test('Shows an error when depositor email is invalid', async ({ page }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.createNewRecord();
    await contentRegistrationPage.fillInvalidDepositorEmail();
  });

  test('Shows an error when the grant doi is invalid', async ({ page }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.createNewRecord();
    await contentRegistrationPage.fillInvalidGrantDoi();
  });

  test.skip('Shows an -is required- error when the grant doi is empty', async ({
    page,
  }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.createNewRecord();
    await contentRegistrationPage.fillAndRemoveGrantDoi();
  });

  test('Shows an error when the landing page is invalid', async ({ page }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.createNewRecord();
    await contentRegistrationPage.fillInvalidGrantLandingPage();
  });

  test.skip('Shows an -is required- error when the landing page is empty', async ({
    page,
  }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.createNewRecord();
    await contentRegistrationPage.fillAndRemoveGrantLandingPage();
  });

  test('Shows an -is required- error when the funder name is empty', async ({
    page,
  }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.createNewRecord();
    await contentRegistrationPage.fillAndRemoveFunderName();
  });

  test.skip('Shows no errors when form is filled with valid data', async ({
    page,
  }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.goto();
    await contentRegistrationPage.createNewRecord();
    await contentRegistrationPage.fillFormWithValidData();
  });

  // TODO - wire up the tests to use a dev web server with specific env var for API BASE URL
  // and HTTP_MOCKING
  test('Logs in given valid credentials @sandbox', async ({
    page,
    username,
    password,
  }) => {
    mockLoginSuccess(page, 2000);
    mockDepositSuccess(page, 0);
    const contentRegistrationPage = new ContentRegistrationPage(page);
    const loginBoxPage = new LoginBoxPage(page);
    await contentRegistrationPage.loadExampleRecord();
    await contentRegistrationPage.submitFormButton.click();
    await loginBoxPage.enterCredentials(username, password);
    await loginBoxPage.clickLogin();
    await expect(
      contentRegistrationPage.submissionCompleteHeading
    ).toBeVisible();
  });

  test('Loads a record from the filesystem', async ({ page }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.loadExampleRecord();
  });

  test('Downloads a Record', async ({ page }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    const pathToLocalRecord = 'downloaded-record.json';
    await contentRegistrationPage.loadExampleRecord();
    // TODO - replace with a check for a more reliable waitFor... signal
    await page.waitForTimeout(500);
    const [download] = await Promise.all([
      // Start waiting for the download
      page.waitForEvent('download'),
      // Perform the action that initiates download
      contentRegistrationPage.downloadRecordButton.click(),
    ]);
    await download.saveAs(pathToLocalRecord);
    // Wait for the download process to complete
    // const path = await download.path()
    const downloadedRecord = JSON.parse(
      fs.readFileSync(pathToLocalRecord, 'utf-8')
    );
    const testRecord = JSON.parse(
      fs.readFileSync(
        'tests/playwright/fixtures/test-grant-record.json',
        'utf-8'
      )
    );
    expect(testRecord).toEqual(downloadedRecord);
  });

  test('Login dialog can be re-opened after clicking outside to close it', async ({
    page,
  }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    await contentRegistrationPage.loadExampleRecord();
    // Click button:has-text("Submit")
    await page.locator('button:has-text("Submit")').click();
    const scrim = await page.locator('.v-overlay__scrim');
    const loginText = await page.locator('text=Crossref Login');
    expect(loginText).toBeVisible();
    // Click at the top-left corner, to ensure the clicks falls outside of the login dialog
    // triggering the 'close on click outside' behaviour
    await scrim.click({ position: { x: 0, y: 0 } });
    expect(loginText).not.toBeVisible();
    // Click button:has-text("Submit")
    await page.locator('button:has-text("Submit")').click();
    expect(loginText).toBeVisible();
  });

  test('Error notifications are cleared on user dismissal', async ({
    page,
  }) => {
    const contentRegistrationPage = new ContentRegistrationPage(page);
    const _loginBoxPage = new LoginBoxPage(page);
    await contentRegistrationPage.loadInvalidJsonFile();
    const notification = page.locator('text=in JSON at position 996');
    expect(notification).toBeVisible();
    await page.locator('button:has-text("Close")').click();
    await expect(notification).toBeHidden();
  });
});
