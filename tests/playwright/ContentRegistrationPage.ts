import { expect, Locator, Page } from '@playwright/test';

export class ContentRegistrationPage {
  readonly page: Page;
  readonly title: Locator;
  readonly titleText: string;
  readonly newRecordButton: Locator;
  readonly loadRecordButton: Locator;
  readonly downloadRecordButton: Locator;
  readonly submitFormButton: Locator;
  readonly recordTypeSelector: Locator;
  readonly recordTypeSelectorContinueButton: Locator;
  readonly recordTypeSelectorCancelButton: Locator;
  readonly recordNameContinueButton: Locator;
  readonly recordNameCancelButton: Locator;
  readonly recordNameField: Locator;
  readonly recordTypeOptionGrant: Locator;
  readonly formTitle: Locator;
  readonly depositorNameWrapper: Locator;
  readonly depositorNameInput: Locator;
  readonly grantDoiWrapper: Locator;
  readonly grantDoiInput: Locator;
  readonly grantDoiInputErrorMessages: Locator;
  readonly grantLandingPageWrapper: Locator;
  readonly grantLandingPageInput: Locator;
  readonly grantLandingPageInputErrorMessages: Locator;
  readonly funderNameWrapper: Locator;
  readonly funderNameInput: Locator;
  readonly funderNameInputErrorMessages: Locator;
  readonly funderIdWrapper: Locator;
  readonly funderIdInput: Locator;
  readonly funderIdInputErrorMessages: Locator;
  readonly depositorNameInputMessages: Locator;
  readonly depostorNameInputErrorMessages: Locator;
  readonly depositorNameRequiredError: Locator;
  readonly depositorEmailWrapper: Locator;
  readonly depositorEmailInput: Locator;
  readonly depositorEmailInputErrorMessages: Locator;
  readonly depositorEmailRequiredError: Locator;
  readonly addToGrantsButton: Locator;
  readonly firstGrantHeader: Locator;
  readonly submissionCompleteHeading: Locator;
  readonly tocList: Locator;
  readonly formTitleText: string;
  readonly exampleDepositorName: string;
  readonly exampleDepositorEmail: string;
  readonly exampleInvalidDepositorEmail: string;
  readonly exampleDepositorEmailIntl: string;
  readonly exampleInvalidDepositorEmailIntl: string;
  readonly depositorNameNoErrorsMessage: string;
  readonly depositorEmailNoErrorsMessage: string;
  readonly exampleGrantDoi: string;
  readonly exampleInvalidGrantDoi: string;
  readonly exampleGrantLandingPage: string;
  readonly exampleInvalidLandingPage: string;
  readonly exampleFunderName: string;
  readonly exampleFunderId: string;
  readonly grantDoiNoErrorsMessage: string;
  readonly grantLandingPageNoErrorsMessage: string;
  readonly funderNameNoErrorsMessage: string;
  readonly funderIdNoErrorsMessage: string;
  readonly jsonFormDataFilePathJournalArticle1: string;
  readonly recordDownloadAlertText: Locator;

  constructor(page: Page) {
    this.page = page;
    this.title = page.locator('[data-test="content_registration.home.title"]');
    this.titleText = 'Start record submission';
    this.newRecordButton = page.locator('button:has-text("New record")');
    this.loadRecordButton = page.locator('button:has-text("Load record")');
    this.downloadRecordButton = page.locator('button:has-text("Download")');
    this.submitFormButton = page.locator('button:has-text("Submit")');
    this.recordTypeSelector = page.locator(
      '[data-test="new_record_type_dropdown"]'
    );
    this.recordTypeSelectorContinueButton = page.locator(
      '[data-test="new_record-type_select-button-continue"]'
    );
    this.recordTypeSelectorCancelButton = page.locator(
      '[data-test="new_record-type_select-button-cancel"]'
    );
    this.recordNameContinueButton = page.locator(
      '[data-test="new_record-name-button-continue"]'
    );
    this.recordNameCancelButton = page.locator(
      '[data-test="new_record-name-button-cancel"]'
    );
    this.recordNameField = page.locator('[placeholder="Record name"]');
    this.recordTypeOptionGrant = page.locator(
      'div.v-overlay-container div.v-list-item-title >> text=Grant'
    );
    this.formTitle = page.locator(
      '[data-test="content_registration-form-title"]'
    );
    this.depositorNameWrapper = page.locator(
      '[id="#/properties/depositor_name"]'
    );
    this.depositorNameInput =
      this.depositorNameWrapper.locator('input[type="text"]');
    this.grantDoiWrapper = page.locator('[id="#/properties/doi"]');
    this.grantDoiInput = this.grantDoiWrapper.locator('input[type="text"]');
    this.grantDoiInputErrorMessages = this.grantDoiWrapper.locator(
      '.v-messages__message'
    );
    this.grantLandingPageWrapper = page.locator('[id="#/properties/resource"]');
    this.grantLandingPageInput =
      this.grantLandingPageWrapper.locator('input[type="text"]');
    this.grantLandingPageInputErrorMessages =
      this.grantLandingPageWrapper.locator('.v-messages__message');
    this.funderNameWrapper = page.locator('[id="#/properties/funder-name"]');
    this.funderNameInput = this.funderNameWrapper.locator('input[type="text"]');
    this.funderNameInputErrorMessages = this.funderNameWrapper.locator(
      '.v-messages__message'
    );
    this.funderIdWrapper = page.locator('[id="#/properties/funder-id"]');
    this.funderIdInput = this.funderIdWrapper.locator('input[type="text"]');
    this.funderIdInputErrorMessages = page.locator(
      this.generateInputErrorMessageElementSelector('#/properties/funder-id"]')
    );
    this.depositorNameInputMessages = page.locator(
      this.generateInputErrorMessageElementSelector(
        '#/properties/depositor_name'
      )
    );
    this.depostorNameInputErrorMessages = page.locator(
      this.generateInputErrorMessageElementSelector(
        '#/properties/depositor_name'
      )
    );
    this.depositorNameRequiredError = page.locator(
      'div[label="Depositor name"] div.v-messages__message:has-text("is required")]'
    );
    this.depositorEmailWrapper = page.locator(
      '[id="#/properties/email_address"]'
    );
    this.depositorEmailInput = page.getByLabel('Email address*');
    this.depositorEmailInputErrorMessages = page.locator(
      this.generateInputErrorMessageElementSelector(
        '#/properties/email_address'
      )
    );
    this.depositorEmailRequiredError = page.locator(
      'div[label="Depositor email"] div.v-messages__message:has-text("is required")]'
    );
    this.addToGrantsButton = page.locator('[aria-label="Add to Grants"]');
    this.firstGrantHeader = page.locator('button:has-text("1")');
    this.tocList = page.locator('article ul > li > a');
    this.submissionCompleteHeading = page.locator(
      'text=Record submission complete'
    );
    this.formTitleText = 'test-grant-record';
    this.exampleDepositorName = 'Given Family';
    this.exampleDepositorEmail = 'given.family@domain.tld';
    this.exampleInvalidDepositorEmail = 'given.family@';
    this.exampleDepositorEmailIntl = '用户@例子.广告';
    this.exampleInvalidDepositorEmailIntl = '用户@例子.';

    this.exampleGrantDoi = '10.5555/test4';
    this.exampleInvalidGrantDoi = '14.4121/';
    this.exampleGrantLandingPage = 'https://www.crossref.org/';
    this.exampleInvalidLandingPage = 'ww.something/';
    this.exampleFunderName = 'Example Funder';
    this.exampleFunderId = 'https://doi.org/10.13039/501100000000';

    this.depositorNameNoErrorsMessage =
      'The name of the individual who is submitting the metadata record';
    this.depositorEmailNoErrorsMessage =
      'The email address where deposit results should be sent';

    this.grantDoiNoErrorsMessage = 'The DOI to assign to this grant';
    this.grantLandingPageNoErrorsMessage =
      'The URL the DOI should resolve to, starting with http:// or https://';
    this.funderNameNoErrorsMessage = 'Name of the funder issuing the award';
    this.funderIdNoErrorsMessage =
      'Funder identifier from the Funder Registry, starting with https://doi.org/10.13039/';
    this.jsonFormDataFilePathJournalArticle1 =
      './tests/resources/json.formdata/journal_article_1.download.json';
    this.recordDownloadAlertText = page.getByText(
      'You can download your record'
    );
  }

  async fillAndRemoveText(inputId: string) {
    const selector = this.generateInputElementSelector(inputId);
    const input = this.page.locator(selector);
    await input.focus();
    await input.type('N');
    await this.page.waitForTimeout(300);
    await input.clear();
    await input.blur();
    await this.page.waitForTimeout(300);
  }

  async fillAndRemoveTextAndGetErrorMessages(
    inputId: string
  ): ReturnType<typeof this.getErrorMessages> {
    await this.fillAndRemoveText(inputId);
    return await this.getErrorMessages(inputId);
  }

  async waitForErrorMessages(
    inputId: string
  ): ReturnType<typeof this.page.waitForSelector> {
    const selector = this.generateInputErrorMessageElementSelector(inputId);
    return await this.page.waitForSelector(selector);
  }

  async waitForMessages(
    inputId: string
  ): ReturnType<typeof this.page.waitForSelector> {
    const selector = this.generateInputMessageElementSelector(inputId);
    return await this.page.waitForSelector(selector);
  }

  async getMessages(inputId: string): Promise<string> {
    const selector = this.generateInputMessageElementSelector(inputId);
    await this.waitForMessages(inputId);
    const messageContainer = this.page.locator(selector);
    const messages = messageContainer.textContent();

    // @ts-ignore
    return messages;
  }

  async getErrorMessages(inputId: string): Promise<string> {
    const selector = this.generateInputErrorMessageElementSelector(inputId);
    await this.waitForErrorMessages(inputId);
    const messageContainer = this.page.locator(selector).first();
    const messages = messageContainer.textContent();

    // @ts-ignore
    return messages;
  }

  generateControlSelector(inputId: string): string {
    return `div.control[id="${inputId}"]`;
  }

  generateInputElementSelector(inputId: string): string {
    return `div[id="${inputId}"] input[id="${inputId}-input"]`;
  }

  generateInputErrorMessageElementSelector(inputId: string): string {
    return this.generateInputMessageElementSelector(inputId);
  }

  generateInputMessageElementSelector(inputId: string): string {
    return `div[id="${inputId}-input-messages"]`;
  }

  async goto() {
    await this.page.goto('/records');
  }

  async createNewRecordSelectType() {
    await this.newRecordButton.click();
    await this.recordTypeSelector.click();
    await this.recordTypeOptionGrant.click();
  }

  async cancelCreateNewRecordSelectType() {
    await this.createNewRecordSelectType();
    await this.recordTypeSelectorCancelButton.click();
    const titleText = await this.title.textContent();
    expect(titleText).toEqual(this.titleText);
  }

  async createNewRecordEnterName() {
    await this.recordTypeSelectorContinueButton.click();
    await this.recordNameField.type(this.formTitleText);
  }

  async cancelCreateNewRecordEnterName() {
    await this.createNewRecordSelectType();
    await this.createNewRecordEnterName();
    await this.recordNameCancelButton.click();
    const titleText = await this.title.textContent();
    expect(titleText).toEqual(this.titleText);
  }

  async createNewRecord() {
    await this.createNewRecordSelectType();
    await this.createNewRecordEnterName();
    await this.recordNameContinueButton.click();
    const titleContent = await this.formTitle.textContent();
    expect(titleContent).toEqual('test-grant-record.json');
  }

  async fillValidDepositorName() {
    await this.depositorNameInput.type(this.exampleDepositorName);
    await this.depositorEmailWrapper.focus();
    const messages = await this.getMessages('#/properties/depositor_name');

    expect(messages).toEqual(this.depositorNameNoErrorsMessage);
  }

  async fillValidGrantDoi() {
    await this.grantDoiInput.type(this.exampleGrantDoi);
    const messages = await this.getMessages('#/properties/doi');
    expect(messages).toEqual(this.grantDoiNoErrorsMessage);
  }

  async fillValidGrantLandingPage() {
    await this.grantLandingPageInput.type(this.exampleGrantLandingPage);
    const messages = await this.getMessages('#/properties/resource');
    expect(messages).toEqual(this.grantLandingPageNoErrorsMessage);
  }

  async fillValidFunderName() {
    await this.funderNameInput.type(this.exampleFunderName);
    const messages = await this.getMessages('#/properties/funder-name');
    expect(messages).toEqual(this.funderNameNoErrorsMessage);
  }

  async fillValidFunderId() {
    await this.funderIdInput.type(this.exampleFunderId);
    const messages = await this.getMessages('#/properties/funder-id');
    expect(messages).toEqual(this.funderIdNoErrorsMessage);
  }

  async fillValidDepositorEmail() {
    await this.depositorEmailInput.type(this.exampleDepositorEmail);
    await this.depositorNameWrapper.focus();
    const messages = await this.getMessages('#/properties/email_address');
    expect(messages).toEqual(this.depositorEmailNoErrorsMessage);
  }

  async fillInvalidDepositorName() {
    await this.depositorNameInput.focus();
    await this.depositorNameInput.type('F');
    await this.page.keyboard.press('Backspace');
    await this.page.keyboard.press('Tab');
    await this.page.waitForSelector(
      '[id="#/properties/depositor_name"] >> .v-messages__message >> text=is required'
    );
    const messages = await this.depostorNameInputErrorMessages.textContent();
    expect(messages).toEqual('is required');
  }

  async fillInvalidDepositorEmail() {
    await this.depositorEmailInput.click();
    await this.depositorEmailInput.type(this.exampleInvalidDepositorEmail);
    await this.page.waitForTimeout(400);
    const errorsAttribute = await this.page
      .locator(this.generateControlSelector('#/properties/email_address'))
      .getAttribute('errors');
    await expect(errorsAttribute).toEqual('Please enter a valid email address');
  }

  async fillInvalidGrantDoi() {
    await this.grantDoiInput.focus();
    await this.grantDoiInput.type(this.exampleInvalidGrantDoi);
    await this.page.waitForSelector(
      'text=Please enter a valid DOI, starting with 10.'
    );
  }

  async fillInvalidGrantLandingPage() {
    await this.grantLandingPageInput.focus();
    await this.grantLandingPageInput.type(this.exampleInvalidLandingPage);
    await this.page.waitForSelector(
      'text=Please enter a valid URL, starting with http:// or https://'
    );
  }

  async fillAndRemoveGrantDoi() {
    const messages = await this.fillAndRemoveTextAndGetErrorMessages(
      '#/properties/doi'
    );
    expect(messages).toEqual('is required');
  }

  async fillAndRemoveGrantLandingPage() {
    const messages = await this.fillAndRemoveTextAndGetErrorMessages(
      '#/properties/resource'
    );
    expect(messages).toEqual('is required');
  }

  async fillAndRemoveFunderName() {
    const messages = await this.fillAndRemoveTextAndGetErrorMessages(
      '#/properties/funder-name'
    );
    expect(messages).toEqual('is required');
  }

  async fillFormWithValidData() {
    await this.fillValidDepositorName();
    await this.fillValidDepositorEmail();
    await this.fillValidGrantDoi();
    await this.fillValidGrantLandingPage();
    await this.fillValidFunderName();
    await this.fillValidFunderId();
  }

  async fillDepositorEmail() {
    await this.depositorEmailWrapper.fill('firstname.lastname@domain.tld');
  }

  async addGrant() {
    await this.addToGrantsButton.click();
  }

  async expandFirstGrant() {
    await this.firstGrantHeader.click();
  }

  async loadAndSubmitRecord(
    filePath = this.jsonFormDataFilePathJournalArticle1
  ) {
    await this.loadRecord(filePath);
    await this.page.getByRole('button', { name: 'Next' }).click();
    await this.page.getByRole('button', { name: 'Next' }).click();
    await this.page.getByRole('button', { name: 'Next' }).click();
    await this.page
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();
    await expect(this.page.getByText(' successfully submitted')).toBeVisible();
  }

  async loadSubmitAndRestartInNewJournal(
    filePath = this.jsonFormDataFilePathJournalArticle1
  ) {
    await this.loadAndSubmitRecord(filePath);
    await this.page
      .getByRole('button', { name: 'start new submission' })
      .click();
    await this.page.getByText('In a new journal').click();
    await expect(this.page.getByLabel('Article title*')).toBeVisible();
  }

  async loadSubmitAndRestartInSameJournal(
    filePath = this.jsonFormDataFilePathJournalArticle1
  ) {
    await this.loadAndSubmitRecord(filePath);
    await this.page
      .getByRole('button', { name: 'start new submission' })
      .click();
    await this.page.getByText('In the same journal').click();
    await expect(this.page.getByLabel('Article title*')).toBeVisible();
  }

  async loadSubmitAndRestartInSameJournalAndIssue(
    filePath = this.jsonFormDataFilePathJournalArticle1
  ) {
    await this.loadAndSubmitRecord(filePath);
    await this.page
      .getByRole('button', { name: 'start new submission' })
      .click();
    await this.page.getByText('In the same issue').click();
    await expect(this.page.getByLabel('Article title*')).toBeVisible();
  }

  async loadExampleRecord(
    filePath = 'tests/playwright/fixtures/test-grant-record.json'
  ) {
    await this.goto();
    // Note that Promise.all prevents a race condition
    // between clicking and waiting for the file chooser.
    const [fileChooser] = await Promise.all([
      // It is important to call waitForEvent before click to set up waiting.
      this.page.waitForEvent('filechooser'),
      // Opens the file chooser.
      await this.loadRecordButton.click(),
    ]);
    await fileChooser.setFiles(filePath);
    expect(await this.depositorNameInput.inputValue()).toEqual(
      this.exampleDepositorName
    );
  }

  async loadRecord(
    filePath = 'tests/playwright/fixtures/test-grant-record.json'
  ) {
    await this.goto();
    // Note that Promise.all prevents a race condition
    // between clicking and waiting for the file chooser.
    const [fileChooser] = await Promise.all([
      // It is important to call waitForEvent before click to set up waiting.
      this.page.waitForEvent('filechooser'),
      // Opens the file chooser.
      await this.loadRecordButton.click(),
    ]);
    await fileChooser.setFiles(filePath);
  }

  async loadJournalArticleRecord() {
    await this.loadRecord(this.jsonFormDataFilePathJournalArticle1);
    await expect(this.recordDownloadAlertText).toBeVisible();
  }

  async loadInvalidJsonFile() {
    await this.goto();
    // Note that Promise.all prevents a race condition
    // between clicking and waiting for the file chooser.
    const [fileChooser] = await Promise.all([
      // It is important to call waitForEvent before click to set up waiting.
      this.page.waitForEvent('filechooser'),
      // Opens the file chooser.
      await this.loadRecordButton.click(),
    ]);
    await fileChooser.setFiles(
      'tests/playwright/fixtures/test-grant-record--invalid-json.json'
    );
  }
}
