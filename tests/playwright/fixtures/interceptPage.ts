import { test as baseTest } from '@playwright/test';

const STATUS_PAGE_URL = 'https://status.crossref.org';

const interceptPage = baseTest.extend({
  page: async ({ page }, use) => {
    await page.route(`${STATUS_PAGE_URL}/**`, (route) => route.abort());
    await use(page);
  },
});

export const test = interceptPage;
