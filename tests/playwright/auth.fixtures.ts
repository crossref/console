import { test as base, expect, Page } from '@playwright/test';
import { LoginBoxPage } from './LoginBoxPage';
import { AuthenticatedPage } from './AuthenticatedPage';
import jwt from 'jsonwebtoken';

export const CDS_LOGIN_URL = 'https://test.crossref.org/servlet/login';
export const CDS_LOGIN_REGEXP = /.*.crossref.org\/servlet\/login/;
export const CDS_TEST_USER = 'test.user';
export const CDS_TEST_PASSWORD = 'test.password';
export const STATUS_PAGE_URL = 'https://status.crossref.org';

export type AuthFixtures = {
  cdsLoginPage: LoginBoxPage;
  authenticatedPage: AuthenticatedPage;
};

// Function to generate a JWT matching the auth server format
export const generateJwtToken = (expiryInHours: number) => {
  const expiryTime = Math.floor(Date.now() / 1000) + expiryInHours * 3600; // Expiry time in seconds

  const payload = {
    exp: expiryTime,
    user_name: 'pvale@crossref.org',
    authorities: ['ROLE_USER'],
    jti: 'E1P4reH_RIVTa3pVe935sOADMEs', // Unique identifier for the token, usually random
    client_id: 'cs',
    scope: ['read', 'write'],
  };

  const header = {
    alg: 'HS256',
    typ: 'JWT',
  };

  const secretKey = 'mock-secret'; // Use any secret key, as this is just for testing purposes

  return jwt.sign(payload, secretKey, { header });
};

let generatedToken: string;
let generatedRefreshToken: string;  

export const test = base.extend<AuthFixtures>({
  cdsLoginPage: async ({ page }, use) => {
    // Nullify requests to the status page (stop the widget)
    await page.route(`${STATUS_PAGE_URL}/**`, (route) => route.abort());
    const cdsLoginPage = new LoginBoxPage(page);
    await use(cdsLoginPage);
  },
  authenticatedPage: async ({ page, cdsLoginPage }, use) => {
    // Mock the login route
    generatedToken = generateJwtToken(24); // Generate a token with 24-hour expiry
    generatedRefreshToken = generateJwtToken(48); // Generate a refresh token with 48-hour expiry

    await page.route(CDS_LOGIN_REGEXP, async (route, request) => {
      route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify({
          success: true,
          message: 'Authenticated',
          redirect: null,
          roles: ['andplus'],
          Authorization: `Bearer ${generatedToken}`, // Return the generated JWT as Authorization token
          refresh_token: generatedRefreshToken, // Return the generated refresh token
          identity: 'pvale@crossref.org:andplus',
          authenticated: true,
          authorised: true,
        }),
      });
    });

    // Perform login
    await cdsLoginPage.visit();
    await cdsLoginPage.enterCredentials(CDS_TEST_USER, CDS_TEST_PASSWORD);
    await cdsLoginPage.clickLogin();

    // Create an instance of AuthenticatedPage with the token
    const authenticatedPage = new AuthenticatedPage(
      cdsLoginPage,
      generatedToken
    );

    // Use the authenticated page instance
    await use(authenticatedPage);
  },
});

export { expect } from '@playwright/test';
