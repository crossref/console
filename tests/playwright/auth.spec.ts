import { test, expect } from './auth.fixtures';
import { CDS_TEST_USER } from './auth.fixtures';

test.describe('Authentication', () => {
  test('Can login to CDS', async ({ authenticatedPage }) => {
    // Use the authenticated page to verify login success
    await expect(authenticatedPage.page.getByText(CDS_TEST_USER)).toBeVisible();
  });

  test('Can logout from CDS', async ({ authenticatedPage }) => {
    // Open the user menu and click logout
    await authenticatedPage.userMenuTrigger.click();
    const lb = authenticatedPage.page.getByText('Log out');
    // await lb.click();
    // await expect(authenticatedPage.logoutButton).toBeVisible();
    await authenticatedPage.logoutButton.click();

    // Verify the user is logged out, for example by checking if the login page is displayed again
    await expect(
      authenticatedPage.page.getByText(CDS_TEST_USER)
    ).not.toBeVisible();
  });
});
