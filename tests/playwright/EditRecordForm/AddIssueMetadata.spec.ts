import { articleFormWithIssueStep, expect } from './my-setup';

//TODO: turn this form fill-out into a shared test fixture
articleFormWithIssueStep(
  'Shows Issue step after clicking add issue metadata button',
  { tag: '@smoke' },
  async ({ form }) => {
    await expect(form.getByLabel('Volume number (optional)')).toBeVisible();

    await expect(form.getByLabel('Issue number (optional)')).toBeVisible();
  }
);
