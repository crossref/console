import {
  articleAtReviewStep,
  articleFormWithIssueStep,
  expect,
  mockConvertFailure,
  mockConvertSuccess,
  mockDepositFailure,
  mockDepositSuccess,
  mockLoginSuccess,
} from './my-setup';
import { LoginBoxPage } from '../LoginBoxPage';

const assertErrorCodeVisible = async (form, errorCode) => {
  const errorCodeSelector = `div.error-code:has-text("Error Code: ${errorCode}")`;
  await expect(form.locator(errorCodeSelector)).toBeVisible();
};

const assertErrorMessageVisible = async (form, errorMessage) => {
  const errorMessageSelector = `div.error-text:has-text("${errorMessage}")`;
  await expect(form.locator(errorMessageSelector)).toBeVisible();
};

const assertDomainMessageVisible = async (form, domainMessage) => {
  const errorMessageSelector = `div.domain-message:has-text('${domainMessage}')`;
  await expect(form.locator(errorMessageSelector)).toBeVisible();
};

articleFormWithIssueStep(
  'Shows record deposit progress bar',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockConvertSuccess(form);
    await mockDepositSuccess(form, 0);
    const loginBoxPage = new LoginBoxPage(form);
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await nextBtn.click();
    form
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();

    await expect(form.getByText(' successfully submitted')).toBeVisible();
  }
);

articleFormWithIssueStep(
  'End-to-end deposit works',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockConvertSuccess(form);
    await mockDepositSuccess(form);
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await nextBtn.click();
    form
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();

    const submissionIdElement = form.locator('[data-test="submission-id"]');
    const submissionIdValue = await submissionIdElement.textContent();

    await expect(form.getByText(' successfully submitted')).toBeVisible();
    await form.reload();
    await expect(form.getByText(' successfully submitted')).toBeVisible();
    await expect(submissionIdElement).toContainText(submissionIdValue);
  }
);

articleAtReviewStep(
  'Shows error dialog on xml conversion 400 bad data error',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockConvertFailure(form);
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await nextBtn.click();
    form
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();

    await expect(
      form.getByText(
        'Your metadata could not be converted to XML successfully.'
      )
    ).toBeVisible();
    await assertErrorCodeVisible(form, 400);
    await assertErrorMessageVisible(form, 'Error: Bad Request');
    await assertDomainMessageVisible(form, 'Details: {');
  }
);

articleAtReviewStep(
  'Shows error dialog on xml conversion 500 internal server error',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockLoginSuccess(form);
    await mockConvertFailure(form, 0, 500);
    const loginBoxPage = new LoginBoxPage(form);
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await nextBtn.click();
    form
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();

    await expect(form.getByText('Metadata conversion error')).toBeVisible();
    await expect(
      form.getByText(
        'Your metadata could not be converted to XML successfully.'
      )
    ).toBeVisible();
    await assertErrorCodeVisible(form, 500);
    await expect(
      form.locator('div.error-code:has-text("Error Code: 500")')
    ).toBeVisible();
    await expect(
      form.locator('div.error-text:has-text("Error: Internal Server Error")')
    ).toBeVisible();
    await assertErrorMessageVisible(form, 'Error: Internal Server Error');
  }
);

articleAtReviewStep(
  'Shows error dialog on xml conversion network failure',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockLoginSuccess(form);
    await mockConvertFailure(form, 0, -1);
    const loginBoxPage = new LoginBoxPage(form);
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await nextBtn.click();
    form
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();

    await expect(form.getByText('Network error')).toBeVisible();
    await expect(
      form.getByText(
        'Your metadata could not be converted to XML successfully.'
      )
    ).toBeVisible();
  }
);

articleAtReviewStep(
  'Shows error dialog on deposit 403 unauthorised error',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockLoginSuccess(form);
    await mockConvertSuccess(form);
    await mockDepositFailure(form, 0, 403);
    const loginBoxPage = new LoginBoxPage(form);
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await nextBtn.click();
    form
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();

    await expect(form.getByText('An error occurred.')).toBeVisible();
    await expect(
      form.getByText(
        'Your submission could not be completed due to the following error(s).'
      )
    ).toBeVisible();
    await assertErrorCodeVisible(form, 403);
    await assertErrorMessageVisible(
      form,
      'Error: User not allowed to add or modify records for prefix: 10.32013'
    );
    await assertDomainMessageVisible(
      form,
      'User not allowed to add or modify records for prefix: 10.32013'
    );
  }
);

articleAtReviewStep(
  'Shows error dialog on deposit 200 processed but with errors',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockLoginSuccess(form);
    await mockConvertSuccess(form);
    await mockDepositFailure(form, 0, 200);
    const loginBoxPage = new LoginBoxPage(form);
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await nextBtn.click();
    form
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();

    await expect(form.getByText('An error occurred.')).toBeVisible();
    await expect(
      form.getByText(
        'Your submission could not be completed due to the following error(s).'
      )
    ).toBeVisible();
    await assertErrorCodeVisible(form, 200);
    await assertErrorMessageVisible(
      form,
      'Error: Record not processed because submitted version: 2012061910233600835 is less or equal to previously submitted version (DOI match)'
    );
    await assertDomainMessageVisible(
      form,
      'Details: <?xml version=\\"1.0\\" encoding=\\"UTF-8\\"?>'
    );
  }
);

articleAtReviewStep(
  'Shows error dialog on deposit network error',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockLoginSuccess(form);
    await mockConvertSuccess(form);
    await mockDepositFailure(form, 0, -1);
    const loginBoxPage = new LoginBoxPage(form);
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await nextBtn.click();
    form
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();

    await expect(form.getByText('An error occurred.')).toBeVisible();
    await expect(
      form.getByText(
        'There was an error submitting your metadata. Please try again.'
      )
    ).toBeVisible();
    await assertErrorMessageVisible(form, 'Error: Network Error');
  }
);

articleAtReviewStep(
  'Shows error dialog on deposit 500 internal server error',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockLoginSuccess(form);
    await mockConvertSuccess(form);
    await mockDepositFailure(form, 0, 500);
    const loginBoxPage = new LoginBoxPage(form);
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await nextBtn.click();
    form
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'submit' })
      .click();

    await expect(form.getByText('An error occurred.')).toBeVisible();
    await expect(
      form.getByText(
        'There was an error submitting your metadata. Please try again.'
      )
    ).toBeVisible();
    await assertErrorCodeVisible(form, 500);
    await assertErrorMessageVisible(form, 'Error: Internal server error');
  }
);
