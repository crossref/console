import { PageElement, By } from '@serenity-js/web';

export const OrganizationUsersCard = {
  container: () => PageElement.located(By.css('.organization-users-table')),
  title: () =>
    PageElement.located(
      By.cssContainingText('.v-toolbar-title', 'Metadata Plus Admins')
    ),
  addButton: () =>
    PageElement.located(
      By.css('[data-test-id="add-organization-user-button"]')
    ),
  addUserNameField: () =>
    PageElement.located(By.id('#/properties/username-input')).describedAs(
      'Add user name input'
    ),
  saveNewUserButton: () =>
    PageElement.located(
      By.css('.organization-users-card__save-new-user-button')
    ),
  cancelButton: () =>
    PageElement.located(By.cssContainingText('.v-btn__content', 'cancel')),
};

export const AddNewOrganizationUserDialog = {
  container: () =>
    PageElement.located(By.css('.add-new-organization-user-dialog')),
};
