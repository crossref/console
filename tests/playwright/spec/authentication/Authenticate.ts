import { not } from '@serenity-js/assertions';
import { Answerable, Check, Log, Task, Wait } from '@serenity-js/core';
import {
  By,
  Click,
  Enter,
  isVisible,
  Navigate,
  PageElement,
} from '@serenity-js/web';

import {
  ADMIN_UI_PASSWORD,
  ADMIN_UI_USERNAME,
  STAFFER_PASSWORD,
  STAFFER_USERNAME,
  USER_PASSWORD,
  USER_USERNAME,
} from './test-data';
import { UserMenu } from './VerifyAuthentication';

/**
 * This is called a "Task".
 * Use tasks to compose a sequence of one or more activities and give them domain meaning.
 *
 * Here, the actor performs three activities:
 * - enter username
 * - enter password
 * - click on the login button
 *
 * This sequence of activities together means to "log in"
 */
export const Authenticate = {
  using: (username: Answerable<string>, password: Answerable<string>) =>
    Task.where(
      `#actor authenticates in as ${username}`,
      Check.whether(UserMenu.userMail(), isVisible()).andIfSo(
        Log.the('#actor is already logged in')
      ),
      Enter.theValue(username).into(LoginForm.usernameField()),
      Enter.theValue(password).into(LoginForm.passwordField()),
      Click.on(LoginForm.loginButton())
    ),
};

export const Logout = () =>
  Task.where(
    `#actor logs out`,
    Check.whether(UserMenu.username(), not(isVisible()))
      .andIfSo(Log.the('#actor is already logged out'))
      .otherwise(
        Click.on(UserMenu.trigger()),
        Wait.until(UserMenu.logOutTrigger(), isVisible()),
        Click.on(UserMenu.logOutTrigger())
      )
  );

export const Login = {
  using: (username: Answerable<string>, password: Answerable<string>) =>
    Task.where(
      `#actor logs in as ${username}`,
      Check.whether(UserMenu.userMail(), isVisible()).andIfSo(
        Log.the(`#actor is already logged in as ${UserMenu.userMail()}`)
      ),
      Navigate.to('/login'),
      Authenticate.using(username, password)
    ),
};

export const LoginAsStaffer = () =>
  Task.where(
    `#actor logs in as a Staff member`,
    Login.using(STAFFER_USERNAME, STAFFER_PASSWORD)
  );

export const LoginAsUser = () =>
  Task.where(
    `#actor logs in as an Admin`,
    Login.using(USER_USERNAME, USER_PASSWORD)
  );

export const loginAsAdmin = () =>
  Task.where(
    `#actor logs in as an Admin`,
    Login.using(ADMIN_UI_USERNAME, ADMIN_UI_PASSWORD)
  );

/**
 * This is called a "Lean Page Object".
 * Lean Page Objects describe interactive elements of a widget.
 * In this case, the login form widget at https://the-internet.herokuapp.com/login
 */
const LoginForm = {
  usernameField: () =>
    PageElement.located(By.css('[name="username"]')).describedAs(
      'username field'
    ),

  passwordField: () =>
    PageElement.located(By.css('[name="password"]')).describedAs(
      'password field'
    ),

  loginButton: () =>
    PageElement.located(By.css('[name="login"]')).describedAs('login button'),
};
