import { By, PageElement } from '@serenity-js/web';
export const Snackbar = {
  content: () => PageElement.located(By.css('.notification-snackbar-content')),
};
