import { env } from '@/env';
import { disableApiKey } from './support/OrganizationsApiKeys/tasks';
import { OrganizationUsersCard } from './organization-users/questions';
import { contextMenuTrigger } from './support/DataTable/questions';
import {
  Ensure,
  equals,
  isPresent,
  not,
  contain,
  includes,
} from '@serenity-js/assertions';
import {
  Cast,
  Notepad,
  TakeNotes,
  Wait,
  notes,
  q,
  Log,
  Duration,
} from '@serenity-js/core';
import { describe, it, test } from '@serenity-js/playwright-test';
import { CallAnApi } from '@serenity-js/rest';
import {
  By,
  Click,
  ExecuteScript,
  LastScriptExecution,
  PageElement,
  Page,
  Text,
  Value,
  isVisible,
  Enter,
  isClickable,
  isEnabled,
} from '@serenity-js/web';
import {
  ObtainAccessToken,
  ResetKeycloakUsersAndGroups,
  createApiKey,
} from './../../screenplay/tasks/SetupKeycloak';
import { enterTextIntoDialog, GlobalDialog } from './global-dialog/Dialog';
import {
  OrganizationApiKeysTable,
  rowByKeyId,
  firstApiKeyRow,
  cellByKeyId,
} from './organizations/OrganizationApiKeysTable';
import { OrganizationCard } from './organizations/View';
import { createUserAndAddToOrg } from './organization-users/tasks';

import { BrowseTheWebWithPlaywright } from '@serenity-js/playwright';
import axios from 'axios';
import https from 'https';
import {
  LoginAsStaffer,
  LoginAsUser,
  Logout,
} from './authentication/Authenticate';
import {
  CreateOrganizationForm,
  createNewApiKeyDescribedAs,
  createNewOrganizaton,
} from './organizations/Create';
import { updateOrganizationName } from './organizations/Update';
import {
  organizationSearchResultNames,
  searchOrganizationsInput,
} from './support/OrganizationSearchBox/questions';
import {
  searchForOrganizationsMatching,
  selectOrganizationByName,
} from './support/OrganizationSearchBox/tasks';
import { toggleTheMetaDataPlusServiceSubscription } from './support/OrganizationSubscriptions/tasks';
import {
  createNewOrganizationButton,
  organizationCard,
  organizationCardTitle,
  organizationSettingsListOrganizationId,
  organizationSettingsListOrganizationName,
} from './support/Organizations/questions';
import {
  goToTheAdminLandingPageForOrganization,
  startOnTheOrganizationsLandingPage,
} from './support/Organizations/tasks';
import {
  createApiKeyDialog,
  apiKeyTableRow,
} from './support/OrganizationsApiKeys/questions';
import { ORGANIZATIONS, ORGANIZATION_SEARCH_QUERY } from './support/test-data';
import { endsWith } from 'lodash';
// const port = process.env.E2E_WEBSERVER_PORT || process.env.PORT || 8000;
// const host = process.env.E2E_WEBSERVER_HOST || 'localhost';

const baseUrl = `${env().BASE_URL}`;
const keycloakServerUrl = `${env().keycloakServerURL}/auth`;

const axiosInstance = axios.create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
  baseURL: keycloakServerUrl,
});

export interface MyNotes {
  apiKey?: string | null;
  metadataPlusSubscriptionEnabled?: string;
  keyId?: string;
  keyEnabled?: string;
  auth_token?: string;
  first_organization_id?: number;
  apiKeyDescription?: string;
  apiKeyId?: string;
}

describe('Key manager', () => {
  test.use({
    baseURL: baseUrl,
    actors: async ({ browser, contextOptions }, use) => {
      await use(
        Cast.where((actor) =>
          actor.whoCan(
            BrowseTheWebWithPlaywright.using(browser, contextOptions),
            TakeNotes.using(Notepad.with<MyNotes>({})),
            // Type error caused by version mismatch between axios in the app and serenity-js/rest test runner
            // Suppressing it until the version of axios used in the app is updated
            // FIXME: update axios version from 0.21.0 to 1.4.0
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore: Suppress "Argument of type..." error
            CallAnApi.using(axiosInstance)
          )
        )
      );
    },
  });
  describe('Managing Organizations', () => {
    test.beforeEach(async ({ actor }) => {
      await actor.attemptsTo(ResetKeycloakUsersAndGroups());
    });
    it('should allow me to search for organizations', async ({ actor }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        startOnTheOrganizationsLandingPage(),
        searchForOrganizationsMatching(ORGANIZATION_SEARCH_QUERY),
        Ensure.that(organizationSearchResultNames(), equals(ORGANIZATIONS))
      );
    });
    it("should take me to the organization's Landing Page when I select it", async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsUser(),
        startOnTheOrganizationsLandingPage(),
        searchForOrganizationsMatching(ORGANIZATION_SEARCH_QUERY),
        selectOrganizationByName('org1 : 1'),
        Ensure.eventually(organizationCardTitle(), equals('org1')),
        Ensure.eventually(
          organizationSettingsListOrganizationName(),
          equals('org1')
        ),
        Ensure.eventually(organizationSettingsListOrganizationId(), equals('1'))
      );
    });
    it('should allow me to disable the Metadata Plus service', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        goToTheAdminLandingPageForOrganization(1),
        toggleTheMetaDataPlusServiceSubscription(),
        toggleTheMetaDataPlusServiceSubscription()
      );
    });
    it('should allow me to attempt to create a new organisation if I am a staffer', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        startOnTheOrganizationsLandingPage(),
        Click.on(createNewOrganizationButton()),
        createNewOrganizaton.using('org100', 100)
      );
    });
    it('Should allow me, as a staffer to create a new organization', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        startOnTheOrganizationsLandingPage(),
        Click.on(createNewOrganizationButton()),
        Enter.theValue('TestOrg1').into(CreateOrganizationForm.nameField()),
        Enter.theValue('101').into(CreateOrganizationForm.memberIdField()),
        Wait.for(Duration.ofMilliseconds(400)), // wait for the form debounce timeout
        Click.on(CreateOrganizationForm.createButton()),
        Ensure.that(organizationCardTitle(), equals('TestOrg1'))
      );
    });
    it('request confirmation before cancelling create new organization if form is dirty', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        startOnTheOrganizationsLandingPage(),
        Click.on(createNewOrganizationButton()),
        Enter.theValue('TestOrg1').into(CreateOrganizationForm.nameField()),
        Wait.for(Duration.ofMilliseconds(400)), // wait for the form debounce timeout
        Click.on(CreateOrganizationForm.cancelButton()),
        Click.on(GlobalDialog.acceptButton()),
        Ensure.eventually(searchOrganizationsInput(), isPresent())
      );
    });
    it('should allow me to cancel my attempt to create a new organisation', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        startOnTheOrganizationsLandingPage(),
        Click.on(createNewOrganizationButton()),
        Click.on(CreateOrganizationForm.cancelButton()),
        Ensure.that(searchOrganizationsInput(), isVisible())
      );
    });
    it('should only show the create organization button to staffers', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsUser(),
        startOnTheOrganizationsLandingPage(),
        Ensure.that(createNewOrganizationButton(), not(isVisible()))
      );
    });
    it('should show the add new user button to staffmembers', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        goToTheAdminLandingPageForOrganization(1),
        Wait.until(OrganizationUsersCard.title(), isPresent()),
        Ensure.that(OrganizationUsersCard.addButton(), isClickable())
      );
    });
    it('should not show the add new user button to users', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsUser(),
        goToTheAdminLandingPageForOrganization(1),
        Wait.until(OrganizationUsersCard.title(), isPresent()),
        Ensure.that(OrganizationUsersCard.addButton(), not(isClickable()))
      );
    });
    it('should allow me to change the name of an Organization', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        goToTheAdminLandingPageForOrganization(1),
        Click.on(OrganizationCard.editButton()),
        updateOrganizationName('updatedOrg1'),
        Wait.until(
          organizationSettingsListOrganizationName(),
          equals('updatedOrg1')
        ),
        Click.on(OrganizationCard.editButton()),
        updateOrganizationName('org1'),
        Wait.until(organizationSettingsListOrganizationName(), equals('org1'))
      );
    });
    it('should allow me, as a staffmember, to add a user to an Organization', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        goToTheAdminLandingPageForOrganization(1),
        createUserAndAddToOrg('test-user@user.com'),
        Wait.until(
          PageElement.located(
            By.cssContainingText('td', 'test-user@user.com')
          ).of(OrganizationUsersCard.container()),
          isVisible()
        )
      );
    });
    it('should clear the username field when add user dialog is cancelled', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        goToTheAdminLandingPageForOrganization(1),
        Click.on(OrganizationUsersCard.addButton()),
        Enter.theValue('a.test.user@fake.crossref.org').into(
          OrganizationUsersCard.addUserNameField()
        ),
        Wait.for(Duration.ofMilliseconds(400)), // wait for the form debounce timeout
        Click.on(OrganizationUsersCard.cancelButton()),
        Click.on(OrganizationUsersCard.addButton()),
        Ensure.that(
          Text.of(OrganizationUsersCard.addUserNameField()),
          equals('')
        )
      );
    });
    it('add user save button is disabled until validation passes', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsStaffer(),
        goToTheAdminLandingPageForOrganization(1),
        Click.on(OrganizationUsersCard.addButton()),
        Ensure.that(
          OrganizationUsersCard.saveNewUserButton(),
          not(isEnabled())
        ),
        Enter.theValue('user@').into(OrganizationUsersCard.addUserNameField()),
        Wait.until(
          PageElement.located(By.css('.v-messages__message')),
          isVisible()
        ),
        Enter.theValue('user@user.com').into(
          OrganizationUsersCard.addUserNameField()
        ),
        Wait.until(
          PageElement.located(By.css('.v-messages__message')),
          not(isVisible())
        )
      );
    });
    it('should allow me, as a user, to delete an API key', async ({
      actor,
    }, testInfo) => {
      // Pick the first api key in the table
      // Note its keyId
      // Delete it
      // Check it's gone from the table
      await actor.attemptsTo(
        ObtainAccessToken(),
        createApiKey(testInfo.title),
        LoginAsUser(),
        goToTheAdminLandingPageForOrganization(1),
        Click.on(OrganizationApiKeysTable.rowContextMenuTrigger()),
        notes<MyNotes>().set(
          'keyId',
          Text.of(
            PageElement.located(By.css('.api-keys-table__table-row__key-id'))
          )
        ),
        Wait.until(cellByKeyId(notes().get('keyId')), isVisible()),
        Click.on(OrganizationApiKeysTable.deleteButton()),
        enterTextIntoDialog('DELETE'),
        Ensure.that(OrganizationApiKeysTable.container(), isPresent()),
        Wait.until(cellByKeyId(notes().get('keyId')), not(isVisible())),
        notes().clear()
      );
    });
    it('should allow me, as a user, to update the description of an API key', async ({
      actor,
    }) => {
      await actor.attemptsTo(
        LoginAsUser(),
        startOnTheOrganizationsLandingPage(),
        Ensure.that(createNewOrganizationButton(), not(isVisible()))
      );
    });
    it('should allow me, as a user, to see and copy my API key once and only once after creating it', async ({
      actor,
    }) => {
      const description = 'A new API key created under test.';
      await actor.attemptsTo(
        notes<MyNotes>().set('apiKey', null),
        LoginAsUser(),
        goToTheAdminLandingPageForOrganization(1),
        createNewApiKeyDescribedAs(description),
        Wait.until(createApiKeyDialog.apiKey(), isVisible()),
        notes<MyNotes>().set('apiKey', Value.of(createApiKeyDialog.apiKey())),
        Click.on(createApiKeyDialog.copyButton()),
        ExecuteScript.sync('return navigator.clipboard.readText()'),
        Ensure.that(
          LastScriptExecution.result<string>(),
          equals(notes().get('apiKey'))
        )
      );
    });
    it('should allow me, as a staffmember, to disable a key', async ({
      actor,
    }, testInfo) => {
      await actor.attemptsTo(
        ObtainAccessToken(),
        createApiKey(testInfo.title),
        LoginAsStaffer(),
        goToTheAdminLandingPageForOrganization(
          notes().get('first_organization_id')
        ),
        Log.the(notes().get('apiKeyId')),
        OrganizationApiKeysTable.container().scrollIntoView(),
        Wait.until(OrganizationApiKeysTable.keyId(), isPresent()),
        notes().set('keyEnabled', Text.of(OrganizationApiKeysTable.keyId())),
        Log.the(notes().get('keyEnabled')),
        Click.on(OrganizationApiKeysTable.rowContextMenuTrigger()),
        Wait.until(
          OrganizationApiKeysTable.rowContextMenuActivateKeyButton(),
          isVisible()
        ),
        Click.on(OrganizationApiKeysTable.rowContextMenuActivateKeyButton()),
        Click.on(GlobalDialog.acceptButton()),
        Ensure.that(
          Text.of(OrganizationApiKeysTable.status()),
          equals('Inactive')
        )
      );
    });
  });
});
