import { Attribute, By, PageElement, Value } from '@serenity-js/web';

import { toggleSwitch } from '../Organizations/questions';

export const metadataPlusSubscription = () =>
  PageElement.located(By.css('.subscription-item--METADATA_PLUS'));
export const metadataPlusSubscriptionSwitch = () =>
  PageElement.located(By.css('.v-input.v-switch'));
export const metadataPlusSubscriptionSwitchInput = () =>
  toggleSwitch().of(metadataPlusSubscription());
export const metadataPlusSubscriptionSwitchTrigger = () =>
  toggleSwitch().of(metadataPlusSubscription());
export const metadataPlusSubscriptionValue = () =>
  Value.of(metadataPlusSubscriptionSwitchInput());
export const subscriptionEditDialogCancelButton = () =>
  PageElement.located(By.css('[data-test-id="suscription-card__edit--close"]'));
export const checked = () => Attribute.called('aria-checked');
export const acceptButton = () =>
  PageElement.located(By.css('.button--accept'));
export const dialog = () => PageElement.located(By.css('.v-dialog'));
export const loadingSpinner = () =>
  PageElement.located(By.css('.v-input--is-loading'));
