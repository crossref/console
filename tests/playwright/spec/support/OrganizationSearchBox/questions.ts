import { equals } from '@serenity-js/assertions';
import { Answerable, QuestionAdapter } from '@serenity-js/core';
import { By, PageElement, PageElements, Text } from '@serenity-js/web';

export const organizationSearchResults = () =>
  PageElements.located(
    By.css('.search-for-organisations-results .v-list-item')
  ).describedAs('organisationn search results');

export const label = () => PageElement.located(By.css('.v-list-item-title'));

export const organizationSearchResultNames = () =>
  Text.ofAll(organizationSearchResults())
    .map((name) => name.trim())
    .describedAs('organisation search result names') as QuestionAdapter<
    string[]
  >;

export const organizationResultByName = (name: Answerable<string>) =>
  organizationSearchResults().where(Text.of(label()), equals(name)).first();

export const searchOrganizationsInput = () =>
  PageElement.located(
    By.css('[data-test-id="search-for-organizations-input"] input')
  );

export const searchOrganizationSubmitButton = () =>
  PageElement.located(
    By.css('[data-test-id="search-for-organizations-submit"]')
  );
