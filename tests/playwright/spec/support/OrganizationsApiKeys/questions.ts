import { By, PageElement } from '@serenity-js/web';

export const apiKeyTableRow = PageElement.located(By.css(''));
export const createApiKeyDialog = {
  copyButton: () =>
    PageElement.located(
      By.css('[data-test-id="key-mananger-api-key-created-copy-key-button"]')
    ),
  apiKey: () =>
    PageElement.located(
      By.css('[data-test-id="key-mananger-api-key-created-api-key"] input')
    ),
};
