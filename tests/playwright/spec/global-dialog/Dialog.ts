import { Answerable, Task } from '@serenity-js/core';
import { By, Click, Enter, PageElement } from '@serenity-js/web';

export const GlobalDialog = {
  inputField: () =>
    PageElement.located(By.css('.global-dialog__text-input input')),
  acceptButton: () =>
    PageElement.located(By.css('.global-dialog .button--accept')),
  cancelButton: () =>
    PageElement.located(By.css('.global-dialog .button--cancel')),
  container: () => PageElement.located(By.css('.global-dialog')),
};

export const enterTextIntoDialog = (text: Answerable<string>) =>
  Task.where(
    `#actor enters ${text} into the Globan Dialog`,
    Enter.theValue(text).into(GlobalDialog.inputField()),
    Click.on(GlobalDialog.acceptButton())
  );
