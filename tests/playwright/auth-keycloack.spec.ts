import { test, expect } from '@playwright/test';
import useConfig from '../support/useConfig';
import { env } from '../../src/env';

const config = useConfig('VUE_APP_TEST_');

const loginUser = async (page) => {
  // Runs before each test and signs in each page.
  // Go to http://localhost:8000/
  await page.goto(`${env().BASE_URL}/keys`);

  // Fill input[name="username"]
  await page
    .locator('input[name="username"]')
    .fill(config.value?.AUTH?.CL?.UI?.USERNAME);

  // Click input[name="password"]
  await page.locator('input[name="password"]').click();

  // Fill input[name="password"]
  await page
    .locator('input[name="password"]')
    .fill(config.value?.AUTH?.CL?.UI?.PASSWORD);

  // Click input:has-text("Sign In")
  await Promise.all([page.locator('input:has-text("Sign In")').click()]);
};

test('URL does not retain fragments after login @keycloak', async ({
  page,
}) => {
  await loginUser(page);
  await page.goto(`${env().BASE_URL}/keys`);
  await expect(page).toHaveURL(`${env().BASE_URL}/keys`);
});

test('Can access path restricted to a role I have when logged in @sandbox', async ({
  page,
}) => {
  await loginUser(page);
  await page.goto(`${env().BASE_URL}/keys`);
  await expect(page.locator('text=Organisation')).toBeVisible();
  await expect(page.locator('text=fake.user@crossref.org')).toBeVisible();
});

test('Cannot access path restricted to a role I do not have @sandbox', async ({
  page,
}) => {
  await page.goto('/keys');
  await expect(page.locator('text="Not Authorised"')).toBeVisible();
});
