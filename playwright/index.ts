import { vuetifyInstanceUnderTest } from '@/plugins/vuetify';
// Import styles, initialize component theme here.
// Styles are imported by vite-plugin-vuetify in ../playwright-ct.config.ts
// Material Design Icons font is included in ./index.html
// Other global CSS imports could be included here

import { beforeMount } from '@playwright/experimental-ct-vue/hooks';
import { useI18next } from '../src/composable/useI18n';
import I18NextVue from 'i18next-vue';
import { createPinia } from 'pinia';
import { store } from '../src/store';
import { getAppService } from '../src/statemachines/app.machine';

const pinia = createPinia();

beforeMount(async ({ app }) => {
  store.appService = getAppService();
  // Setup plugins here
  app.use(pinia);
  app.use(vuetifyInstanceUnderTest);
  const { i18next, initialized: i18nInitialized } = useI18next();
  // Wait for i18n framework to initialise before injecting
  await i18nInitialized.then(async () => {
    app.use(I18NextVue, { i18next });
  });
});
