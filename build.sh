#!/bin/sh

# Exit on any non zero return value
set -ex

npm ci
npm run build

echo "Built frontend"
