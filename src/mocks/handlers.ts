import { MockedRequest, rest } from 'msw';
import { UserDetails } from '../statemachines/auth.machine';
import { DepositRequestBody } from '@/utils/deposit';
import {
  grantidFullExampleValidXML,
  depositResponse200Processed,
  depositResponse200ProcessedWithErrors,
  depositResponse403Unauthorised,
  grantidFullExampleInvalidXML,
} from '../../tests/unit/XmlDeposit/exampleXMLs';
import { AuthRequestBody } from '@/common/types';
import {
  LOGIN_USERNAME_ONE_ROLE,
  LOGIN_PASSWORD_ONE_ROLE,
} from '@/../tests/constants';

type LoginResponse = {
  username: string;
  firstName: string;
};

export const deposit401UnauthorisedResponse = {
  success: false,
  errorMessage: 'Wrong credentials. Incorrect username or password.',
  authenticated: false,
  authorised: false,
};

export const deposit500ServerErrorResponse = '<html><body>500 Server Error occurred</body></html>';

const isBrowser = (req: FormData | DepositRequestBody) => {
  if (req instanceof FormData) {
    return false;
  }
  return true;
};

const getMdFile = async (req: FormData | DepositRequestBody) => {
  if (req instanceof FormData) {
    return req.get('mdFile');
  }
  return await (req as DepositRequestBody).mdFile.text();
};

const getUsr = (
  requestBody: FormData | DepositRequestBody | AuthRequestBody
) => {
  if (requestBody instanceof FormData) {
    return requestBody.get('usr') as string;
  }

  return (requestBody as DepositRequestBody | AuthRequestBody).usr;
};

const getValueFromRequest = (
  req: MockedRequest,
  propertyName: string
): FormDataEntryValue | string | null => {
  if (req.headers.get('content-type') === 'application/x-www-form-urlencoded') {
    const searchParams = new URLSearchParams(req.body as string);
    const value = searchParams.get(propertyName);
    return value ?? null;
  }
  if (req.body instanceof FormData) {
    return req.body.get(propertyName);
  }
  if (req.body instanceof Object) {
    return req.body?.[propertyName];
  }
  return null;
};

const checkCredentials = (username: string, password: string) => {
  if (username && password === 'badpass') {
    return false;
  }
  if (password === 'undefined') {
    return false;
  }
  return true;
};
export default [
  rest.post<AuthRequestBody>('*/servlet/login', (req, res, ctx) => {
    const username = getValueFromRequest(req, 'usr') as string;
    const password = getValueFromRequest(req, 'pwd') as string;
    const role = getValueFromRequest(req, 'role');
    let roles = [
      'psychoceramics1',
      'psychoceramics2',
      'psychoceramics3',
      'psychoceramics4',
      'psychoceramics5',
    ];
    if (username.match(/noroles|ams/)) {
      roles = [];
    }
    if (username.match(/onerole/)) {
      roles = ['onerole'];
    }
    if (!checkCredentials(username, password)) {
      return res(ctx.status(401), ctx.json(deposit401UnauthorisedResponse));
    }
    // User has only one role - automatically login and redirect
    if (
      username === LOGIN_PASSWORD_ONE_ROLE &&
      password === LOGIN_USERNAME_ONE_ROLE
    ) {
      return res(
        ctx.status(200),
        ctx.json({
          success: true,
          message: 'Authorised',
          redirect: null,
          roles: ['psychoceramics1andOnly1'],
          authenticated: true,
          authorised: false,
        })
      );
    }
    // Role Authorisation requested
    if (typeof role === 'string' && role.length > 0) {
      if (username.match(/badrole/)) {
        return res(
          ctx.status(401),
          ctx.json({
            success: false,
            errorMessage: `User '${username}' cannot assume specified role '${role}'`,
            redirect: null,
            authenticated: true,
            authorised: false,
          })
        );
      }
      if (!checkCredentials(username, password)) {
        return res(
          ctx.status(401),
          ctx.json({
            success: false,
            errorMessage: 'Login error. No token provided.',
            authenticated: false,
            authorised: false,
          })
        );
      }
      return res(
        ctx.status(200),
        ctx.set({
          Authorization: 'Bearer TOKEN',
        }),
        ctx.json({
          success: true,
          message: 'Authorised',
          redirect: null,
          authenticated: true,
          authorised: true,
          refresh_token: 'RTOKEN',
        })
      );
      if (roles.length < 1) {
        return res(
          ctx.status(200),
          ctx.json({
            success: false,
            message: 'Authenticated',
            roles: roles,
          })
        );
      }
      return res(
        ctx.status(200),
        ctx.json({
          success: true,
          message: 'Authorised',
          redirect: null,
          authenticated: true,
          authorised: true,
        })
      );
    }
    if (roles.length < 1) {
      return res(
        ctx.status(200),
        ctx.json({
          success: false,
          message: 'Authenticated',
          roles: roles,
        })
      );
    }
    return res(
      ctx.status(401),
      ctx.json({
        success: false,
        message: 'Authenticated',
        roles: roles,
        authenticated: true,
        authorised: false,
      })
    );
  }),
  rest.post<UserDetails, LoginResponse>('/login', (req, res, ctx) => {
    const { username } = req.body;
    return res(
      ctx.json({
        username,
        firstName: username,
      })
    );
  }),
  rest.post<any>('*/convert', async (req, res, ctx) => {
    return res(
      ctx.delay(1000),
      ctx.status(200),
      ctx.xml('<xml>valid</xml>'),
    );
  }),
  rest.options('*/v2/deposits', (req, res, ctx) => {
    return res(
      ctx.set({
        'Access-Control-Allow-Origin': '*',
      })
    );
  }),
  rest.post<FormData | DepositRequestBody>(
    '*/v2/deposits',
    async (req, res, ctx) => {
      return res(
        ctx.delay(1000),
        ctx.status(200),
        ctx.xml(depositResponse200Processed)
      );
    }
  ),
];
