import { useAlert } from './../composable/useAlert';
import { useApi } from '@/composable/useApi';
import { ref, readonly, Ref } from 'vue';
// FIXME: we should make it possible to alter the available subscriptions at runtime
import { AddOrgSubscriptionSubscriptionEnum } from '@/generated/openapi/apis/DefaultApi';

const { alertError } = useAlert();

// Get the available subcscriptions from the openapi client
export const SUBSCRIPTIONS = Object.values(AddOrgSubscriptionSubscriptionEnum);
const subscriptionKeys = Object.keys(AddOrgSubscriptionSubscriptionEnum);
export type Subscription = (typeof subscriptionKeys)[number];
export type SubscriptionsWithStatus = {
  [key: Subscription]: {
    active: boolean;
    updating: boolean;
  };
};
export type SubscriptionChangeRequest = {
  memberId: number;
  subscription: Subscription;
  active: boolean;
};

export interface useSubscriptions {
  rawSubscriptions: Subscription[];
  subscriptions: Readonly<Subscription[]>;
  loading: Ref<boolean>;
  load: () => void;
  update: (request: SubscriptionChangeRequest) => void;
  showSpinner: () => void;
  stopSpinner: () => void;
}

const { api } = useApi();

/**
 * Check whether a subscription is active
 *
 * @param subscription
 * @param activeSubscriptions
 * @returns boolean
 */
export const isSubscriptionActive = (
  subscription: Subscription,
  activeSubscriptions: Subscription[] = SUBSCRIPTIONS
) => activeSubscriptions.includes(subscription);

/**
 * Map the array of SUBSCRIPTIONS to an dictionary containing
 * the subscription name as key, the value as an object
 * storing the subscription's status
 *
 * @param activeSubscriptions the list of currently active subscriptions
 */
export const mapSubscriptions = (activeSubscriptions: Subscription[]) =>
  SUBSCRIPTIONS.reduce((obj: SubscriptionsWithStatus, item) => {
    obj[item] = {
      active: isSubscriptionActive(item, activeSubscriptions),
      updating: false,
    };
    return obj;
  }, {});

/**
 * De/active a subscruption for an Organization
 *
 * @param request SubscriptionChangeRequest
 */
export const updateSubscriptionForOrganization = async (
  request: SubscriptionChangeRequest
) => {
  const changeRequest = {
    memberId: request.memberId,
    subscription:
      // We know here the passed Subscription will be one of AddOrgSubscriptionSubscriptionIdEnum
      request.subscription as AddOrgSubscriptionSubscriptionEnum,
  };
  if (request.active) {
    return api.addOrgSubscription(changeRequest);
  }
  if (!request.active) {
    return api.removeOrgSubscription(changeRequest);
  }
};

/**
 * Get all active Subscriptions for an Organization
 * @param memberId number
 */
export const getSubscriptionsForOrganization = async (memberId: number) => {
  return await api.getOrgSubscription({
    memberId: memberId,
  });
};

/**
 * A composable holding reactive state for an Organization's Subscriptions and the
 * means to update it
 * @param memberId number
 */
export const useSubscriptions = (memberId: number) => {
  const loading = ref(false);
  const rawSubscriptions = ref<Subscription[]>();
  const subscriptions = ref<SubscriptionsWithStatus>({});

  const load = async () => {
    loading.value = true;
    try {
      getSubscriptionsForOrganization(memberId).then((response) => {
        rawSubscriptions.value = response;
        subscriptions.value = mapSubscriptions(rawSubscriptions.value);
      });
    } finally {
      loading.value = false;
    }
  };

  const showSpinner = (request: SubscriptionChangeRequest) => {
    subscriptions.value[request.subscription].updating = true;
  };

  const stopSpinner = (request: SubscriptionChangeRequest) => {
    subscriptions.value[request.subscription].updating = false;
  };

  const update = async (request: SubscriptionChangeRequest) => {
    try {
      subscriptions.value[request.subscription].updating = true;
      await updateSubscriptionForOrganization(request);
      load();
    } catch (e) {
      console.error(e);
      alertError(
        'There was an error updating the subscription. Please try again.'
      );
      throw e;
    } finally {
      subscriptions.value[request.subscription].updating = false;
    }
  };

  return {
    rawSubscriptions: readonly(rawSubscriptions),
    subscriptions: readonly(subscriptions),
    loading,
    load,
    update,
    showSpinner,
    stopSpinner,
  };
};
