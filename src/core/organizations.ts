import { AddOrgRequest } from './../generated/openapi/apis/DefaultApi';
import { useApi } from '@/composable/useApi';
import { useAuth, UserInfo } from '@/composable/useAuth';
import { Organization } from '@/generated/openapi/models/Organization';
import { RESOURCE_MANAGER_API_DEFAULT_PAGE_SIZE } from '@/utils/fetchers';
import { unref } from 'vue';
import { uuid4 } from '@sentry/utils';
import { generateUuid } from '@/utils/ids';

const { api } = useApi();
const auth = useAuth();
/**
 * Update the name of an Organization
 * @param memberId
 * @param newName
 */
export const updateNameForOrganization = (orgId: string, newName: string) => {
  return api.updateOrg({
    organization: {
      orgId: orgId,
      name: newName,
    },
  });
};

/**
 * Get Organizations Visbible to a user.
 * Used to populate the Organization search dropdown.
 * Staff can see all organizations, Users can see (or search within)
 * organizations to which they belong.
 * Optionally filtered by a query.
 * @param userInfo
 * @param query
 */
export const getOrganizationsVisibleToUser = async (
  userInfo: UserInfo,
  query?: string | null
) => {
  await auth.initialized;
  if (unref(userInfo.isStaff)) {
    return api.findOrgs({
      name: query ? query : '',
      page: 1,
      pagesize: RESOURCE_MANAGER_API_DEFAULT_PAGE_SIZE,
    });
  }
  return api.listUserOrgs({ userId: userInfo.id.value as string });
};

export const createOrganization = (request: AddOrgRequest) => {
  if (!request.organization) {
    throw new Error(
      'No Organization included in the createOrganisation request'
    );
  }
  // We must provide a random UUID for the request to succeed
  request.organization.orgId = generateUuid();
  return api.addOrg(request);
};
