import { inject, InjectionKey, provide } from 'vue';
import { useActor, useInterpret } from '@xstate/vue';
import { createMachine, assign, InterpreterFrom } from 'xstate';
import { useInspector } from '@/statemachines/utils';

export const exampleMachine = createMachine(
  {
    predictableActionArguments: true,
    schema: {
      context: {} as { count: number },
      events: {} as
        | { type: 'TOGGLE' }
        | { type: 'SOME_OTHER_EVENT'; text: string }
        | { type: 'SOME_EVENT' },
    },
    id: 'toggle',
    tsTypes: {} as import('./example.machine.typegen').Typegen0,
    initial: 'inactive',
    states: {
      inactive: {
        on: { TOGGLE: 'active' },
      },
      active: {
        entry: 'incrementCount',
        on: { TOGGLE: 'inactive' },
      },
    },
  },
  {
    actions: {
      incrementCount: assign({ count: (ctx) => ctx.count + 1 }),
    },
  }
);

export type ToggleService = InterpreterFrom<typeof exampleMachine>;
export const toggleServiceSymbol: InjectionKey<ToggleService> =
  Symbol('toggle.service');

export function getToggleService() {
  /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
  // @ts-ignore
  const service = useInterpret(exampleMachine.withContext({}), {
    devTools: useInspector(),
  });
  return service;
}

export function provideToggleService() {
  const service = getToggleService();
  /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
  // @ts-ignore
  provide(toggleServiceSymbol, service);

  return service;
}

export function useToggleService() {
  const service = inject(toggleServiceSymbol);

  if (!service) {
    throw new Error('Toggle service not provided.');
  }

  return useActor(service);
}
