// This file was automatically generated. Edits will be overwritten

export interface Typegen0 {
  '@@xstate/typegen': true;
  internalEvents: {
    'error.platform.FormDataMachine.submitRecord.depositRecord:invocation[0]': {
      type: 'error.platform.FormDataMachine.submitRecord.depositRecord:invocation[0]';
      data: unknown;
    };
    'error.platform.FormDataMachine.submitRecord.generateXML:invocation[0]': {
      type: 'error.platform.FormDataMachine.submitRecord.generateXML:invocation[0]';
      data: unknown;
    };
    'xstate.init': { type: 'xstate.init' };
  };
  invokeSrcNameMap: {
    checkCredentials: 'done.invoke.FormDataMachine.submitRecord.checkCredentials:invocation[0]';
    generateXMLString: 'done.invoke.FormDataMachine.submitRecord.generateXML:invocation[0]';
    makeDeposit: 'done.invoke.FormDataMachine.submitRecord.depositRecord:invocation[0]';
  };
  missingImplementations: {
    actions: 'handleDepositFailure';
    delays: never;
    guards: never;
    services: never;
  };
  eventsCausingActions: {
    assignCredentialsToContext: 'CREDENTIALS_VERIFIED';
    clearCurrentRecord: 'START_EDITING_NEW_RECORD';
    clearFormData: 'DEPOSIT_RECORD_SUCCESS' | 'RESTART';
    conditionallyAssignCurrentRecord: 'SUBMIT_RECORD';
    goToSubmissionCompleteRoute: 'DEPOSIT_RECORD_SUCCESS';
    handleDepositFailure: 'DEPOSIT_RECORD_FAILURE';
    logDepositFailure: 'DEPOSIT_RECORD_FAILURE';
    moveRecordToCompleted: 'DEPOSIT_RECORD_SUCCESS';
    notifyDepositError: 'DEPOSIT_RECORD_FAILURE';
    notifyXmlError: 'error.platform.FormDataMachine.submitRecord.generateXML:invocation[0]';
    requestCredentials: 'REQUEST_CREDENTIALS';
    setErrorMessageFromInvoke:
      | 'error.platform.FormDataMachine.submitRecord.depositRecord:invocation[0]'
      | 'error.platform.FormDataMachine.submitRecord.generateXML:invocation[0]';
    updateDepositResponse: 'DEPOSIT_RECORD_FAILURE' | 'DEPOSIT_RECORD_SUCCESS';
    updateXMLString: 'XML_GENERATED';
  };
  eventsCausingDelays: {};
  eventsCausingGuards: {};
  eventsCausingServices: {
    checkCredentials: 'SUBMIT_RECORD';
    generateXMLString: 'CREDENTIALS_VERIFIED';
    makeDeposit: 'XML_GENERATED';
  };
  matchesStates:
    | 'editingForm'
    | 'idle'
    | 'submitRecord'
    | 'submitRecord.checkCredentials'
    | 'submitRecord.depositRecord'
    | 'submitRecord.generateXML'
    | 'submitRecord.submissionComplete'
    | {
        submitRecord?:
          | 'checkCredentials'
          | 'depositRecord'
          | 'generateXML'
          | 'submissionComplete';
      };
  tags: never;
}
