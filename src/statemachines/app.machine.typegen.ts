// This file was automatically generated. Edits will be overwritten

export interface Typegen0 {
  '@@xstate/typegen': true;
  internalEvents: {
    'xstate.init': { type: 'xstate.init' };
  };
  invokeSrcNameMap: {};
  missingImplementations: {
    actions: never;
    delays: never;
    guards: never;
    services: never;
  };
  eventsCausingActions: {
    clearMessage: 'CLEAR';
    reportLoginBoxClosed: 'LOGIN_BOX_CLOSED';
    requestCredentials: 'REQUEST_CREDENTIALS';
    setMessage: 'ALERT';
    updateLocale: 'UPDATE_LOCALE';
  };
  eventsCausingDelays: {};
  eventsCausingGuards: {};
  eventsCausingServices: {};
  matchesStates: 'active' | 'idle';
  tags: never;
}
