import { setActivePinia, createPinia } from 'pinia';
import { describe, it, beforeEach, vi, expect } from 'vitest';
import { useNotificationStore } from '@/stores/notifications';

describe('NotificationStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
    vi.useFakeTimers(); // Use fake timers to control setTimeout behavior
  });

  afterEach(() => {
    vi.clearAllMocks();
    vi.useRealTimers(); // Restore real timers after each test
  });

  it('should add a notification and show it', () => {
    const notificationStore = useNotificationStore();
    notificationStore.addNotification('Test Message', 'Close');

    expect(notificationStore.notifications.length).toBe(0); // Notification should have moved to currentNotification
    expect(notificationStore.currentNotification).not.toBeNull();
    expect(notificationStore.currentNotification?.message).toBe('Test Message');
    expect(notificationStore.currentNotification?.actionText).toBe('Close');
  });

  it('should automatically dismiss a notification after 4 seconds if autoDismiss is true', () => {
    const notificationStore = useNotificationStore();
    notificationStore.addNotification('Auto-Dismiss Message', 'Close', undefined, true);

    expect(notificationStore.currentNotification?.message).toBe('Auto-Dismiss Message');

    vi.advanceTimersByTime(4000); // Fast-forward time by 4000ms

    expect(notificationStore.currentNotification).toBeNull(); // Notification should be dismissed after 4 seconds
  });

  it('should not auto-dismiss if autoDismiss is false', () => {
    const notificationStore = useNotificationStore();
    notificationStore.addNotification('No Auto-Dismiss', 'Close', undefined, false);

    expect(notificationStore.currentNotification?.message).toBe('No Auto-Dismiss');

    vi.advanceTimersByTime(4000); // Fast-forward time by 4000ms

    expect(notificationStore.currentNotification?.message).toBe('No Auto-Dismiss'); // Notification should still be active
  });

  it('should trigger the callback and dismiss the notification when CTA button is clicked', () => {
    const notificationStore = useNotificationStore();
    const callback = vi.fn();
    notificationStore.addNotification('Callback Test', 'Close', callback, false);

    expect(notificationStore.currentNotification?.message).toBe('Callback Test');

    notificationStore.triggerCallbackAndDismiss(notificationStore.currentNotification!.id);

    expect(callback).toHaveBeenCalled(); // Ensure callback is executed
    expect(notificationStore.currentNotification).toBeNull(); // Notification should be dismissed
  });

  it('should dismiss the notification without running the callback on auto-dismiss', () => {
    const notificationStore = useNotificationStore();
    const callback = vi.fn();
    notificationStore.addNotification('Auto-Dismiss No Callback', 'Close', callback, true);

    expect(notificationStore.currentNotification?.message).toBe('Auto-Dismiss No Callback');

    vi.advanceTimersByTime(4000); // Fast-forward time by 4000ms

    expect(callback).not.toHaveBeenCalled(); // Callback should not be called during auto-dismiss
    expect(notificationStore.currentNotification).toBeNull(); // Notification should be dismissed
  });

  it('should correctly manage a queue of multiple notifications', () => {
    const notificationStore = useNotificationStore();

    // Add multiple notifications
    notificationStore.addNotification('First Message', 'Close');
    notificationStore.addNotification('Second Message', 'Close');
    notificationStore.addNotification('Third Message', 'Close');

    expect(notificationStore.currentNotification?.message).toBe('First Message');
    expect(notificationStore.notifications.length).toBe(2); // Two notifications should be in queue

    // Dismiss the first notification
    notificationStore.dismissNotification(notificationStore.currentNotification!.id);

    expect(notificationStore.currentNotification?.message).toBe('Second Message');
    expect(notificationStore.notifications.length).toBe(1);

    // Dismiss the second notification
    notificationStore.dismissNotification(notificationStore.currentNotification!.id);

    expect(notificationStore.currentNotification?.message).toBe('Third Message');
    expect(notificationStore.notifications.length).toBe(0);

    // Dismiss the third notification
    notificationStore.dismissNotification(notificationStore.currentNotification!.id);

    expect(notificationStore.currentNotification).toBeNull(); // No more notifications in queue
  });
});
