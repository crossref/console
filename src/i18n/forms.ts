import { useI18next } from '@/composable/useI18n';
import { env } from '@/env';
import {
  defaultErrorTranslator,
  ErrorTranslator,
  Translator,
} from '@jsonforms/core';
import { ErrorObject } from 'ajv';
import { TOptions } from 'i18next';
import { I18N_FALLBACK_NAMESPACE, I18nNamespace, SupportedLocales } from './i18n';

import enLocalizeErrors from 'ajv-i18n/localize/en';
import deLocalizeErrors from 'ajv-i18n/localize/de';
import esLocalizeErrors from 'ajv-i18n/localize/es';
import frLocalizeErrors from 'ajv-i18n/localize/fr';

import memoize from 'lodash/memoize';

type $Dictionary<T = unknown> = Record<string, T>;

/**
 * Creates a translator function tailored for JSON Forms, supporting namespaced translations and
 * adjusting for syntax compatibility with Fluent FTL. It memoizes translations for performance.
 *
 * @param namespace - Optional namespace to scope the translation keys.
 * @returns A memoized translator function that resolves translations based on the provided key,
 * default message, and optional value replacements, with special handling for non-existent translations.
 *
 * @example
 * const translator = createJsonFormsTranslator('myNamespace');
 * const message = translator('form.field.label', 'Default Label');
 */
export const createJsonFormsTranslator = (
  namespace?: I18nNamespace
): Translator => {
  const { i18next } = useI18next();
  const translator = (
    key: string,
    defaultMessage: string | undefined,
    values?: $Dictionary
  ) => {
    // sanity check values as JSON Forms does not impose any restrictions here
    const valuesToUse =
      values && typeof values === 'object' && !Array.isArray(values)
        ? values
        : undefined;

    // dots are the default separator in JSON Forms. This has a special meaning in Fluent FTL.
    // As do : and $. Replace with underscore for Fluent FTL syntax comptability.
    const transformKey = key ?
      key
      .replace(/\./g, '_')
      .replace(/:/g, '__')
      .replace(/\$/g, '___')
      : '';

    const fluentOptions: TOptions = {
      ...valuesToUse,
      ns: namespace ? [namespace, I18N_FALLBACK_NAMESPACE] : undefined,
      defaultValue: defaultMessage,
    };
    // Fluent will return the key as value if it doesn't exist. However JSON Forms relies on
    // undefined being returned when the key does not exist. We therefore need to check manually.
    if (
      defaultMessage === undefined &&
      !i18next.exists(transformKey, fluentOptions)
    ) {
      if (env().isI18NDebugLogging) {
        console.debug(
          `There is no message for key "${transformKey}" in namespace "${
            namespace || 'default-namespace'
          }"`
        );
      }
      // XXX The Translator type is not flexible enough. This forced cast can be removed once the
      // latest version of JSON Forms is consumed in which the type is improved
      return undefined as unknown as string;
    }
    const result = i18next.t(transformKey, fluentOptions);
    if (env().isI18NDebugLogging) {
      console.debug(
        `Determined message "${result}" for key "${key}" in namespace "${
          namespace || 'default-namespace'
        }"`
      );
    }
    return result;
  };
  return memoize(translator, (key, defaultMessage, values) => {
    /**
     * XXX 'values' contain both: context values from JSON Forms or custom variables to resolve
     * in custom 'Console' renderers.
     *
     * At the moment we don't use any of the JSON Forms context values to show a different translation
     * so we ignore them here. Instead we're looking for a memoize string which will be handed over
     * by custom renderers.
     */
    const memoizedValues =
      (values && typeof values === 'object' && values.memoize) || undefined;
    return `${i18next.language}${key}${defaultMessage}${memoizedValues}`;
  });
};

/**
 * Creates an error translator function for JSON Forms that localizes AJV validation error messages
 * based on the specified locale. It integrates with `ajv-i18n` to provide localized error messages.
 *
 * @param locale - The locale to use for error message translation.
 * @returns An error translator function that localizes error messages according to the given locale.
 *
 * @example
 * const errorTranslator = createJsonFormsErrorTranslator('en');
 * const localizedMessage = errorTranslator(errorObject);
 */
export const createJsonFormsErrorTranslator =
  (locale: SupportedLocales): ErrorTranslator =>
  (error: ErrorObject, ...other) => {
    switch (locale) {
      case 'en':
        enLocalizeErrors([error]);
        break;
      case 'de':
        deLocalizeErrors([error]);
        break;
      case 'es':
        esLocalizeErrors([error]);
        break;
      case 'fr':
        frLocalizeErrors([error]);
        break;
    }
    return defaultErrorTranslator(error, ...other);
  };

/**
 * Prepares a values object for translation in JSON Forms, adding a memoization key for efficient caching.
 *
 * @param values - The original values object to be used for translation variable replacements.
 * @returns An enhanced values object including a memoization key, facilitating efficient caching of translations.
 *
 * @example
 * const memoizedValues = memoizeValues({ name: 'John Doe' });
 * // Use `memoizedValues` in a translator function call
 */
export const memoizeValues = (values: Record<any, any>): Record<any, any> => {
  return {
    ...values,
    memoize: Object.values(values).join(''),
  };
};
