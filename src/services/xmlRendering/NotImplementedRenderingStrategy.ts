import { ContentRegistrationType, XMLGenerationStrategy } from '@/common/types';

export class NotImplementedRenderingStrategy implements XMLGenerationStrategy {
  async generateXML(
    recordType: ContentRegistrationType,
    data: FormData,
    prettyPrint = false
  ): Promise<string> {
    console.log(
      `The generateXML method for the requested content type ${recordType} is not implemented.`
    );

    const xml = prettyPrint
      ? `<?xml version="1.0" encoding="UTF-8"?>\n<!-- Not Implemented -->\n<document/>`
      : '<?xml version="1.0" encoding="UTF-8"?><document/>';

    return xml;
  }
}
