export const ERROR_MESSAGES = {
  NETWORK_ERROR: 'A network error occurred.',
  API_ERROR: 'An error occurred while processing your request.',
  VALIDATION_ERROR: 'The input data is not valid.',
  INSECURE_CONTEXT:
    'This functionality is only avaible in secure contexts (HTTPS or localhost).',
  CLIPBOARD_COPY_ERROR: 'There was an error copying to the clipboard.',
};
