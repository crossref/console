export const ALERT_LEVEL_SUCCESS = 'success';
export const ALERT_LEVEL_INFO = 'info';
export const ALERT_LEVEL_WARNING = 'warning';
export const ALERT_LEVEL_ERROR = 'error';
