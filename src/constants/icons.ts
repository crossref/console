export enum Icons {
  Download = 'mdi-content-save',
  Edit = 'mdi-pencil',
  Delete = 'mdi-delete',
  Settings = 'mdi-cog-outline',
  Help = 'mdi-help-circle-outline',
}
