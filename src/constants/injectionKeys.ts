import { JsonFormsI18nState } from '@jsonforms/core';
import {ComputedRef, InjectionKey} from 'vue';

export const JsonFormsI18nStateSymbol: InjectionKey<ComputedRef<JsonFormsI18nState>> =
  Symbol('jsonforms.i18n');
