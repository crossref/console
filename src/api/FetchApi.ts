import { BaseApi } from '@/api/BaseApi';
export class FetchApi extends BaseApi {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  async fetch(url: string) {
    const response = await fetch(`${this.baseUrl}${url}`);
    return await response.json();
  }
}
