import { FetchParams, Middleware, ResponseContext } from '@/generated/openapi';
import { useAuth } from '@/composable/useAuth';

const { getToken } = useAuth();

export class ApiAuthHeaderMiddleware implements Middleware {
  public async pre(context: ResponseContext): Promise<FetchParams | void> {
    const accessToken = await getToken();
    return {
      url: context.url,
      init: {
        ...context.init,
        headers: new Headers({
          ...context.init.headers,
          Authorization: `Bearer ${accessToken}`,
        }),
      },
    };
  }

  public post(context: ResponseContext): Promise<Response | void> {
    return Promise.resolve(context.response);
  }
}
