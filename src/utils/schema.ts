import { createAjv } from '@pvale/vue-vuetify';
import { AnySchema } from 'ajv';

/**
 * Modifies the handed over data according to the 'default' attributes in the given schema.
 * Returns the handed over data again for convenience.
 */
export const populateDefaults = <T>(data: T, schema: AnySchema): T => {
  const ajv = createAjv({ useDefaults: true });
  ajv.compile(schema)(data);
  return data;
};
