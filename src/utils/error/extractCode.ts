import { ResponseError } from '@/generated/openapi';

export const extractErrorCode = async (e: unknown): Promise<number | null> => {
  if (e instanceof ResponseError) {
    return e.response.status;
  }
  return null;
};
