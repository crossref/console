import { extractErrorMessageAsync } from '@/utils/error/extractMessage';
import { useAlert } from '@/composable/useAlert';

const { alertError } = useAlert();

/**
 * A utility function that extracts an error message from the provided error and alerts the user if a message exists.
 * @param {unknown} error - The error to extract the message from and alert.
 * @param {boolean} returnDefaultMessage - Whether to return the default error message when extraction fails.
 * @returns {Promise<void>} A Promise that resolves when the operation completes.
 */
export async function alertErrorIfMessage(
  error: unknown,
  returnDefaultMessage = true
): Promise<void> {
  // Extract the error message
  const message = await extractErrorMessageAsync(error, returnDefaultMessage);

  // If a message exists, alert the user
  if (message !== null) {
    alertError(message);
  }
}
