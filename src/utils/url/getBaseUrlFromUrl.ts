export const getBaseUrlFromUrl = (url: string): string => {
  const parsedUrl = new URL(url);
  const baseUrl = parsedUrl.origin + parsedUrl.pathname;
  return baseUrl;
};
