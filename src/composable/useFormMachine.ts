import { useAppMachine } from '@/composable/useAppMachine';
import { computed } from 'vue';
import { useActor } from '@xstate/vue';
import { store } from '@/store';

const appMachine = store.appService;

const formMachine = computed(
  () => appMachine?.getSnapshot()?.children.formDataMachine
);

export const useFormMachine = () => {
  if (formMachine.value) {
    return useActor(formMachine.value);
  } else {
    throw new Error('formMachine is undefined');
  }
};
