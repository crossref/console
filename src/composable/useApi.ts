import { ApiAuthHeaderMiddleware } from '@/api/authHeaderMiddleware';
import UnauthorizedMiddleware from '@/api/authUnauthorizedMiddleware';
import { getApiClient } from '@/api/resourceManagementApi';

const apiAuthMiddleware = new ApiAuthHeaderMiddleware();
const unauthorizedMiddleware = new UnauthorizedMiddleware();
const api = getApiClient([apiAuthMiddleware, unauthorizedMiddleware]);

export const useApi = () => {
  return {
    api,
  };
};
