export class WebResource {
  public readonly url;

  constructor(url: string) {
    this.url = url;
  }
}
