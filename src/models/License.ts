class License {
  public readonly url: string;

  constructor(url: string) {
    this.url = url;
  }

  public toJson() {
    return this.url;
  }
}
