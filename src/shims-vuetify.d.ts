declare module 'vuetify/lib/framework' {
  import Vuetify from 'vuetify';
  export default Vuetify;
}

declare module 'vuetify/lib/locale' {
  const de;
  const en;
  const es;
  const fr;
}
