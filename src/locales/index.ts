import {
  de as deVuetifyDefault,
  en as enVuetifyDefault,
  es as esVuetifyDefault,
  fr as frVuetifyDefault,
} from 'vuetify/lib/locale';

export const en = {
  $vuetify: enVuetifyDefault,
};

export const de = {
  $vuetify: deVuetifyDefault,
};

export const es = {
  $vuetify: esVuetifyDefault,
};

export const fr = {
  $vuetify: frVuetifyDefault,
};
