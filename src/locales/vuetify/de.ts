import { de } from 'vuetify/locale';

export default {
  ...de,
  language_menu_button: 'Sprache',
};
