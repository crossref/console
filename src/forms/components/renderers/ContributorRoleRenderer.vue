**
* Custom renderer for Contributor Roles.
* This component provides renderer for handling contributor roles
* within a form. It uses JSON Forms and Vuetify components to render a
* selectable interface for contributor roles with additional logic for role sequence management.
*/
<template>
  <control-wrapper
    v-bind="controlWrapper"
    :styles="styles"
    :is-focused="isFocused"
    :applied-options="appliedOptions">
    <horizontal-label-wrapper applied-options="appliedOptions">
      <template #label>{{ computedLabel }}</template>
        <v-hover v-slot="{ isHovering }">
          <info-text :msg="infoText.msg" :learn-more-link="infoText.link"/>
          <v-select
            :id="control.id + '-input'"
            v-disabled-icon-focus
            :class="styles.control.input"
            :disabled="!control.enabled"
            :autofocus="appliedOptions.focus"
            :placeholder="appliedOptions.placeholder"
            :label="computedLabel"
            :hint="control.description"
            :persistent-hint="persistentHint()"
            :required="control.required"
            :error-messages="control.errors"
            :clearable="isHovering"
            :model-value="control.data"
            :items="control.options"
            :item-title="(item) => t(item.label, item.label)"
            item-value="value"
            v-bind="vuetifyProps('v-select')"
            @update:modelValue="localOnChange"
            @focus="isFocused = true"
            @blur="handleBlur" />
        </v-hover>
    </horizontal-label-wrapper>
  </control-wrapper>
</template>


<script lang="ts">
import { ControlElement, JsonFormsSubStates, Resolve } from '@jsonforms/core';
import { RendererProps, rendererProps, useJsonFormsEnumControl } from '@jsonforms/vue';
import { ControlWrapper, useVuetifyControl, DisabledIconFocus, useTranslator } from '@pvale/vue-vuetify';
import {computed, defineComponent, inject, onMounted, ref} from 'vue';
import { open as openDialog } from '@/composable/useGlobalDialog';
import { useAlert } from '@/composable/useAlert';
import { extractErrorMessageAsync } from '@/utils/error/extractMessage';
import { ContributorRole, ContributorSequence } from '@/models/Contributor';
import InfoText from "@/forms/components/common/InfoText.vue";
import {useInfoText} from "@/forms/compositions";
import {JsonFormsI18nStateSymbol} from "@/constants/injectionKeys";
import HorizontalLabelWrapper from "@/forms/components/renderers/HorizontalLabelWrapper.vue";

function replaceLastSegment(path: string, newSegment: string): string {
  const segments = path.split('.');
  segments[segments.length - 1] = newSegment;
  return segments.join('.');
}

const controlRenderer = defineComponent({
  name: 'ContributorRoleRenderer',
  methods: {ref},
  components: {
    HorizontalLabelWrapper,
    InfoText,
    ControlWrapper,
  },
  directives: {
    DisabledIconFocus,
  },
  props: {
    ...rendererProps<ControlElement>(),
  },
  setup(props: RendererProps<ControlElement>) {
    /**
     * Provides alert functionalities for displaying error messages.
     */
    const { alertError } = useAlert();
    /**
     * Translator function for internationalization.
     */
    const t = useTranslator();
    /**
     * Injected JSON Forms state.
     * Throws an error if JSON Forms context is not available.
     */
    const jsonforms = inject<JsonFormsSubStates>('jsonforms');
    if (!jsonforms) {
      throw "'jsonforms' couldn't be injected. Are you within JSON Forms?";
    }

    const i18n = inject(JsonFormsI18nStateSymbol, null);

    /**
     * Control object for handling form data, validation, and state changes.
     */
    const control = useVuetifyControl(useJsonFormsEnumControl(props), (value) =>
      value !== null ? value : undefined
    );

    const infoText = useInfoText(control, i18n);


    /**
     * Computes the path to the property tracking the contributors in the form data.
     * Slices the last two elements from the path to remove the field name (usually 'role') and array index.
     */
    const contributorsPath = computed(() => {
      const pathSegments = control.control.value.path.split('.');
      const newPathSegments = pathSegments.slice(0, -2);
      return newPathSegments.join('.');
    });

    /**
     * Computes the data stored within the contributors property.
     * Resolves and retrieves contributor data from the JSON Forms state.
     */
    const contributorsData = computed(() =>
      Resolve.data(jsonforms.core?.data, contributorsPath.value)
    );

    /**
     * Gets the json path to the element within the contributors array.
     */
    const currentElementPath = computed(() => {
      const pathSegments = control.control.value.path.split('.');
      const newPathSegments = pathSegments.slice(0, 3);
      return newPathSegments.join('.');
    });

    /**
     * Computes the data stored in the current contributor element.
     */
    const currentElementData = computed(() =>
      Resolve.data(jsonforms.core?.data, currentElementPath.value)
    );

    /**
     * Checks if there's an existing contributor with the role of 'first-author'.
     */
    const hasFirstAuthor = computed(() => {
      return contributorsData.value?.some((contributor) => contributor.role === ContributorRole.FirstAuthor);
    });

    /**
     * Computed property to check if there are multiple contributors.
     */
    const hasMultipleContributors = computed(() => {
      return contributorsData.value.length > 1;
    });

    const extractLastIndex = (path) => {
      const pattern = /\d+/g; // Global match for all occurrences of one or more digits
      const allMatches = path.match(pattern);
      return allMatches && allMatches.length > 0 ? allMatches[allMatches.length - 1] : null;
    };

    /**
     * Handles the change event for the contributor role selection.
     * This method is triggered when the role selection is changed in the UI.
     *
     * @param event - The selected role value.
     *
     * If the selected role is 'first-author' and there's already a 'first-author',
     * a confirmation dialog is displayed to confirm replacing the existing 'first-author'.
     *
     * If the user confirms the change:
     * - All instances of 'first-author' in the contributors are changed to 'additional-author'.
     * - The new role is applied to the current contributor.
     *
     * If the user cancels the dialog or an error occurs during confirmation:
     * - The role change is reverted to its previous state.
     * - An error message is displayed if applicable.
     *
     * If the selected role is not 'first-author', the change is applied directly without confirmation.
     */
    const localOnChange = async (event: string) => {
      // Determine the new role and sequence immediately
      const newRole = event;
      const newSequence =
        newRole === ContributorRole.FirstAuthor
          ? ContributorSequence.First
          : ContributorSequence.Additional;

      // Backup current state in case we need to revert
      const originalContributors = JSON.parse(JSON.stringify(contributorsData.value));

      // Prepare the updated contributors array
      let updatedContributors = [...contributorsData.value];

      // Check if the new role is 'first-author' and if confirmation is needed
      if (newRole === ContributorRole.FirstAuthor && hasFirstAuthor.value) {
        try {
          await openDialog({
            type: 'confirm',
            title: t.value(
              'records_edit_contributors_firstAuthorship_confirmChange_title'
            ),
            message: t.value(
              'records_edit_contributors_firstAuthorship_confirmChange_msg'
            ),
          });

          // Upon confirmation, update all contributors, setting the existing 'first-author' to 'additional-author'
          // and adjusting sequences accordingly
          updatedContributors = updatedContributors.map((contributor) => {
            if (contributor.role === ContributorRole.FirstAuthor) {
              return {
                ...contributor,
                role: ContributorRole.AdditionalAuthor,
                sequence: ContributorSequence.Additional,
              };
            }
            return contributor;
          });
        } catch (_e) {
          // On cancellation or error, revert to original state and abort further processing
          const errorMessage = await extractErrorMessageAsync(_e, false);
          if (errorMessage) alertError(errorMessage);
          control.handleChange(contributorsPath.value, originalContributors);
          return;
        }
      }

      // Update the role and sequence for the current contributor
      const currentIndex = extractLastIndex(control.control.value.path);
      updatedContributors[currentIndex] = {
        ...updatedContributors[currentIndex],
        role: newRole,
        sequence: newSequence,
      };

      // Apply the updated contributors array in one operation
      control.handleChange(contributorsPath.value, updatedContributors);
    };

    /**
     * Lifecycle hook for Vue component.
     * Invoked when the component is mounted.
     * This hook handles the initial setting of the contributor role based on existing data.
     * If the current data value is undefined, it sets the role to either 'additional-author' or 'first-author'
     * based on the conditions:
     * 1. If there is a first author and multiple contributors, it sets the role to 'additional-author'.
     * 2. Otherwise, it sets the role to 'first-author'.
     **/
    onMounted(() => {
      if (control.control.value.data === undefined) {
        const newRole =
          hasFirstAuthor.value && hasMultipleContributors.value
            ? ContributorRole.AdditionalAuthor
            : ContributorRole.FirstAuthor;
        const newSequence =
          newRole === ContributorRole.FirstAuthor
            ? ContributorSequence.First
            : ContributorSequence.Additional;

        // Remove the last segment of the path to get the base path for the current element
        const basePathSegments = control.control.value.path.split('.');
        basePathSegments.pop(); // Remove the last segment
        const basePath = basePathSegments.join('.');

        // Apply the batch update to the current contributor element
        const update = {
          role: newRole,
          sequence: newSequence,
        };
        control.handleChange(basePath, update);
      }
    });

    return { ...control, t, localOnChange, currentElementData, contributorsPath, infoText };
  },
});

export default controlRenderer;
</script>
