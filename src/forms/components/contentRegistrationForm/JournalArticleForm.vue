<template>
  <manage-records-submission-progress-dialog
    :submission-state="submissionState"
    />
  <div v-if="showDebugFormErrors">
    <DebugFormErrors :title="'Article Errors'" :errors="articleErrors" />
    <DebugFormErrors :title="'Journal Errors'" :errors="journalErrors" />
    <DebugFormErrors :title="'Issue Errors'" :errors="issueErrors" />
  </div>
  <v-stepper alt-labels :model-value="currentStep" elevation="0">
    <v-stepper-header>
      <template v-for="(step, index) in activeSteps" :key="index">
        <v-stepper-item
          :value="index + 1"
          :complete="step.complete"
          :title="step.title"
        >
          <template #title>{{ step.title }}</template>
        </v-stepper-item>
        <!-- Render a divider if it's not the last item -->
        <v-divider
          v-if="index < activeSteps.length - 1"
          :key="'divider-' + index"
        ></v-divider>
      </template>
    </v-stepper-header>
    <v-stepper-window>
      <template v-for="(step, index) in activeSteps" :key="index">
        <v-stepper-window-item
          :value="index + 1"
          :complete="step.complete"
          :title="step.title"
          eager
        >
          <component
            :is="step.component"
            class="pt-4"
            v-bind="step.props"
            :hasIssueMetadata="hasIssueMetadata"
            :showIssueStep="showIssueStep"
            :readonly="submitting"
            @update:errors="handleErrors"
            @change="(event) => onChange(event, index + 1)"
            @add-issue-metadata="addIssueMetadata"
          ></component>
        </v-stepper-window-item>
      </template>
    </v-stepper-window>
    <v-stepper-actions
      data-test-id="journal-article-form-next-button"
      :data-test-can-proceed="canProceed"
      :disabled="submitting"
      :color="canProceed ? 'success' : 'error'"
      @click:next="nextStep"
      @click:prev="prevStep"
      :next-text="getNextButtonText"
    >
    <template v-slot:next>
      <v-btn @click="nextStep" :loading="submitting"></v-btn>
    </template></v-stepper-actions>

  </v-stepper>
</template>

<script lang="ts" setup>
/**
 * A Vue component implementing a multi-step form for journal article registration.
 * Utilizes Vuetify's v-stepper for step-wise navigation, integrating JSON Forms for dynamic form rendering based on
 * JSON Schema, which is used for form rendering and data validation.
 */
import {computed, ComputedRef, inject, Ref, ref, unref, watch} from 'vue';
import {useTranslationNamespace} from '@/composable/useI18n';
import {env} from '@/env';
import CustomJsonForms from '@/forms/components/common/CustomJsonForms.vue';
import journalUiSchema from '@/forms/schemas/ui/journalArticleForm/journal.uischema.json';
import issueUiSchema from '@/forms/schemas/ui/journalArticleForm/issue.uischema.json';
import articleUiSchema from '@/forms/schemas/ui/journalArticleForm/article.uischema.json';
import {JsonFormsUISchemaRegistryEntry, JsonSchema, UISchemaElement,} from '@jsonforms/core';
import {ErrorObject} from 'ajv';
import {FormData} from '@/forms/types';
import DebugFormErrors from '@/forms/components/common/DebugFormErrors.vue';
import PageNotFound from '@/views/PageNotFound.vue';
import {enableFilterErrorsBeforeTouchKey} from '@/injection-keys';
import {
  ContributorRoleRendererEntry,
  IssnRendererEntry,
  LanguageChooserRendererEntry,
  PublicationDatesRendererEntry,
  OrcidRendererEntry,
  DoiRendererEntry, baseHorizontalLabelRenderers,
  ReferencesRendererEntry
} from "@/forms/renderers";
import {matchSchemaPath} from "@/forms/utils/schemaHelpers";
import ISSNUISchema from '@/forms/schemas/ui/common/ISSNUISchema.json';
import ContributorUISchema from '@/forms/schemas/ui/common/ContributorUISchema.json';
import {nestedHasValue} from "@/utils/helpers";
import get from "lodash/get";
import JournalStep from "@/forms/components/contentRegistrationForm/journalArticle/JournalStep.vue";
import ReviewStep from "@/forms/components/contentRegistrationForm/journalArticle/SubmissionReviewStep.vue";
import {I18nNamespace} from "@/i18n";
import generateSubmissionReviewUischema from "@/forms/schemas/ui/journalArticleForm/submissionReview.uischema";
import set from "lodash/set";
import {useScrollToFirstError} from "@/composable/useScroll";
import {TrimOption} from "@/forms/utils/format";
import ManageRecordsSubmissionProgressDialog from "@/components/ManageRecordsSubmissionProgressDialog.vue";

const FINAL_STEP = 3;

const emits = defineEmits(['change', 'submit']);

const headerRef = inject('headerRef');
const { scrollToFirstError } = useScrollToFirstError(headerRef);

const enableFilterErrorsBeforeTouch = ref(true);

// Configuration object for forms, incorporating the above injectable setting.
const formConfig = {
  enableFilterErrorsBeforeTouch,
  debounceWait: env().formDebounceWait,
  trim: TrimOption.Both,
  hideAvatar: true,
};

/**
 * Defines the structure for each step in the multi-step form.
 * @property {string} title - The title of the step.
 * @property {boolean} complete - Indicates whether the step is completed.
 * @property {any} component - The Vue component associated with the step.
 * @property {any} [props] - Optional props passed to the component.
 */
type step = {
  title: string;
  complete: boolean;
  component?: any;
  props?: any;
  active: boolean;
  nextText?: any;
  hideErrorsBeforeTouch?: boolean;
  hasErrors?: any;
};
const props = defineProps<{
  /** JSON schema for form validation and structure. */
  schema: JsonSchema;
  /** Optional UI schema for custom form UI configuration. */
  uiSchema?: UISchemaElement;
  /** Form data to be passed to and manipulated by the form. */
  formData: FormData;
  /** Optional array of custom renderer components. */
  renderers?: [];
  /** Semaphore indicating if a submission is in progress */
  submitting: boolean;
  /** If submitting, the state of the submission process */
  submissionState?: string;
  formConfig?: {};
}>();


/**
 * Represents the current active step, defaults to 1.
 */
const currentStep = ref(1);

/**
 * Controls whether the user can move forward in the stepper. Default false.
 */
const allowProceed = ref(false);

const t = useTranslationNamespace(I18nNamespace.ContentRegistration);

const localisedButtonTextNext = computed(() => {
  return t('action_next')
})

const localisedButtonTextSubmit = computed(() => {
  return t('action_submit')
})

/**
 * Holds form validation errors.
 */
const errors = ref([]);
function createErrorFilter(
  errors: Ref<ErrorObject[]>,
  path: string
): ComputedRef<ErrorObject[]> {
  return computed(() =>
    errors.value.filter((error) => error.instancePath.startsWith(path))
  );
}

/** Computes and filters journal-specific errors from the general errors list. */
const journalErrors = createErrorFilter(errors, '/journal');
/**
 * Filters errors based on the instance path related to issue data.
 */
const issueErrors = createErrorFilter(errors, '/issue');
/**
 * Filters errors based on the instance path related to article data.
 */
const articleErrors = createErrorFilter(errors, '/article');

function createHasErrorsFunction(
  errorArray: ComputedRef<ErrorObject[]>
): ComputedRef<boolean> {
  return computed(() => errorArray.value.length > 0);
}

const hasJournalErrors = createHasErrorsFunction(journalErrors);
const hasIssueErrors = createHasErrorsFunction(issueErrors);
const hasArticleErrors = createHasErrorsFunction(articleErrors);

const isFormValid = computed(() => {
  console.log(errors.value.length)
  return errors.value.length === 0;
})

const showIssueStep = ref(false);
const hasIssueMetadata = computed(() => {
  return nestedHasValue(get(props.formData, 'issue'));
});

const issueStepActive = computed(() => {
  return hasIssueMetadata.value || showIssueStep.value;
});

/**
 * Triggered by user click
 * Set Issue step display override
 * Move to the next step (issue step)
 */
const addIssueMetadata = () => {
  showIssueStep.value = true;
  nextStep();
};

const uiSchemas: JsonFormsUISchemaRegistryEntry[] = [
  {
    tester: matchSchemaPath('#/properties/journal/properties/issn'),
    uischema: ISSNUISchema,
  },
  {
    tester: matchSchemaPath('#/properties/article/properties/contributors'),
    uischema: ContributorUISchema,
  },
];

const customRenderers = Object.freeze([
  ...baseHorizontalLabelRenderers,
  PublicationDatesRendererEntry,
  IssnRendererEntry,
  ContributorRoleRendererEntry,
  LanguageChooserRendererEntry,
  OrcidRendererEntry,
  DoiRendererEntry,
  ReferencesRendererEntry,
]);

const customReviewRenderers = [
  PublicationDatesRendererEntry,
];

const activeFormConfig = computed(() => {
  return {
    ...formConfig, enableFilterErrorsBeforeTouch: currentActiveStep.value?.hideErrorsBeforeTouch || false, ...props.formConfig
  }
})

const steps: Ref<step[]> = ref([
  {
    title: t('records_register-journal-article_article_title'),
    complete: !hasArticleErrors.value,
    component: Object.freeze(CustomJsonForms),
    props: {
      jsonSchema: computed(() => props.schema),
      uiSchema: articleUiSchema,
      formData: computed(() => props.formData),
      uiSchemas: uiSchemas,
      renderers: customRenderers,
      config: activeFormConfig,
    },
    active: true,
    hideErrorsBeforeTouch: ref(true),
    hasErrors: hasArticleErrors,
  },
  {
    title: t('records_register-journal-article_journal_title'),
    complete: !hasJournalErrors.value,
    component: Object.freeze(JournalStep),
    props: {
      jsonSchema: computed(() => props.schema),
      uiSchema: journalUiSchema,
      formData: computed(() => props.formData),
      uiSchemas: uiSchemas,
      renderers: customRenderers,
      config: activeFormConfig,
    },
    nextText:  localisedButtonTextNext,
    active: true,
    hideErrorsBeforeTouch: ref(true),
    hasErrors: hasJournalErrors
  },
  {
    title: t('records_register-journal-article_issue_title'),
    complete: !hasIssueErrors.value,
    component: Object.freeze(CustomJsonForms),
    props: {
      jsonSchema: computed(() => props.schema),
      uiSchema: issueUiSchema,
      formData: computed(() => props.formData),
      uiSchemas: uiSchemas,
      renderers: customRenderers,
      config: activeFormConfig,
    },
    nextText: localisedButtonTextNext,
    active: issueStepActive,
    hideErrorsBeforeTouch: ref(true),
    hasErrors: hasIssueErrors,
  },
  {
    title: 'Review',
    complete: isFormValid,
    component: Object.freeze(ReviewStep),
    props: {
      jsonSchema: computed(() => props.schema),
      uiSchema: computed(() => generateSubmissionReviewUischema(props.formData)),
      formData: computed(() => props.formData),
      uiSchemas: uiSchemas,
      renderers: customReviewRenderers,
      config: activeFormConfig,
      nextText: localisedButtonTextSubmit,
    },
    active: true
  },
  // Submission step will not be reached until XML rendering code is added, stubbing out.
  {
    title: t('records_register-journal-article_submit_title'),
    complete: false,
    component: Object.freeze(PageNotFound)  ,
    active: true
  },
]);

const activeSteps = computed(() => steps.value.filter(step => step.active));

const currentStepIndex = computed(() => currentStep.value - 1);  // Convert to zero-based index

const currentActiveStep = computed(() => {
  return activeSteps.value[currentStepIndex.value];
});

const getNextButtonText = computed(() => {
  return unref(currentActiveStep.value.props.nextText) || undefined;
});

watch(hasJournalErrors, (newVal) => {
  steps.value[1].complete = !newVal;
  // This line is necessary to trigger reactivity for array object properties
  steps.value = [...steps.value];
});

watch(hasArticleErrors, (newVal) => {
  steps.value[0].complete = !newVal;
  // This line is necessary to trigger reactivity for array object properties
  steps.value = [...steps.value];
});

watch(hasIssueErrors, (newVal) => {
  steps.value[2].complete = !newVal;
  // This line is necessary to trigger reactivity for array object properties
  steps.value = [...steps.value];
});

/**
 * Determines if the user can navigate to the next step based on the current step's completion status.
 */
const canProceed = computed(() => {
  const currentStepIndex = currentStep.value - 1; // Convert to zero-based index
  return unref(steps.value[currentStepIndex].complete);
});

/**
 * Updates the current step to a new value, used in navigating between steps.
 * @param newStep - The step number to navigate to.
 */
function updateStep(newStep) {
  currentStep.value = newStep;
}

/** Advances to the next form step if allowed. */
const nextStep = () => {
  set(currentActiveStep, 'value.hideErrorsBeforeTouch', false)
  if (currentActiveStep.value?.hasErrors) {
    scrollToFirstError()
  }
  if (
    allowProceed.value ||
    (canProceed.value && Number(currentStep.value) < steps.value.length)
  ) {
    if (currentStep.value === activeSteps.value.length -1) {
      emits('submit');
      return;
    }
    updateStep(Number(currentStep.value) + 1);
  }
};

/**
 * Moves back to the previous form step, ensuring the current step is greater than 1.
 */
const prevStep = () => {
  if (Number(currentStep.value) > 1) {
    updateStep(Number(currentStep.value) - 1);
  }
};

/**
 * Handles changes to form data, emitting an update event.
 * @param event - The change event with updated form data and errors.
 */
const onChange = (event, index: number): void => {

  // Guard to prevent updates bubbling from non-active steps
  if (index === currentStep.value) {
    emits('change', event);
  }
};

const handleErrors = (newErrors: any) => {
  errors.value = newErrors;
};

/**
 * Updates the array of form errors based on emitted errors from child form components.
 * @param newErrors - Array of new error objects to set.
 */

/**
 * Determines if debug form errors should be shown, based on app mode.
 */
const showDebugFormErrors = computed(() => {
  return env().isDevMode && env().isI18NDebugLogging;
});
</script>

<style>
.array-list-item-content .v-expansion-panel-text__wrapper {
  padding: 8px 16px 16px;
}

div.control div h4 {
  display: none;
}
</style>
