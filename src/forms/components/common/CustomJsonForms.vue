<template>
  <div>
    <json-forms
      class="custom-json-forms"
      :ajv="ajv"
      :schema="jsonSchema"
      :data="formData"
      :renderers="mergedRenderers"
      :cells="cells"
      :config="config"
      :i18n="i18n"
      :uischema="uiSchema"
      :uischemas="uiSchemas"
      :readonly="readonly"
      @change="onChange"
    />
  </div>
</template>

<script setup lang="ts">
/**
 * A Vue component that serves as a wrapper for JSON Forms, facilitating the rendering of dynamic forms
 * based on JSON Schema and UI Schema.
 *
 * Example usage:
 * <template>
 *   <YourFormWrapperComponent
 *     :json-schema="yourJsonSchema"
 *     :ui-schema="yourUiSchema"
 *     :form-data="yourFormData"
 *     @change="handleFormChange"
 *   ></YourFormWrapperComponent>
 * </template>
 *
 */

import {
  computed,
  provide,
  ref,
  unref,
  withDefaults,
} from 'vue';
import { JsonForms } from '@jsonforms/vue';
import {
  createAjv,
  JsonFormsI18nState, JsonFormsUISchemaRegistryEntry,
  JsonSchema,
  UISchemaElement,
} from '@jsonforms/core';
import {
  arrayRendererEntry,
  labeledObjectRendererEntry,
  rorRendererEntry,
  spacerRendererEntry,
  baseHorizontalLabelRenderers,
} from '@/forms/renderers';
import useFormTheme from '@/composable/useFormTheme';
import {
  defaultStyles,
  extendedVuetifyRenderers,
  mergeStyles,
} from '@pvale/vue-vuetify';
import {
  createJsonFormsErrorTranslator,
  createJsonFormsTranslator,
  I18nNamespace,
} from '@/i18n';
import { useLocale } from '@/composable/useI18n';
import {JsonFormsI18nStateSymbol} from "@/constants/injectionKeys";
import uniqueItemField from '@/forms/validators/AjvKeywordUniqueValue'

/**
 * Props for the JSON Forms component.
 */
export interface Props {
  /** The JSON Schema definition for the form data. */
  jsonSchema: JsonSchema;
  /** The UI schema for customizing form layout and UI components. */
  uiSchema?: UISchemaElement;
  /** Additional UI schemas for complex form structures. */
  uiSchemas?: JsonFormsUISchemaRegistryEntry[];
  /** Initial data to populate the form. */
  formData?: Record<string, any>;
  /** Configuration options for JSON Forms. */
  config?: Record<string, any>;
  /** Custom renderer components for rendering specific parts of the form. */
  renderers?: (typeof JsonForms)['renders'];
  /** Custom cell renderer components for rendering specific cells of tables within the form. */
  cells?: (typeof JsonForms)['cells'];
  /** Namespace for i18n translation strings. */
  i18nNamespace?: I18nNamespace;
  /** Set the entire form as read-only */
  readonly?: boolean;
}

// Default props values.
const props = withDefaults(defineProps<Props>(), {
  formData: () => ({}),
  config: () => ({}),
  renderers: () => baseHorizontalLabelRenderers,
  i18nNamespace: I18nNamespace.ContentRegistration,
  uiSchemas: () => [],
  uiSchema: undefined,
  readonly: () => false,
});

// AJV instance for validation.
const ajv = createAjv({ useDefaults: true });
ajv.addKeyword(uniqueItemField())

// Merged renderers array combining custom and default renderers.
const mergedRenderers = Object.freeze([
  ...extendedVuetifyRenderers,
  arrayRendererEntry,
  labeledObjectRendererEntry,
  rorRendererEntry,
  spacerRendererEntry,
  ...props.renderers,
]);

/**
 * Component emits for various purposes:
 *
 * - `change`: Emits both form data and validation errors when the form state changes.
 *
 * - `update:formData`: Emits only the form data when the form state changes.
 *
 * - `update:errors`: Emits only the validation errors when the form state changes.
 */
const emit = defineEmits<{
  (e: 'change', value: any): void;
  (e: 'update:formData', value: any): void;
  (e: 'update:errors', value: any): void;
}>();

// Setup i18n.
const { locale } = useLocale();
const i18n = computed<JsonFormsI18nState>(() => ({
  locale: unref(locale),
  translate: createJsonFormsTranslator(props.i18nNamespace),
  translateError: createJsonFormsErrorTranslator(unref(locale)),
}));

// Error handling.
const errors = ref([]);
const setErrors = (newErrors) => {
  errors.value = newErrors;
};
const onChange = ({ errors, data }: { errors: any[]; data: any }) => {
  setErrors(errors);
  emit('update:errors', errors);
  emit('update:formData', data);
  emit('change', { errors, data });
};

// Theming and styles.
const { currentFormTheme, formThemes } = useFormTheme();
const formThemeStyles = computed(
  () => formThemes.value[currentFormTheme.value].styles
);
const formStyles = computed(() =>
  mergeStyles(defaultStyles, formThemeStyles.value)
);
provide('styles', formStyles);
provide(JsonFormsI18nStateSymbol, i18n);

// Form configuration. Takes global form theme and applies overrides from props.
const config = computed(() => ({
  ...formThemes.value[currentFormTheme.value].config,
  ...props.config,
}));
</script>
