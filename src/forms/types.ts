import { JsonSchema } from '@jsonforms/core';

/**
 * Represents the data model for a form, structured as a key-value map. The form data is subject
 * to validation against a JSON schema, which dictates the permissible structure and types of data.
 *
 * @typeParam
 * Record<string, any> - Keys are form field names, values are data of any type as defined by the JSON schema.
 */
export type FormData = Record<string, any>;

/**
 * Defines a function type for testing whether a schema transformation should be applied.
 * @param schema - The current JSON Schema of the form.
 * @param newData - The newly updated form data.
 * @param prevData - The previous form data before the latest update.
 * @param context - An optional context object that may contain additional information affecting the transformation logic.
 * @returns A boolean indicating whether the schema should be transformed based on the provided data and context.
 */
export type SchemaTransformationTester = (
  schema: JsonSchema,
  newData: FormData,
  prevData?: FormData,
  context?: any
) => boolean;

/**
 * Defines a function type for transforming a JSON Schema based on the current form data.
 * @param schema - The JSON Schema to be transformed.
 * @param data - The current form data based on which the schema transformation should be carried out.
 * @returns The transformed JSON Schema.
 */
export type SchemaTransformer = (
  schema: JsonSchema,
  data: FormData
) => JsonSchema;

/**
 * Represents a schema transformation operation, encapsulating both the conditions under which the transformation
 * should be applied and the transformation logic itself.
 */
export type SchemaTransformationPair = {
  /**
   * A tester function to determine whether the transformation logic should be applied.
   */
  tester: SchemaTransformationTester;

  /**
   * A transformer function that carries out the schema transformation.
   */
  transform: SchemaTransformer;
};
