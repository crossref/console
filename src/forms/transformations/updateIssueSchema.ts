import cloneDeep from 'lodash/cloneDeep';
import set from 'lodash/set';
import { FormData } from '@/forms/types';
import { JsonSchema } from '@jsonforms/core';
import {
  SchemaTransformationPair,
  SchemaTransformationTester,
  SchemaTransformer,
} from '@/forms/types';

/**
 * Determines if the issue data within the form has changed, necessitating a schema transformation.
 * Always returns `true` as per current logic.
 *
 * @param schema - The current JSON Schema of the form (unused, but required by the type signature).
 * @param newData - The new form data.
 * @param prevData - The previous form data (defaults to an empty object).
 * @param context - Optional context (unused, but required by the type signature).
 * @returns `true` to indicate a transformation should always be applied.
 */
export const hasIssueChanged: SchemaTransformationTester = (
  schema: JsonSchema,
  newData: FormData,
  prevData: FormData = {},
  context?: any
): boolean => {
  return true; // Transformation always applies for now
};

/**
 * Applies transformations to the form's JSON Schema based on publication dates.
 *
 * @param schema - The current JSON Schema of the form.
 * @param data - The current form data.
 * @returns The transformed JSON Schema with updated `required` fields for issue publication dates.
 */
export const transformPublicationDates: SchemaTransformer = (
  schema: JsonSchema,
  data: FormData
): JsonSchema => {
  // Create a deep copy of the schema to ensure immutability
  const schemaCopy = cloneDeep(schema);
  const requiredFields: string[] = [];

  // Check if volume or issue information exists
  const hasIssueInfo =
    data?.issue?.volume?.length > 0 || data?.issue?.issue?.length > 0;

  // Add publication dates to required fields if no dates are provided
  if (
    hasIssueInfo &&
    !data?.issue?.publishedOnline?.length &&
    !data?.issue?.publishedInPrint?.length
  ) {
    requiredFields.push('publishedOnline', 'publishedInPrint');
  }

  // Safely set the 'required' fields in the schema copy
  set(schemaCopy, 'properties.issue.required', requiredFields);

  return schemaCopy;
};

/**
 * Schema transformation entry, linking the tester and transformer logic.
 */
export const entry: SchemaTransformationPair = {
  tester: hasIssueChanged,
  transform: transformPublicationDates,
};

export default entry;
