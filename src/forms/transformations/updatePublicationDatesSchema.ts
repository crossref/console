import cloneDeep from 'lodash/cloneDeep';
import isEqual from 'lodash/isEqual';
import set from 'lodash/set';
import { FormData } from '@/forms/types';
import { JsonSchema } from '@jsonforms/core';
import {
  containsPrimitiveValue,
  hasAtLeastOnePropertyWithValue,
  hasPropertyWithValue,
} from '@/utils/helpers';
import {
  SchemaTransformationPair,
  SchemaTransformationTester,
  SchemaTransformer,
} from '@/forms/types';

/**
 * Determines if the publication dates within the form data have changed, necessitating a schema transformation.
 * This matches the `SchemaTransformationTester` signature.
 *
 * @param schema - The current JSON Schema of the form. Unused here but required by the type signature.
 * @param newData - The new form data.
 * @param prevData - The previous form data.
 * @param context - Optional context. Unused here but required by the type signature.
 * @returns A boolean indicating whether a transformation should be applied.
 */
export const hasPublicationDatesChanged: SchemaTransformationTester = (
  schema: JsonSchema,
  newData: FormData,
  prevData: FormData = {},
  context?: any
): boolean => {
  // Return true if publication dates do not exist, or have changed between the previous and new form data.
  const shouldTransform = (
    !containsPrimitiveValue(newData?.article?.publicationDates) ||
    !isEqual(
    newData?.article?.publicationDates,
    prevData?.article?.publicationDates
  ));

  return shouldTransform;
};

/**
 * Defines the transformation logic to be applied to the form's JSON Schema, based on publication dates.
 * This matches the `SchemaTransformer` signature.
 *
 * @param schema - The current JSON Schema of the form.
 * @param data - The current form data.
 * @returns The transformed JSON Schema.
 */
export const transformPublicationDates: SchemaTransformer = (
  schema: JsonSchema,
  data: FormData
): JsonSchema => {
  const schemaCopy = cloneDeep(schema);
  const required: string[] = [];
  if (!hasAtLeastOnePropertyWithValue(data.article.publicationDates)) {
    if (
      !hasPropertyWithValue(data.article.publicationDates, 'publishedOnline')
    ) {
      required.push('publishedOnline');
    }
    if (
      !hasPropertyWithValue(data.article.publicationDates, 'publishedInPrint')
    ) {
      required.push('publishedInPrint');
    }
  }

  // Safely set the 'required' property
  set(
    schemaCopy,
    'properties.article.properties.publicationDates.required',
    required
  );

  return schemaCopy;
};

/**
 * An entry for the schema transformation logic, specifying when and how the form schema should be transformed.
 */
export const entry: SchemaTransformationPair = {
  tester: hasPublicationDatesChanged,
  transform: transformPublicationDates,
};

export default entry;
