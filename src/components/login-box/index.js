import LoginBox from './LoginBox';
import BrandedCard from './BrandedCard';
import LoginBoxCard from './LoginBoxCard';
import LoginBoxCardLogo from './LoginBoxCardLogo';
import LoginBoxCardTitle from './LoginBoxCardTitle';
import LoginBoxStepAuthenticate from './LoginBoxStepAuthenticate';
import LoginBoxStepSelectRole from './LoginBoxStepSelectRole';
import LoginBoxInputSelectRole from './LoginBoxInputSelectRole';
import LoginBoxFormUserCredentials from './LoginBoxFormUserCredentials';
import LoginBoxActionButtonLogin from './LoginBoxActionButtonLogin';
import LoginBoxActionButtonBack from './LoginBoxActionButtonBack';
import LoginBoxStepNoRolesAssigned from './LoginBoxStepNoRolesAssigned';
import AlertSuccess from './AlertSuccess';
import ForgotYourPasswordLink from './ForgotYourPasswordLink';

export default {
  LoginBox,
  BrandedCard,
  LoginBoxCard,
  LoginBoxCardLogo,
  LoginBoxCardTitle,
  LoginBoxStepAuthenticate,
  LoginBoxInputSelectRole,
  LoginBoxFormUserCredentials,
  LoginBoxActionButtonLogin,
  LoginBoxActionButtonBack,
  LoginBoxStepNoRolesAssigned,
  AlertSuccess,
  ForgotYourPasswordLink,
  LoginBoxStepSelectRole,
};
