// AlertSuccess
import ForgotYourPasswordLink from '@/components/login-box/ForgotYourPasswordLink';

// Utilities
import { mount } from '@vue/test-utils';
import { describe, it, expect } from 'vitest';
import vuetifyInstance from '@/plugins/vuetify';

const mockedRootParent = {
  data() {
    return {
      config: {
        authApiBaseUrl: 'doi.crossref.org',
        reset_password_url:
          'https://authenticator.crossref.org/reset-password/',
      },
    };
  },
};

describe('ForgotYourPasswordLink', () => {
  function mountFunction(options = {}) {
    const wrapper = mount(ForgotYourPasswordLink, {
      global: {
        plugins: [vuetifyInstance],
      },
      ...options,
    });
    return wrapper;
  }

  it('should work', () => {
    const wrapper = mountFunction({
      propsData: {},
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it('has url from config', () => {
    const wrapper = mountFunction({
      propsData: {},
    });
    const anchor = wrapper.find('a');
    expect(anchor.attributes('href')).toBe(
      mockedRootParent.data().config.reset_password_url
    );
  });
});
