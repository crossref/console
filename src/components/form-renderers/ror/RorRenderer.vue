<!--
Custom renderer for ROR Ids. Handles both the institution name and ROR id at the same time.
-->
<template>
  <control-wrapper
    v-bind="controlWrapper"
    :styles="styles"
    :is-focused="isFocused"
    :applied-options="appliedOptions"
  >
      <horizontal-label-wrapper :applied-options="appliedOptions">
        <template #label>{{ computedLabel }}</template>
        <info-text :msg="infoText.msg" :learn-more-link="infoText.link"/>
        <v-autocomplete
          :id="control.id + '-input'"
          :items="items"
          :loading="loading"
          :label="control.label"
          :disabled="!control.enabled"
          :error-messages="control.errors"
          :autofocus="appliedOptions.focus"
          :placeholder="
          appliedOptions.placeholder ?? t('ror_start_typing_to_search')
        "
          :hint="control.description"
          :persistent-hint="persistentHint()"
          :model-value="selectedValue"
          :value-comparator="valueComparator"
          v-bind="vuetifyProps('v-autocomplete')"
          no-filter
          clearable
          hide-no-data
          hide-selected
          prepend-inner-icon="mdi-database-search"
          @update:search="onSearch"
          @focus="isFocused = true"
          @blur="isFocused = false"
          @update:model-value="onSelectValue"
        >
          <template #prepend v-if="false">
            <v-icon title="Research Organization Registry">
              mdi-database-search
            </v-icon>
          </template>
          <template #item="{ props, item }">
            <v-list-item :disabled="item.raw.type === 'subheader'" v-bind="props">
              <template #title>
                <div class="list-item-text">{{ item.raw.title }}</div>
              </template>
              <template #default>
                <div class="list-item">
                  <template v-if="item.raw.value?.type === 'organization'">
                    <div
                      v-if="item.raw.value.org.labels.length > 0"
                      class="list-item-organization-labels"
                    >
                      {{
                        item.raw.value.org.labels
                          .map((label) => label.label)
                          .join(', ')
                      }}
                    </div>
                    <div class="list-item-organization-country">
                      {{
                        getRegionName(item.raw.value.org.country.country_code) ||
                        item.raw.value.org.country.country_name
                      }}
                    </div>
                    <div
                      v-if="item.raw.value.org.acronyms.length > 0"
                      class="list-item-organization-acronyms"
                    >
                      {{ item.raw.value.org.acronyms.join(', ') }}
                    </div>
                  </template>
                </div>
              </template>
            </v-list-item>
            <v-divider v-if="item.raw.type === 'subheader'"></v-divider>
          </template>
        </v-autocomplete>
        <ror-id :ror-id="rorData ? rorData : !!selectedValue" />
      </horizontal-label-wrapper>
  </control-wrapper>
</template>

<script lang="ts">
import {
  Organization,
  OrganizationQueryResponse,
  queryOrganizations,
} from '@/api/ror';
import { getRegionName, memoizeValues } from '@/i18n';
import {
  ControlElement,
  JsonFormsRendererRegistryEntry,
  JsonFormsSubStates,
  rankWith,
  Resolve,
  scopeEndsWith,
} from '@jsonforms/core';
import { rendererProps, useJsonFormsControl } from '@jsonforms/vue';
import { ControlWrapper, useVuetifyControl } from '@pvale/vue-vuetify';
import debounce from 'lodash/debounce';
import { computed, defineComponent, inject, ref } from 'vue';
import { VAutocomplete, VIcon } from 'vuetify/components';
import RorId from './RorId.vue';
import InfoText from "@/forms/components/common/InfoText.vue";
import {JsonFormsI18nStateSymbol} from "@/constants/injectionKeys";
import {useInfoText} from "@/forms/compositions";
import HorizontalLabelWrapper from "@/forms/components/renderers/HorizontalLabelWrapper.vue";

interface FreeTextValue {
  type: 'freetext';
  freetext: string;
}
const freeTextValue = (text: string): FreeTextValue => ({
  type: 'freetext',
  freetext: text,
});
interface OrganizationValue {
  type: 'organization';
  org: Organization;
}
const organizationValue = (org: Organization): OrganizationValue => ({
  type: 'organization',
  org,
});
type SelectableValue = FreeTextValue | OrganizationValue;
const getText = (value: SelectableValue): string =>
  value.type === 'freetext' ? value.freetext : value.org.name;

interface SelectableItem {
  title: string;
  value: SelectableValue;
}
interface HeaderItem {
  type: 'subheader';
  title: string;
}
type ListItem = SelectableItem | HeaderItem;

const createItemsForSearchResult = (
  response: OrganizationQueryResponse | null,
  searchTerm: string | null,
  t: (key: string, values?: any) => string | undefined
): ListItem[] => {
  const result: ListItem[] = [];
  if (response) {
    const headerMessage = response.numberOfResults
      ? t(
          'ror_matched_institutions',
          memoizeValues({
            count: response.numberOfResults,
          })
        )
      : t('ror_no_matched_institutions');
    if (headerMessage) {
      result.push({
        type: 'subheader',
        title: headerMessage,
      });
    }
    response.organizations.forEach((organization) => {
      result.push({
        title: organization.name,
        value: organizationValue(organization),
      });
    });
  }
  if (searchTerm) {
    // This item will be hidden in the UI but is needed by autocomplete as
    // otherwise it will not recognize the current value in case it is just
    // a text
    result.push({
      title: searchTerm,
      value: freeTextValue(searchTerm),
    });
  }
  return result;
};

const createItemsForValue = (
  value: SelectableValue | null
): SelectableItem[] => {
  if (!value) {
    return [];
  }
  return [{ title: getText(value), value }];
};

/**
 * Determines whether two values are equal. Used by autocomplete to check
 * which element in "items" is equal to "value".
 * The default implementation of autocomplete fails in our use cases.
 */
const valueComparator = (
  valueA: SelectableValue | null,
  valueB: SelectableValue | null
) => {
  if (valueA === null && valueB === null) {
    return true;
  }
  if (valueA === null || valueB === null) {
    return false;
  }
  if (valueA.type !== valueB.type) {
    return false;
  }
  if (valueA.type === 'organization') {
    return valueA.org.id === (valueB as OrganizationValue).org.id;
  }
  if (valueA.type === 'freetext') {
    return valueA.freetext === (valueB as FreeTextValue).freetext;
  }
  // should not happen
  return false;
};

const rorRenderer = defineComponent({
  name: 'RorRenderer',
  components: {
    HorizontalLabelWrapper,
    InfoText,
    VAutocomplete,
    VIcon,
    ControlWrapper,
    RorId,
  },
  props: {
    ...rendererProps<ControlElement>(),
  },
  setup(props) {
    const jsonforms = inject<JsonFormsSubStates>('jsonforms');
    if (!jsonforms) {
      throw "'jsonforms' couldn't be injected. Are you within JSON Forms?";
    }

    const i18n = inject(JsonFormsI18nStateSymbol, null);

    const t = (key: string, values?: any) =>
      // at this point of time it's guaranteed to be set
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      jsonforms.i18n!.translate!(key, undefined, values);

    const jsonFormsVuetifyControl = useVuetifyControl(
      useJsonFormsControl(props)
    );

    const infoText = useInfoText(jsonFormsVuetifyControl, i18n);

    const jsonFormsControl = jsonFormsVuetifyControl.control;

    /**
     * Data path to the property tracking the ror id
     */
    const rorPath = computed(() => {
      const pathSegments = jsonFormsControl.value.path.split('.');
      pathSegments[pathSegments.length - 1] = 'ror';
      return pathSegments.join('.');
    });
    /**
     * Data stored within the ror id property
     */
    const rorData = computed(() =>
      Resolve.data(jsonforms.core?.data, rorPath.value)
    );

    /**
     * Tracks the current selected item of the list.
     *
     * The initial value is either empty or an unverified "free text entry".
     */
    const selectedValue = ref<SelectableValue | null>(
      jsonFormsControl.value.data
        ? freeTextValue(jsonFormsControl.value.data)
        : null
    );
    /**
     * Tracks the current items of the list.
     *
     * There are four states of the list:
     *
     * 1. The initial state of the list is either empty or contains the initial selected value
     * 2. When searching the list contains all found items and a (invisible) "free text" entry
     * 3. When an element is selected, only that element is contained
     * 4. In case the element is cleared, the list will be empty
     */
    const items = ref<ListItem[]>(createItemsForValue(selectedValue.value));

    /**
     * Loading indicator while querying
     */
    const loading = ref(false);
    /**
     * Stores the abort controller with which running queries can be aborted when a new
     * query is issued
     */
    const abortController: { instance: AbortController | null } = {
      instance: null,
    };
    /**
     * Last response. Used to populate the search with previous items when gaining focus.
     */
    const lastResponse = ref<OrganizationQueryResponse | null>(null);

    /**
     * Issues a new search using the new search term
     */
    const search = (newSearchInput: string | null) => {
      if (!newSearchInput) {
        // dismiss old search results if the input is cleared
        lastResponse.value = null;
      }

      // we need to update the form wide data as well as the locally
      // stored selected value and items. Otherwise the input will either
      // not accept new characters or reset immediately once the user
      // dismisses the popup. However we can't listen to that dismiss event
      // so we update on the fly.

      // search is called on initial rendering, therefore we also check the
      // currently saved data to avoid unnecessary data updates and therefore
      // unnecessary rerenderings
      if (jsonFormsControl.value.data !== (newSearchInput || undefined)) {
        jsonFormsVuetifyControl.handleChange(
          jsonFormsControl.value.path,
          newSearchInput || undefined
        );
      }
      if (rorData.value !== undefined) {
        jsonFormsVuetifyControl.handleChange(rorPath.value, undefined);
      }
      selectedValue.value = newSearchInput
        ? freeTextValue(newSearchInput)
        : null;
      items.value = createItemsForSearchResult(
        lastResponse.value,
        newSearchInput,
        t
      );

      abortController.instance?.abort();
      if (!newSearchInput) {
        loading.value = false;
        return;
      }
      abortController.instance = new AbortController();
      loading.value = true;
      queryOrganizations(
        newSearchInput || '',
        abortController.instance.signal
      ).then((response) => {
        if (response.type === 'success') {
          loading.value = false;
          lastResponse.value = response;
          items.value = createItemsForSearchResult(response, newSearchInput, t);
        }
        if (response.type === 'error') {
          // reset as if no search happened
          loading.value = false;
          console.error(
            'Something went wrong when querying organisations',
            response.message,
            response.data
          );
          items.value = createItemsForValue(selectedValue.value);
        }
        if (response.type === 'abort') {
          // ignore aborted responses, don't modify loading indicator
        }
      });
    };

    // only trigger search once the user waits a bit with typing
    const debouncedSearch = debounce(search, 200);
    const onSearch = (newSearchInput: string | null) => {
      // onSearch is called with an empty string on blur
      // when the user manually removes all characters 'null' will be handed over
      if (newSearchInput === '') {
        // ignore on blur
        return;
      }
      // onSearch will be called when a new element is selected by the user
      // because the search input changes. In that case do not issue a new search
      if (
        newSearchInput?.trim() ===
        (selectedValue.value ? getText(selectedValue.value) : null)
      ) {
        return;
      }

      // handle input clear without waiting
      if (!newSearchInput) {
        debouncedSearch.cancel();
        search(newSearchInput);
        return;
      }

      loading.value = true;
      debouncedSearch(newSearchInput);
    };
    /**
     * Called by autocomplete once the user selects an item
     */
    const onSelectValue = (value: SelectableValue | null) => {
      selectedValue.value = value;
      items.value = createItemsForValue(value);

      if (!value) {
        jsonFormsVuetifyControl.handleChange(
          jsonFormsControl.value.path,
          undefined
        );
        jsonFormsVuetifyControl.handleChange(rorPath.value, undefined);
        return;
      }
      if (value.type === 'organization') {
        jsonFormsVuetifyControl.handleChange(
          jsonFormsControl.value.path,
          value.org.name
        );
        jsonFormsVuetifyControl.handleChange(rorPath.value, value.org.id);
        return;
      }
    };

    const localedGetRegionName = (region: string) =>
      // at this point of time it's guaranteed to be set
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      getRegionName(jsonforms.i18n!.locale!, region);

    return {
      onSearch,
      onSelectValue,
      items,
      loading,
      rorData,
      selectedValue,
      valueComparator,
      getRegionName: localedGetRegionName,
      t,
      ...jsonFormsVuetifyControl,
      infoText,
    };
  },
});

export default rorRenderer;

export const entry: JsonFormsRendererRegistryEntry = {
  renderer: rorRenderer,
  tester: rankWith(50, scopeEndsWith('institution')),
};
</script>

<style scoped>
.list-item {
  display: flex;
  flex-direction: column;
  margin-bottom: 10px;
}
.list-item-text {
  font-weight: bold;
}
.list-item-organization-acronyms {
  font-style: italic;
}
</style>
