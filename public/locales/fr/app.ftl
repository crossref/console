records_home_title = Commencer la soumission d'enregistrement

accessibility_high_contrast_theme = Thème à contraste élevé

accessibility_field_style_outline = Champs de plan

organization_setting_api_title = Clés API

organization_setting_metadata_title = Utilisateur de métadonnées plus

organization_setting_org_title = Paramètres de l'organisation

resource-manager-organisation-users_title = Utilisateurs de métadonnées plus

resource-manager-organisation-users_edit-form-title = Ajouter un utilisateur

menu_item_content_registration_label = Enregistrement des subventions

menu_item_psychoceramix_label = Psychocéramix

language_menu_button = Langue

error_required = est requis

button-close = Fermer

button-ok = D'ACCORD

button_cancel_label = Annuler

button_continue_text = Continuer

button_submit_text = Soumettre

button_download_text = Télécharger

home_headline = Bienvenue sur MyCrossref

site_license_name = Licence internationale Creative Commons Attribution 4.0

site_license_preamble = Le contenu de ce site est sous licence

arraylayout_add = Ajouter nouveau

organization-create-confirm-button = Créer une organisation

confirm-leave-page-with-unsaved-changes = Êtes-vous sûr de vouloir quitter? Toutes les modifications non enregistrées seront perdues.

confirm-leave-page-confirm-button = Partir

customer_support_email_address = support@crossref.org

customer_support_contact_url = https://www.crossref.org/contact

open_dialog = ouvrir la boîte de dialogue

i_accept = J'accepte

privacy_policy_title = politique de confidentialité

login_succeeded_no_roles_assigned = Aucun rôle lié à votre compte

user_avatar__logout = Se déconnecter

ror_no_associated_ror_id = Aucun ID ROR associé

ror_start_typing_to_search = Commencez à taper pour rechercher

ror_no_matched_institutions = Aucun établissement correspondant

ror_matched_institutions =
  { $count ->
    [one] Correspondant à une institution
   *[other] Apparié { $count } établissements
  }

-metadata-plus = Métadonnées Plus

subscription-row-label =
  { $name ->
    [METADATA_PLUS] { -metadata-plus }
   *[OTHER] Générique
  } Abonnement

subscription-status-text-label-active = actif

subscription-status-text-label-inactive = inactif

dialog-title-confirm-subscription-change = Confirmer le changement d'abonnement

dialog-message-confirm-subscription-enable-METADATA_PLUS = Vraiment activer { -metadata-plus } abonnement?

dialog-message-confirm-subscription-disable-METADATA_PLUS = Vraiment désactiver { -metadata-plus } abonnement?

user-added-to-organization = { $username } a été ajouté à { $organization }

api-key-table-title = Clés API

api-key-created-title = Clé API créée

api-key-created-subtitle = Ce sera la seule fois où la clé sera affichée.

api-key-created-keep-safe-instructions = Copiez la clé et conservez-la précieusement.

api-key-created-copy-key-action = Copier la clé

api-key-created-key-copied = Clé copiée

api-key-created-no-key-error = Une erreur s'est produite. Aucune clé API n'a été renvoyée. Veuillez réessayer.

api-key-created-copy-to-clipboard-error = Une erreur s'est produite lors de la copie dans le presse-papiers. { $error }

api-key-disabled-help-contact-support-team = Équipe d'assistance Contact Plus

api-key-disabled-help-title = Clé API désactivée

api-key-disabled-help-keyid-title = Clé { $apiKeyId }

api-key-disabled-help-explanation = Cette clé a été désactivée. Tout client utilisant cette clé n'aura plus accès aux Services Crossref Plus. Veuillez contacter le support Crossref Plus pour réactiver la clé API

error-no-clipboard-access-in-nonsecure-contexts = L'accès au presse-papiers n'est disponible que dans des contextes sécurisés (HTTPS, localhost).

api-key-delete-confirmation-instructions = Veuillez taper SUPPRIMER pour continuer

api-key-delete-confirmation-subtitle = Voulez-vous vraiment supprimer cette clé API ?

api-key-delete-confirmation-title = Supprimer la clé API

api-key-delete-confirmation-warning = Es-tu sûr? La suppression de cette clé API la révoquera immédiatement, ainsi que l'accès de tous les clients qui l'utilisent.

api-key-activate = Activer la clé

api-key-deactivate = Désactiver la clé

api-key-activate-confirm-title = Activer la clé API ?

api-key-deactivate-confirm-title = Désactiver la clé API ?

api-key-enable = Désactiver la clé

api-key-enable-confirm-button = Activer la clé

api-key-disable = Activer la clé

api-key-disable-confirm-button = Désactiver la clé

api-key-toggle = Basculer la clé API

api-key-status-active = Actif

api-key-status-inactive = Inactif

