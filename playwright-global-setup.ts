import { chromium, FullConfig } from '@playwright/test';
import { env } from './src/env';

async function globalSetup(config: FullConfig): Promise<void> {
  const browser = await chromium.launch();
  const page = await browser.newPage();
  await page.goto(config.projects[0].use.baseURL as string);
  if (env().isUsersnap) {
    await page
      .frameLocator('iframe[name="us-entrypoint-bubbleV2"]')
      .locator('text=Ok')
      .click();
  }
  await page.context().storageState({ path: 'storageState.json' });
  await browser.close();
}

export default globalSetup;
