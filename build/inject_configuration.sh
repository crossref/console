#!/bin/bash
# This script searches the environment for variables that target
# the frontend's configuration, and template them into a JSON
# block within the index.html of the application.

# Enable debugging, to aid in CI diagnoses
set -o xtrace

if [ -z "$1" ]
  then
    echo "No target filepath supplied. Exting."
    exit 1
fi

# A function to join/implode an array to a delimeted string
function joinByChar() {
  # Set the local special IFS variable to the value of the 1st parameter
  # which specifies the delimeter to use in double-quoted variable expansion
  local IFS="$1"
  shift
  # Expand the remaining parameters into a string, joined by the delimeter
  echo "$*"
}

# An array to store the --args declarations to pass into jq
jq_args=( )

# Save the current bash Internal Field Separator value
saveIFS="$IFS"
# Set IFS to comma, to cause variable expansion to quote values with spaces in them
IFS=','
for item in "${!VUE_APP_@}"; do
    printf '%s=%s\n' "$item" "${!item}"
    # Create the --args declaration for the current configuration parameter
    jq_args+=( --arg "$item" "${!item}")
done

# Call jq with null input, the configuration keys and their values as --arg parameters
# and specify the named arguments as the filter
replacement_json=`jq --null-input -c ${jq_args[@]} '$ARGS.named'`
echo $replacement_json

# Escape characters which have special meaning to sed
replacement_json_escaped=$(sed 's/[&/\]/\\&/g' <<<"$replacement_json")
echo $replacement_json_escaped
# Replace the placeholder within the target file
sed -i -e "s/<!-- configuration will be auto injected -->/$replacement_json_escaped/g" $1
# Restore the Bash IFS setting
IFS="$saveIFS"
