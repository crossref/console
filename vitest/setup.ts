import { beforeAll } from 'vitest';
import { config } from '@vue/test-utils';

// Set up a global mock for the $t function
config.global.mocks = {
  $t: (msg: string) => msg, // This mock will simply return the key passed to it
};

(global as any).CSS = { supports: () => false };

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: vi.fn().mockImplementation((query) => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: vi.fn(), // deprecated
    removeListener: vi.fn(), // deprecated
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});
beforeAll(() => {
  class MockResizeObserver {
    observe() {
      // do nothing
    }
    unobserve() {
      // do nothing
    }
    disconnect() {
      // do nothing
    }
  }

  // Mocking the ResizeObserver in the global object
  global.ResizeObserver = MockResizeObserver;
  global.CSS = {
    supports: (str: string) => false,
    escape: (str: string) => str,
  } as any;
});
