#!/bin/sh

# Exit on any non zero return value
set -ex

aws --endpoint-url=http://integration-localstack:4566 s3 cp dist s3://fe-bucket --recursive

echo "Deployed frontend"
