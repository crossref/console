# .gitlab-ci.yml file to be placed in the root of your repository

variables:
  MAINT_BRANCH_PATTERN: /^(([0-9]+)\.)?([0-9]+)\.x/
  PREREL_BRANCH_PATTERN: /^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$/
  CYPRESS_INSTALL_BINARY: '0'
  LIB_NAME: 'crossref-ui'
  DEPLOY_EXCLUDE_GLOB: 'demo.html'
  DEPLOY_INCLUDE_GLOB: '*'
  DEPLOY_PATH_PREFIX: '${CI_PROJECT_NAME}/${LIB_NAME}'
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ''
  NODE_DOCKER_IMAGE: 'node:18'
  CI_BUILD_IMAGE: '${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}'
  PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD: 'true'
  # PUBLIC_PATH: "/${CI_PROJECT_NAME}/"
stages:
  - unit-test
  - component-test
  - get-version
  - build
  - e2e-test
  - publish
  - deploy
  - semantic-release

component-test:
  stage: component-test
  image: mcr.microsoft.com/playwright:v1.43.0-jammy
  script:
    - npm ci
    - npx playwright install
    - npm run test-ct
  artifacts:
    paths:
      - test-results
    when: always
  except:
    - tags

e2e-test:
  stage: e2e-test
  image: docker:27.1.1
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  before_script:
    - docker compose version
    - which docker
    - docker info
    - env
  script: 
    - docker compose -f docker-compose-integration-test.yml up test-runner --build --exit-code-from=test-runner
  after_script:
    - docker-compose down
    - ls -la playwright-report
    - ls -la test-results
  artifacts:
    paths:
      - test-results
      - playwright-report
    when: always
    expire_in: 2 days
  except:
    - tags

build:
  image: '$NODE_DOCKER_IMAGE'
  stage: build
  script:
    - export APP_VERSION="$(cat VERSION.txt)"
    - echo "APP_VERSION is ${APP_VERSION}"
    - export VUE_APP_SENTRY_RELEASE="${APP_VERSION}"
    - npm ci
    - npm run build
  artifacts:
    paths:
      - dist

# integration-test:
#   image: docker/compose
#   stage: integration-test
#   services:
#     - docker:dind

#   before_script:
#     - docker info

#   script:
#     - docker-compose -f docker-compose-integration-test.yml up --build --exit-code-from test-runner
#   artifacts:
#     paths:
#       - test-results
#     when: always

publish:
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  stage: publish
  tags:
    - aws
    - crossref-portal
  services:
    - docker:dind
  before_script:
    - export APP_VERSION="$(cat VERSION.txt)"
    - echo "APP_VERSION is ${APP_VERSION}"
    - export VUE_APP_SENTRY_RELEASE="${APP_VERSION}"
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - npx semantic-release
    - docker build --pull -f deploy/Dockerfile.prod -t "$CI_REGISTRY_IMAGE:$APP_VERSION" -t "$CI_REGISTRY_IMAGE:latest" .
    - docker push "$CI_REGISTRY_IMAGE:$APP_VERSION"
    - docker push "$CI_REGISTRY_IMAGE:latest"
  only:
    - alpha
    - main
  artifacts:
    paths:
      - CHANGELOG.md

unit-test:
  image: '$NODE_DOCKER_IMAGE'
  stage: unit-test
  before_script:
    - apt-get update
    - apt-get install libxml2-utils
  script:
    - export APP_VERSION="$(cat VERSION.txt)"
    - echo "APP_VERSION is ${APP_VERSION}"
    - npm ci
    - VUE_APP_KEYCLOAK_ENABLED=true npm run test:unit
    - ./tests/unit/XmlGeneration/validate-invalid-xml.sh
    - ./tests/unit/XmlGeneration/validate-xml.sh
  except:
    - tags

semantic-release:
  image: public.ecr.aws/crossref/cicada:latest
  stage: semantic-release
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: staging
  #  services:
  #    - docker:dind
  before_script:
    - git show-ref --tags -d
    - export APP_VERSION="$(cat VERSION.txt)"
    - '[[ -z ${CI_PIPELINE_ID+x} ]] && export APP_VERSION=$CI_COMMIT_SHA'
    - echo "APP_VERSION is ${APP_VERSION}"
    - if [ -z "$APP_VERSION" ]; then echo "APP_VERSION is not defined, exiting."; exit 0; fi
  script:
    - '[[ ! -z ${CI_PIPELINE_ID+x} ]] && npx semantic-release'
  artifacts:
    paths:
      - dist # artifact path must be /public for GitLab Pages to pick it up
  only:
    - main

# Get current semantic version for release branches
get-semantic-version:
  image: public.ecr.aws/crossref/cicada:latest
  stage: get-version
  only:
    refs:
      - main
      - $MAINT_BRANCH_PATTERN
      - $PREREL_BRANCH_PATTERN
  script:
    - echo "Using semantic version"
    #    - npm install @semantic-release/gitlab @semantic-release/exec @semantic-release/changelog
    - npx semantic-release --dry-run
  artifacts:
    paths:
      - VERSION.txt

# Get generic version for non-release branches
get-generic-version:
  image: node:lts-alpine
  stage: get-version
  except:
    refs:
      - main
      - $MAINT_BRANCH_PATTERN
      - $PREREL_BRANCH_PATTERN
  script:
    - echo "Using generic version"
    - export APP_VERSION="build-${CI_PIPELINE_ID}"
    - echo "Version is ${APP_VERSION}"
    - echo $APP_VERSION > VERSION.txt
  artifacts:
    paths:
      - VERSION.txt

build_amd64_image:
  image: public.ecr.aws/crossref/cicada:latest
  stage: build
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  script:
    - 'echo "CI_COMMIT_TAG: $CI_COMMIT_TAG"'
    - 'echo $CI_COMMIT_TAG > VERSION.txt'
    - 'build_image.sh amd64 "${CI_PROJECT_DIR}/deploy/Dockerfile.prod" console $CI_COMMIT_TAG $CI_PROJECT_DIR'
  only:
    - tags

build_arm64_image:
  image: public.ecr.aws/crossref/cicada:latest
  stage: build
  tags:
    - aws
    - ec2
    - arm64
  services:
    - docker:dind
  script:
    - 'echo "CI_COMMIT_TAG: $CI_COMMIT_TAG"'
    - 'echo $CI_COMMIT_TAG > VERSION.txt'
    - 'build_image.sh arm64 "${CI_PROJECT_DIR}/deploy/Dockerfile.prod" console $CI_COMMIT_TAG $CI_PROJECT_DIR'
  only:
    - tags

multi_arch_manifest:
  image: public.ecr.aws/crossref/cicada:latest
  stage: build
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  needs: ['build_amd64_image', 'build_arm64_image']
  script:
    - create_manifest.sh console $CI_COMMIT_TAG
  only:
    - tags

deploy_console_staging:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: staging
  needs: ['multi_arch_manifest']
  script:
    - deploy_container.sh micro-staging console-staging $CI_COMMIT_TAG
  only:
    - tags

deploy_console_sandbox:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: sandbox
  needs: ['deploy_console_staging']
  script:
    - deploy_container.sh micro-sandbox console-sandbox $CI_COMMIT_TAG
  when: manual
  only:
    - tags

deploy_console_production:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: production
  needs: ['deploy_console_sandbox']
  script:
    - deploy_container.sh micro-production console-production $CI_COMMIT_TAG
  when: manual
  only:
    - tags
